/**
 * App Theme - Colors
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */

const app = {
  // background: '#248ca0',//#E9EBEE',
  cardBackground: '#FFFFFF',
  // listItemBackground: '#FFFFFF',
};

const brand = {
  brand: {
    primary: '#276a90',
    secondary: '#17233D',
  },
};

const text = {
  textPrimary: '#222222',
  textSecondary: '#777777',
  headingPrimary: brand.brand.primary,
  headingSecondary: brand.brand.primary,
};

const borders = {
  border: '#D0D1D5',
};

// BottomNavBar FooterStyle
const tabbar = {
  tabbar: {
    background: '#2f5d8e',
    iconDefault: '#fff',
    iconSelected: '#00e9bf',
  },
};

export default {
  ...app,
  ...brand,
  ...text,
  ...borders,
  ...tabbar,
};
