/**
 * Test to check if the component renders correctly
 */
/* global it expect */
import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import ActivityView from '@containers/activities/ActivityView';

it('ActivityView renders correctly', () => {
  const thisActivity = {
    id: 1,
    title: 'Activity Title',
    body: 'Blah di blah lorem ipsum',
    image: 'http://placehold.it/300x100',
  //  ingredients: ['Hello world', 'Hello world', 'Hello world'],
  //  method: ['And again', 'And again', 'And again'],
  };

  const tree = renderer.create(
    <ActivityView activity={thisActivity} />,
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
