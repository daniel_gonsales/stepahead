/**
 * Test to check if the component renders correctly
 */
/* global it expect */
import 'react-native';
import React from 'react';
import renderer from 'react-test-renderer';

import ListingView from '@containers/activities/Listing/ListingView';

it('ListingView renders correctly', () => {
  const theseActivities = [
    { image: 'http://placehold.it/300x100', title: 'A Cake', content: 'Lorem ipsum' },
    { image: 'http://placehold.it/300x100', title: 'A Muffin', content: 'Lorem ipsum' },
  ];

  const tree = renderer.create(
    <ListingView activities={theseActivities} />,
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
