/**
 * Style Guide
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {

  View,
  Alert,
  ListView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  // Badge,
  Image
} from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';
import { SocialIcon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { ListItem, Left, Right, Body, Icon, Badge } from 'native-base'

import LinearGradient from 'react-native-linear-gradient';


// Consts and Libs
import { AppColors, AppStyles, AppSizes } from '@theme/';


// Components
import {
  Alerts,
  Button,
  Card,
  Spacer,
  Text,
  List,
  // ListItem,
  FormInput,
  FormLabel,
} from '@components/ui/';

import * as UserActions from '@redux/user/actions'
import * as ChatActions from '@redux/chat/actions';



//Conversations.componentName = 'Conversations';


// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  user: state.user,
  chat: state.chat

});


// Any actions to map to the component?
const mapDispatchToProps = {
  // getChat: UserActions.getChat
  getChat: ChatActions.getChat,
  getLastMessage: ChatActions.getLastMessage,
  getConversations: ChatActions.getConversations

};


/* Styles ==================================================================== */
const styles = StyleSheet.create({
  // Tab Styles
  tabContainer: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: AppColors.brand.primary,
  },
  tabbarIndicator: {
    backgroundColor: '#FFF',
  },
  tabbar_text: {
    color: '#FFF',
  },
});

/* Component ==================================================================== */
class Conversations extends Component {
  static componentName = 'Conversations';



  componentWillMount() {
    if (this.props.data){
      // console.log('\n' + this.props.data[0] + '\n' + this.props.data[1] + '\n' + this.props.data[2] + '\n' + this.props.data.conversation)
      Actions.chat([this.props.data[0], this.props.data[1], this.props.data[2]])
    } else {
      if (JSON.stringify(this.props.user) != '{}') {
        console.log('ConversationsWillMountHasUser ' + JSON.stringify(this.props.user.fullName) + ' ' + this.props.user.avatar)
        this.props.getConversations(this.props.user.fullName, this.props.user.avatar)

      } else {
        Actions.login()
      }
    }
  }


  getDate(date){
    let tempTime = new Date (date)
    let tempNow = new Date( Date.now())
    let tempMin

    console.log('Date.Now ' + Date.now() + ' Date message ' + date)

    if (tempTime.getMinutes() < 10 ) {
      tempMin = '0' + tempTime.getMinutes().toString()
    } else {
      tempMin = tempTime.getMinutes()
    }

    if( tempNow.getFullYear() === tempTime.getFullYear() && tempNow.getMonth() === tempTime.getMonth() && tempNow.getDate() === tempTime.getDate()) {
      console.log('DayIsToday')
      return(`${tempTime.getHours()}:${tempMin}`)
    } else {
      return(`${tempTime.getDate()}/${tempTime.getMonth()}/${tempTime.getFullYear()}`)

    }

  }


  getConvView(conversations) {
  console.log('getConversationsView ' + JSON.stringify(conversations))
  let temp = Object.values(conversations).map((conv, item) => {
    return(
      <TouchableOpacity key={Date.now()} style={{height:70, backgroundColor:'transparent', marginBottom: 10,marginTop:-1, marginLeft:20, marginRight:5, flexDirection:'row'}}
        onPress={() =>
          Actions.chat([conv.receiverId, conv.receiverFullName, conv.avatar, conv.conversation])
        }
      >
        <View style={{width:65}}>
        <Image
        source={{
          uri: conv.avatar,
          overlayColor: 'white',
        }}
        // imageStyle={{ borderRadius: 5 }}
        // style={[styles.featuredImage]}
        style={{
          width: 65,
          height: 65,
          borderRadius: 32.5,
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf:'center'

        }}
        imageStyle={{
          borderRadius: 32.5
        }}
      />
        </View>

      <View style={{marginLeft:15, borderBottomColor:'#4697a7',borderBottomWidth:2}}>
        <View style={{alignSelf:'flex-start'}}>
          <Text style={{fontSize:16,fontWeight:'bold',color:'white', width:AppSizes.screen.width * 0.6}}>{conv.receiverFullName}</Text>
          </View>
        {conv.lastmessage ?
        <View>
          <View style={{marginLeft:0}}>
          <Text style={{color:'white', fontSize:13}} note>{conv.lastmessage}</Text>
          </View>
          <View><Text style={{color:'white', fontSize:11, position:'relative', bottom:-10}} note>{this.getDate(conv.lastmessageTime)}</Text></View>
          </View>
          :

          <View>
            <Text style={{marginLeft:15,color:'white'}} note>No messages yet</Text>
          </View>
        }

      </View>
      <Right style={{justifyContent:'flex-end',alignContent:'flex-end',alignSelf:'center',alignItems:'flex-end'}}>
        {conv.messageCounter > 0 ?
          <Badge style={{backgroundColor:'#5aacba'}}>
            <Text style={{color:'white'}}>{conv.messageCounter}</Text>
          </Badge>
          :
            null
        }
      </Right>
      </TouchableOpacity>
    )
  })

  return(temp)
  }

  render = () => (
    <LinearGradient
    colors={['#008d9a', '#2f6ba1']}
    style={{height:AppSizes.screen.height}}
    start={{x: 0.5, y: 0.3}}
    end={{x: 0.7, y: 1.0}}
  >
  {JSON.stringify(this.props.user) != '{}' ?
    <ScrollView style={{borderTopColor:'rgba(255,255,255,0.5)',borderTopWidth:1}}>
    {console.log('LIVEUSER  ' + JSON.stringify(this.props.user))}
    {console.log('LIVEUSER  ' + JSON.stringify(this.props.user.conversations))}

    <View style={{marginTop:20}}></View>
    {JSON.stringify(this.props.user.conversations) != undefined ?
      <View>
    {this.getConvView(this.props.user.conversations)}

      </View>
    :
      null
    }

</ScrollView>
    :
    null
    }

</LinearGradient>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Conversations);
