/**
 * Launch Screen
 *  - Shows a nice loading screen whilst:
 *    - Preloading any specified app content
 *    - Checking if user is logged in, and redirects from there
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  Alert,
  StatusBar,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

// Consts and Libs
import { AppStyles, AppSizes } from '@theme/';
import * as ActivityActions from '../../redux/activities/actions';

/* Styles ==================================================================== */
const styles = StyleSheet.create({
    map: {
      ...StyleSheet.absoluteFillObject,
    }
  });

  var mapStyle = [
    {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
            {
                "hue": "#00ffbc"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -70
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": -60
            }
        ]
    }
];

setMarkerRef = (ref) => {
    this.marker = ref
  }

  showCallout = () => {
    this.marker.showCallout()
  }

/* Component ==================================================================== */
class AppLaunch extends Component {
  static componentName = 'AppLaunch';

  static propTypes = {
    activities: PropTypes.arrayOf(PropTypes.object).isRequired,
  }

  constructor() {
    super();
    console.ignoredYellowBox = ['Setting a timer'];
  }


  componentDidMount = async () => {
    // Preload content here
      this.props.getMapMarkers()
    .then(() => (
    //  console.log("MOII " + JSON.stringify(this.props.activities[0].title)),
    //  console.log("DASDAS " + typeof this.props.activities),
      this.setState({ activities: props.activities })

    )

    ).catch(
      // Log the rejection reason
     (reason) => {
          console.log('Handle rejected promise ('+reason+') here.')
          console.log("FFUUCK YOU")
          alert("FUUUUCKYOOOUU")

      })
  }

  componentWillReceiveProps(props) {
    console.log("POROPS " + JSON.stringify(props))

    if (props.activities) {
      this.setState({ activities: props.activities });
    }
  }

  componentDidMount() {
this.setState({
  getMapMarkers: ActivityActions.getMapMarkers,
  //getCategory: ActivityActions.getCategory,
})

    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
      let region = {
        latitude:       position.coords.latitude,
        longitude:      position.coords.longitude,
        latitudeDelta:  0.00922*1.5,
        longitudeDelta: 0.00421*1.5
      }
      this.onRegionChange(region, region.latitude, region.longitude);
    });
  }

  onRegionChange(region, lastLat, lastLong) {
    this.setState({
      mapRegion: region,
      // If there are no new values set use the the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  onMapPress(e) {
    //console.log("PROOPS " + JSON.stringify(this.props));
    //console.log(e.nativeEvent.coordinate.longitude);
    let region = {
      latitude:       e.nativeEvent.coordinate.latitude,
      longitude:      e.nativeEvent.coordinate.longitude,
      latitudeDelta:  0.00922*1.5,
      longitudeDelta: 0.00421*1.5
    }
    this.onRegionChange(region, region.latitude, region.longitude)
    //console.log("DOIOIO " + this.state.activities)
  }

  handleMarkerPress(event) {
    const markerID = event.nativeEvent.id
    renderedMarker = markerID
    //console.log(markerID)
                  {/* Callout */}
                  <MapView.Callout tooltip style={styles.callout}>
                  <Callout
                    name={event.nativeEvent.name}
                    image={event.description}
                  />
                </MapView.Callout>

  }



render() {
    return (
      <View style={{flex: 1}}>
        <MapView
          calloutOffset={{ x: -8, y: 28 }}
          ref={ref => { this.map = ref; }}
          onRegionChangeComplete={this.showCallout}
          style={styles.map}
          region={this.state.mapRegion}
          showsUserLocation={true}
					showsMyLocationButton={true}
					showsCompass={true}
          followUserLocation={true}
          customMapStyle={mapStyle}
//          provider={PROVIDER_GOOGLE}
          onRegionChange={this.onRegionChange.bind(this)}
          onPress={this.onMapPress.bind(this)}>

<MapView.Marker
        //    style={styles.customMap}
            coordinate={{
              latitude: (this.state.lastLat + 0.00030) || -36.82339,
              longitude: (this.state.lastLong + 0.00062) || -73.03569,
            }}
            image={require('../../images/markers/music.png')}
            identifier={"1"}
            onPress={e => this.handleMarkerPress(e)}
            title="Fa lala latfel"
            description="Lafafele"

          />


          <MapView.Marker
            calloutOffset={{ x: -8, y: 28 }}
            style={styles.customMap}
            coordinate={{
              latitude: (this.state.lastLat + 0.00310) || -36.82339,
              longitude: (this.state.lastLong + 0.00620) || -73.03569,
            //    latitude: (this.state.activities.)
            }}
            image={require('../../images/markers/music.png')}
            identifier={"2"}
            onPress={e => this.handleMarkerPress(e)}
            title={this.props.activities[0].title}
            description={this.props.activities[0].body}

          />


          <MapView.Marker
            coordinate={{
              latitude: (this.state.lastLat + 0.00042) || -36.82339,
              longitude: (this.state.lastLong + 0.00320) || -73.03569,
            }}

            image={require('../../images/markers/sport.png')}
            identifier={"3"}
            onPress={e => this.handleMarkerPress(e)}
            onPress={e => console.log(JSON.stringify(e.nativeEvent))}
            title="RUN ASS! RUN!"
            description="TEXTY TEXT TEXT THA TEXT!"

          />

          <MapView.Marker
            coordinate={{
              latitude: (this.state.lastLat + 0.00050) || -36.82339,
              longitude: (this.state.lastLong + 0.00050) || -73.03569,
            }}>
            <View>
              <Text style={{color: '#000'}}>
                { this.state.lastLong } / { this.state.lastLat }
              </Text>
            </View>
          </MapView.Marker>
        </MapView>
      </View>
    );
  }
}

/* Export Component ==================================================================== */
export default AppLaunch;
