import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  AsyncStorage
} from 'react-native';

import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import PropTypes from 'prop-types';
import Callout from './Callout';
import * as UserActions from '../../redux/user/actions';
import * as ActivityActions from '../../redux/activities/actions';

import { connect } from 'react-redux';










class MapExtended extends Component {







static componentName = 'MapExtended';
}



export default connect(mapStateToProps, mapDispatchToProps)(MapExtended);


/**
 * List of Activities for a Category Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
//import * as ActivityActions from '@redux/activities/actions';
import * as ActivityActions from '@redux/activities/actions';
// Components
import Loading from '@components/general/Loading';
import ActivityListingRender from './ListingView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({  
    activities: state.activity.activities || [],
    getMapMarkers: ActivityActions.getMapMarkers(),
  
  //  filters: state.activity.filters || []
  });

// Any actions to map to the component?
const mapDispatchToProps = {
};

/* Component ==================================================================== */
class CategoryListing extends Component {
  static componentName = 'CategoryListing';

  static propTypes = {
  //  activities: PropTypes.arrayOf(PropTypes.object),
      activities: PropTypes.arrayOf(PropTypes.object),
      category: PropTypes.string.isRequired,
  //  getActivities: PropTypes.func.isRequired,
      getActivities: PropTypes.func.isRequired,
  }

  static defaultProps = {
    text: 'MapExtended',
    //meals: [],
    getActivities: null,
    //getCategory: ActivityActions.getCategory(),
    activities: null,
   };

   state = {
    mapRegion: null,
    lastLat: null,
    lastLong: null,
  }

  //componentDidMount = () => this.getThisCategoriesActivities(this.props.activities);
  //componentWillReceiveProps = props => this.getThisCategoriesActivities(props.activities);

  //componentDidMount = () => this.getThisCategoriesActivities(this.props.activities);
  //componentWillReceiveProps = props => this.getThisCategoriesActivities(props.activities);

  /**
    * Pick out activities that are in the current category
    * And hide loading state
    */
  getThisCategoriesActivities = (allActivities) => {
    if (allActivities.length > 0) {
      const activities = allActivities.filter(activity =>
        activity.category.toString() === this.props.category.toString(),
      );

      this.setState({
        activities,
        loading: false,
      });
    }
  }

  /**
    * Fetch Data from API
    */
//  fetchActivities = () => this.props.getActivities()
    fetchActivities = () => this.props.getActivities()
    .then(() => this.setState({ error: null, loading: false }))
    .catch(err => this.setState({ error: err.message, loading: false }))

  render = () => {
    if (this.state.loading) return <Loading />;

    return (
      <ActivityListingRender
        activities={this.state.activities}
        reFetch={this.fetchActivities}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListing);
