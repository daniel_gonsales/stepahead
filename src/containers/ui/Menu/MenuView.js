/**
 * Menu Contents
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import { Switch, Slider, TextInput  } from 'react-native';
import PropTypes from 'prop-types';
import {
  View,
  Alert,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';

import LinearGradient from 'react-native-linear-gradient';

// Consts and Libs
import { AppStyles, AppSizes } from '@theme/';

// Components
import { Spacer, Text, Button } from '@ui/';

import * as UserActions from '@redux/user/actions'
import * as ActivityActions from '@redux/activities/actions'
import * as ListingContainer from '../../activities/Listing/ListingContainer'

import { Firebase, FirebaseRef } from '../../../constants/';

import { FormInput, Icon } from 'react-native-elements';
import ModalSelector from 'react-native-modal-selector';


export let tempoFil = 0
/* Styles ==================================================================== */
const MENU_BG_COLOR = 'transparent';

const styles = StyleSheet.create({
  backgroundFill: {
    backgroundColor: MENU_BG_COLOR,
    height: AppSizes.screen.height,
    width: AppSizes.screen.width,
    position: 'absolute',
    top: 0,
    left: 0,
  },
  container: {
    position: 'relative',
    flex: 1,
  },
  menuContainer: {
    flex: 1,
    left: 0,
    right: 0,
    backgroundColor: MENU_BG_COLOR,
  },

  // Main Menu
  menu: {
    flex: 3,
    left: 0,
    right: 0,
    backgroundColor: MENU_BG_COLOR,
    padding: 20,
    paddingTop: AppSizes.statusBarHeight + 20,
  },
  menuItem: {
    //borderBottomWidth: 1,
    //borderBottomColor: 'rgba(255, 255, 255, 0.1)',
    marginBottom: 20,

  },
  menuItem_text: {
    fontSize: 16,
    lineHeight: parseInt(16 + (16 * 0.5), 10),
    fontWeight: '500',
    marginTop: 14,
    marginBottom: 8,
    color: '#EEEFF0',
  },

  // Menu Bottom
  menuBottom: {
    flex: 1,
    left: 0,
    right: 0,
    justifyContent: 'flex-end',
    paddingBottom: 10,
  },
  menuBottom_text: {
    color: '#EEEFF0',
  },

  row: {
    flex: 1,
    flexDirection: 'row',
  },
  cell: {
    flex: 1,
    borderWidth: StyleSheet.hairlineWidth,
  },
  scrollView: {
    flex: 1,
  },
  contentContainer: {
    height: 500,
    paddingVertical: 100,
    paddingLeft: 20,
  },
  textButton: {
    color: 'deepskyblue',
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: 'deepskyblue',
    margin: 2,
  },
  dropdown_1: {
    flex: 1,
    top: 0,
    left: 0,
  },
  menuLabel: {
    color: 'white',
    fontWeight: '500',
    paddingBottom: 3
  }
});


const CATEGORIES = ['All', 'Falling', 'Jumping', 'Running']
const AGES = ['All', '0-16', '16-30', '30-66', '66-99']

const catsData = [
    { key: 999, section: true, label: 'Select a category' },
    { key: 0, label: 'All' },
    { key: 1, label: 'Falling' },
    { key: 2, label: 'Jumping' },
    { key: 3, label: 'Running' }
];

const ageData = [
    { key: 999, section: true, label: 'Select an age range' },
    { key: 0, label: 'All' },
    { key: 1, label: '0-16' },
    { key: 2, label: '16-30' },
    { key: 3, label: '30-66' },
    { key: 4, label: '66-99' }
];

/* Component ==================================================================== */
class Menu extends Component {
  static propTypes = {
    logout: PropTypes.func.isRequired,
    closeSideMenu: PropTypes.func.isRequired,
    user: PropTypes.shape({
      email: PropTypes.string,
    }),
    unauthMenu: PropTypes.arrayOf(PropTypes.shape({})),
    authMenu: PropTypes.arrayOf(PropTypes.shape({})),
    getActivities: PropTypes.func.isRequired,
    getCategory: PropTypes.func.isRequired
  }

  static defaultProps = {
    unauthMenu: [],
    authMenu: [],
    getActivities: null,
    getCategory: null,
    minPrice: 0,
    maxPrice: 1000,
  }

  componentWillMount () {
    if (this.props.user.uid){
      console.log('MenuViewWillMount rad ' + this.props.user.radius)
      this.props.updateRadius(this.props.user.radius)
      this.setState({
        email: this.props.user.email,
        uid: this.props.user.uid,
        radius: this.props.user.radius,
      })

    }
    console.log("MenuView WillMount" + JSON.stringify(this.props))
    this.setState({
      // authMenu_0 : this.props.menuViewText.authMenu_0,
      // authMenu_1 : this.props.menuViewText.authMenu_1,
      // unauthMenu_0 : this.props.menuViewText.unauthMenu_0,
      // unauthMenu_1 : this.props.menuViewText.unauthMenu_1,
      // catFilterTitle : this.props.menuViewText.catFilterTitle,
      // ageTitle : this.props.menuViewText.ageTitle,
      // minPTitle : this.props.menuViewText.minPTitle,
      // maxPTitle: this.props.menuViewText.maxPTitle,
      // radius : this.props.menuViewText.radius,
      // loggedInAs : this.props.menuViewText.loggedInAs,

      authMenu_0 : this.props.menuViewText.authMenu_0,
      authMenu_1 : this.props.menuViewText.authMenu_1,
      unauthMenu_0 : this.props.menuViewText.unauthMenu_0,
      unauthMenu_1 : this.props.menuViewText.unauthMenu_1,
      catFilterTitle : this.props.menuViewText.catFilterTitle,
      ageTitle : this.props.menuViewText.ageTitle,
      minPTitle : this.props.menuViewText.minPTitle,
      maxPTitle: this.props.menuViewText.maxPTitle,
      // radius : this.props.menuViewText.radius,
      loggedInAs : this.props.menuViewText.loggedInAs,


    })

  }

  componentDidMount(){
  }

componentWillReceiveProps (props) {
  // this.setState({
  //   authMenu_0 : props.menuViewText.authMenu_0,
  //   authMenu_1 : props.menuViewText.authMenu_1,
  //   unauthMenu_0 : props.menuViewText.unauthMenu_0,
  //   unauthMenu_1 : props.menuViewText.unauthMenu_1,
  //   catFilterTitle : props.menuViewText.catFilterTitle,
  //   ageTitle : props.menuViewText.ageTitle,
  //   minPTitle : props.menuViewText.minPTitle,
  //   maxPTitle: props.menuViewText.maxPTitle,
  //   radius : props.menuViewText.radius,
  //   loggedInAs : props.menuViewText.loggedInAs,
  //   email: props.user.email,
  //   uid: props.user.uid,
  //   radius: props.user.radius,

  // })

  if(JSON.stringify(props.user) != '{}') {
    let tempPriceMin
    let tempPriceMax

    if (JSON.stringify(props.user.priceRange) === undefined) {
      console.log('NoPrice! ')
      tempPriceMax = 5000
      tempPriceMin = 0

    } else {
      tempPriceMax = props.user.priceRange.max
      tempPriceMin = props.user.priceRange.min
    }

    console.log('MenuViewCategories ' + CATEGORIES.indexOf(props.user.filter) )

    // CATEGORIES.forEach(cat => {if (cat === props.user.filter){ console.log(' ' + cat.id) + ' indexof ' + indexo } })

    this.setState({
      authMenu_0 : props.menuViewText.authMenu_0,
      authMenu_1 : props.menuViewText.authMenu_1,
      unauthMenu_0 : props.menuViewText.unauthMenu_0,
      unauthMenu_1 : props.menuViewText.unauthMenu_1,
      catFilterTitle : props.menuViewText.catFilterTitle,
      ageTitle : props.menuViewText.ageTitle,
      minPTitle : props.menuViewText.minPTitle,
      maxPTitle: props.menuViewText.maxPTitle,
      // radiusTitle : props.menuViewText.radius,
      loggedInAs : props.menuViewText.loggedInAs,
      email: props.user.email,
      uid: props.user.uid,
      // radius: props.user.radius,

      maxPrice: tempPriceMax,
      minPrice: tempPriceMin,
      radius: props.user.radius,
      categoryTextValue: (props.user.filter === undefined || props.user.filter === null ? CATEGORIES[0] : CATEGORIES[CATEGORIES.indexOf(props.user.filter)]),
      ageTextValue: (props.user.agefilter === undefined ? AGES[0] : AGES[props.user.agefilter])
    })
    console.log("MenuView LoggedIn User Gets " + JSON.stringify(this.state))
  }
  // else {
  //   this.setState({
  //     maxPrice: 50000000,
  //     minPrice: 0,
  //     radius: 1500
  //   })
  //   console.log("MenuView Not LoggedIn User Gets " + JSON.stringify(this.state))

  // }
}

  /**
   * On Press of any menu item
   */
  onPress = (action) => {
    this.props.closeSideMenu();
    if (action) action();
  }

  /**
   * On Logout Press
   */
  logout = () => {
    if (this.props.logout) {
      this.props.logout()
        .then(() => {
          this.props.closeSideMenu();
          Actions.login();
        }).catch(() => {
          Alert.alert('Oh uh!', 'Something went wrong.');
        });
    }
  }

  /**
   * Each Menu Item looks like this
   */
  menuItem = item => (
    <TouchableOpacity
      key={`menu-item-${item.title}`}
      onPress={() => this.onPress(item.onPress)}
    >
      <View style={[styles.menuItem]}>
        <Text style={[styles.menuItem_text]}>
        {/* Translate - Done - CheckLogin */}
          {item.title}
        </Text>
      </View>
    </TouchableOpacity>
  )

  /**
   * Build the Menu List
   */
  menuList = () => {
    // Determine which menu to use - authenticated user menu or unauthenicated version?
    let menu = this.props.unauthMenu;
    if (this.props.user && this.props.user.email) menu = this.props.authMenu;
    return menu.map(item => this.menuItem(item))
  }

radiusChange(value){
     this.props.updateRadius(value)
     this.props.getActivities()
}

onChanged(text){
  let newText = '';
  let numbers = '0123456789';

  for (var i=0; i < text.length; i++) {
      if(numbers.indexOf(text[i]) > -1 ) {
          newText = newText + text[i];
      }
      else {
          // your call back function
          alert("please enter numbers only");
      }
  }
  this.setState({ myNumber: newText });
}
  //@todo menu JSX needs refactoring
  render = () => {
    this.props.authMenu[0].title = this.state.authMenu_0
    this.props.authMenu[1].title = this.state.authMenu_1

    this.props.unauthMenu[0].title = this.state.unauthMenu_0
    this.props.unauthMenu[1].title = this.state.unauthMenu_1
    const { user, localLanguage } = this.props
    const { catFilterTitle, ageTitle, minPTitle, maxPTitle, loggedInAs, email, uid, radius, minPrice, maxPrice, filter, agefilter} = this.state
return(
  <LinearGradient colors={['#008a9e', '#1c7a9f']} style={{flex:1}} start={{x: 0.0, y: 0.1}} end={{x: 1.5, y: 1.0}}>


    <View style={[styles.container]}>
      <View style={[styles.backgroundFill]} />

        <View style={[styles.menuContainer]}>
          <View style={[styles.menu]}>


        {this.props.user && this.props.user.email ?
          <View>

            <View style={[styles.menuItem], { marginTop: 5 }}>
            {/* Translate - Done - CheckLogin */}
              <Text style={[styles.menuLabel]}>
              {catFilterTitle}
              </Text>
              <ModalSelector
                  data={catsData}
                  scrollViewAccessibilityLabel={'Scrollable options'}
                  cancelButtonAccessibilityLabel={'Cancel Button'}
                  onChange={(option)=>{
                    console.log(option.key + " filtery " + CATEGORIES[option.key])
                    this.props.updateUserData(option.label,'filter')
                    this.props.getActivities()

                  }}>
                  <TextInput
                      style={{borderWidth:2, borderColor:'rgba(255,255,255,0.3)',borderRadius:5, padding:10, height:40, color: 'white'}}
                      editable={false}
                      placeholder="Select a category"
                      placeholderTextColor="white"
                      value={this.state.categoryTextValue} />
              </ModalSelector>
            </View>

            <View style={[styles.menuItem]}>
            {/* Translate - Done - CheckLogin */}
              <Text style={[styles.menuLabel]}>
              {ageTitle}
              </Text>
               <ModalSelector
                   data={ageData}
                   scrollViewAccessibilityLabel={'Scrollable options'}
                   cancelButtonAccessibilityLabel={'Cancel Button'}
                   onChange={(option)=>{
                     console.log("ModalDropdown Got Age " + option.key)
                     this.props.updateUserData(option.key,'agefilter')
                     this.props.getActivities()
                   }}>
                   <TextInput
                   style={{borderWidth:2, borderColor:'rgba(255,255,255,0.3)',borderRadius:5, padding:10, height:40, color: 'white'}}
                   editable={false}
                       editable={false}
                       placeholder="Select a age"
                       placeholderTextColor="white"
                       value={this.state.ageTextValue} />
               </ModalSelector>
            </View>

              <View style={[styles.menuItem], {flexDirection: 'row', justifyContent: 'space-between', alignItems:'center', marginBottom:20}}>
                <View style={{flex:1, marginRight: 10}}>
                 {/* Translate - Done - CheckLogin */}
                  <Text style={[styles.menuLabel]}>
                  {minPTitle}
                  </Text>
                  <TextInput
                    style={{color: 'white'}}
                    keyboardType= 'numbers-and-punctuation'
                    borderBottomWidth= {1}
                    borderColor= 'rgba(255,255,255,0.3)'
                    onChangeText={(text) => {
                      this.props.upDatePriceRange({text}, "min")
                      this.onChanged(text)
                    }}
                    value={`${minPrice}`}
                    defaultValue={`${minPrice}`}
                    onSubmitEditing={res => this.props.getActivities()}
                  />
                </View>

                <View style={{flex:1}}>
                {/* Translate - Done - CheckLogin */}
                  <Text style={[styles.menuLabel]}>
                  {maxPTitle}
                  </Text>
                  <TextInput
                    style={{color: 'white'}}
                    keyboardType= 'numbers-and-punctuation'
                    borderBottomWidth= {1}
                    borderColor= 'rgba(255,255,255,0.3)'
                    onChangeText={(text) => {
                      this.props.upDatePriceRange({text}, "max")
                      this.onChanged(text)
                    }}
                    value={`${maxPrice}`}
                    defaultValue={`${maxPrice}`}
                    onSubmitEditing={res => this.props.getActivities()}
                  />
                </View>
              </View>




            <View style={[styles.menuItem]}>
              {/* Translate - Done - CheckLogin */}
              <Text style={[styles.menuLabel]}>
              Radius ({radius} km)
              </Text>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems:'center', marginTop:20 }}>
                {/* <Icon
                  name='md-radio-button-on'
                  type='ionicon'
                  color='#fff'
                  style={{flex:1, width: 10 ,  paddingRight: 10}}
                /> */}

                  <Slider
                    style={{ flex: 8}}
                    step={1}
                    maximumValue={1000}
                    minimumTrackTintColor='rgba(255,255,255,0.3)'
                    onSlidingComplete={this.radiusChange.bind(this)}
                    value={radius}
                  />

                  {/* <Icon
                    name='ios-radio-button-on'
                    type='ionicon'
                    color='#fff'
                    style={{flex:1,  width: 10, paddingLeft: 10 }}
                  /> */}
              </View>
            </View>
        </View>
        :
        <View></View>
    }

        </View>
        <View style={{marginLeft:20}}>
          {this.menuList()}
          </View>
        <View style={[styles.menuBottom]}>
          {this.props.user && this.props.user.email ?
            <View>
              <Text
                style={[
                  styles.menuBottom_text,
                  AppStyles.textCenterAligned,
                ]}
              >
                {/* Translate - Done - CheckLogin */}
                {loggedInAs}
                {'\n'}
                {email}
              </Text>

              <Spacer size={10} />

              <View style={[AppStyles.paddingHorizontal, AppStyles.paddingVerticalSml]}>
                <Button small
                 backgroundColor={'rgba(37,135,154,0.8)'}
                title={'Log Out'} onPress={this.logout} />
              </View>
            </View>
          :
            <View style={[AppStyles.paddingHorizontal, AppStyles.paddingVerticalSml]}>
              <Button small
              // backgroundColor={'rgba(255,255,255,0.2)'}
              title={'Log In'} onPress={() => this.onPress(Actions.login)} />
            </View>
          }
        </View>
      </View>

    </View>
      </LinearGradient>
)
}

}

/* Export Component ==================================================================== */
export default Menu;
