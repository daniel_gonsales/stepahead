/**
 * Menu Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

// Actions
import * as UserActions from '@redux/user/actions';
import * as ActivityActions from '@redux/activities/actions'
import * as Translate from '@redux/translate/actions'

// The component we're mapping to
import MenuRender from './MenuView';

// Authenticated User Menu
const authMenu = [
  { title: ' ', onPress: () => { Actions.updateProfile(); } },
  { title: ' ', onPress: () => { Actions.passwordReset(); } },
];

// Unauthenticated User Menu
const unauthMenu = [
  { title: 'Login', onPress: () => { Actions.login(); } },
  { title: 'Sign Up', onPress: () => { Actions.signUp(); } },
];


// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  user: state.user,
  unauthMenu,
  authMenu,
  category: state.category,
  localLanguage: state.translate.localLanguage,
  menuViewText: state.translate.menuViewText
});

// Any actions to map to the component?
const mapDispatchToProps = {
  logout: UserActions.logout,
  getActivities: ActivityActions.getActivities,
  getCategory: ActivityActions.getCategory,
  nearMe: ActivityActions.nearMe,
  upDatePriceRange: UserActions.upDatePriceRange,
  updateRadius: UserActions.updateRadius,
  getLanguage: ActivityActions.getLanguage,
  changeLanguage: Translate.changeLanguage,
  updateUserData: UserActions.updateUserData,
  getUserData: UserActions.getUserData
};

export default connect(mapStateToProps, mapDispatchToProps)(MenuRender);
