/**
 * Whole App Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { connect } from 'react-redux';
import SideMenu from 'react-native-side-menu';
import { DefaultRenderer } from 'react-native-router-flux';

// Consts and Libs
import { AppSizes } from '@theme/';
import * as ActivityActions from '@redux/activities/actions'

// Containers
import Menu from '@containers/ui/Menu/MenuContainer';
/* Redux ==================================================================== */
import * as SideMenuActions from '@redux/sidemenu/actions';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  sideMenuIsOpen: state.sideMenu.isOpen,
});

// Any actions to map to the component?
const mapDispatchToProps = {
  toggleSideMenu: SideMenuActions.toggle,
  closeSideMenu: SideMenuActions.close,
  getActivities: ActivityActions.getActivities
};

/* Component ==================================================================== */
class Drawer extends Component {
  static componentName = 'Drawer';

  static propTypes = {
    navigationState: PropTypes.shape({}),
    onNavigate: PropTypes.func,
    sideMenuIsOpen: PropTypes.bool,
    closeSideMenu: PropTypes.func.isRequired,
    toggleSideMenu: PropTypes.func.isRequired,
  }

  static defaultProps = {
    navigationState: null,
    onNavigate: null,
    sideMenuIsOpen: null,
  }


  /**
    * Toggle Side Menu
    */
  onSideMenuChange = (isOpen) => {
    if (isOpen !== this.props.sideMenuIsOpen) {
      this.props.toggleSideMenu();
    }
  }

  componentWillMount(){
    console.log('ChildrensState ' + JSON.stringify(this.props.navigationState))
  }

  render() {
    console.log('ChildrensState rneder' + JSON.stringify(this.props.navigationState))

    const state = this.props.navigationState;
    const children = state.children;

    return (
      <SideMenu
        ref={(a) => { this.rootSidebarMenu = a; }}
        openMenuOffset={AppSizes.screen.width * 0.75}
        menu={
          <Menu
            closeSideMenu={this.props.closeSideMenu}
            ref={(b) => { this.rootSidebarMenuMenu = b; }}
          />
        }
        menuPosition={"right"}
        isOpen={this.props.sideMenuIsOpen}
        onChange={this.onSideMenuChange}
        disableGestures
      >

        <View style={{ backgroundColor: 'transparent', flex: 1 }}>
          <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
        </View>
      </SideMenu>
    );
  }
}

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(Drawer);
