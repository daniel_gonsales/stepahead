/**
 * Activity View Screen
 *  - The individual activity screen
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  List,
  ListView,
  ListItem,
  Animated,
  ScrollView
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Thumbnail, CardItem } from 'native-base';

import { Actions } from 'react-native-router-flux';

import LinearGradient from 'react-native-linear-gradient';

// Consts and Libs
import { AppStyles, AppSizes } from '@theme/';

// Components
import { Card, Text } from '@ui/';

import FastImage from 'react-native-fast-image'

/* Styles ==================================================================== */
const styles = StyleSheet.create({
  favourite: {
    position: 'absolute',
    top: -150,
    right: 25,
  },
  container: {
    flex: 1,
    marginTop: -180,
    flex: 1,
    height: 340,
    // alignItems: 'stretch',
    // marginTop:-16,
    // marginLeft: 10,
    // marginRight: 10,
    // marginBottom: 5,
    backgroundColor: 'transparent'
  },
});

const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

/* Component ==================================================================== */
class ActivityCard extends Component {
  static componentName = 'ActivityCard';



  static propTypes = {
    image: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    category: PropTypes.string.isRequired,

    //user: PropTypes.string.isRequired,

    onPress: PropTypes.func,
    onPressFavourite: PropTypes.func,
    isFavourite: PropTypes.bool,
  }

  static defaultProps = {
    onPress: null,
    onPressFavourite: null,
    isFavourite: null,
  }

  componentWillReceiveProps(props){
    console.log('CardViewActivitiesReceiveProps ' + JSON.stringify(props))
    this.setState({
      location: props.location,
      category: props.category,
      title: props.title,
      body: props.body,
      image: props.image,
      author: props.author,
      fullName: props.fullName,
      avatar: props.avatar,
      timeline: props.timeline,
      moreButt: false

    })    

  }

  componentWillMount(){
    console.log('CardViewAvatar ' + JSON.stringify(this.props.avatar))
    console.log('CardViewActivities ' + JSON.stringify(this.props.activities))    
  }

  constructor(props) {
    super(props);
console.log('constructorCardView ' + JSON.stringify(this.props.timetable))
console.log('constructorCardView ' + JSON.stringify(this.props))

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      dataSource: ds.cloneWithRows(this.props.timetable),
      location: this.props.location,
      category: this.props.category,
      title: this.props.title,
      body: this.props.body,
      image: this.props.image,
      author: this.props.author,
      fullName: this.props.fullName,
      avatar: this.props.avatar,
      timetable: this.props.timetable
    };
    
}

getMarker(category){
  switch(category){
    case'Falling':{
      return('lemon-o')
    }
    case'Jumping':{
      return('leaf')
    }
    case'Running':{
      return('truck')
    }
    default:{
      return('random')
    }
  }
}

getTimetable(timetable){
  return(timetable.map(item => (

      <ScrollView key={Math.random()} style={{}}>
      {/* <Animated.View style={{
        marginTop: 5,
        marginBottom: 0,
        // backgroundColor:'#278ba1'
      }}> */}

        <TouchableOpacity
        activeOpacity={0.5}
        onPress={(tit) => {
           // console.log('TimelineWasPressed ' + rowData)
    
    
          //this.props.updateInterestedList(rowData, title, this.props.activities)
          // this.props.updateActivitiesAfter(this.props.activities)
    
        }}
        // color={'#C2185B'}
        // onPress={this.props.updateInterestedList(rowData)}
        // style={isInterested ? { 
        //   marginRight:10,
        //   alignSelf:'center',
        //   // alignItems:'center'
        //   backgroundColor: '#D3D3D3'
    
        //         }
        //         :
        //         {backgroundColor: '#FFFFFF'}
    
        //       }
      >
    <Card
    containerStyle={{
     borderRadius:40,
     borderWidth:5,
     overflow: 'hidden',
     borderColor:'transparent',
    }}
    >
            <View
              style={{
                flexDirection:'row',
                alignItems:'center',
                flex: 1
                // backgroundColor: isInterested ? '#C2185B' : '#FFFFFF',
              }}
            >
    
            {/* <View
              style={isInterested ? {
                flexDirection:'row',
                alignItems:'center',
                backgroundColor: '#D3D3D3'
              }
            :
              {
                flexDirection:'row',
                alignItems:'center',
                color: 'red'
              }
            }
            >               */}
    
              <View
                style={{
                  // flexDirection:'row'
                  alignItems:'center',
                  paddingLeft:20

                }}
              >
                <Text
                  style={{
                    alignItems: 'center',
                  }}
                >
                  {new Date (item.starTime).getDate()} 
                </Text>
                <Text style={{
                  color: 'red'
                  }}
                >
                  {monthNames[new Date (item.starTime).getMonth()]} 
                </Text>
              </View>
    
              <View
                style={{
                  marginLeft: 20,
                }}
              >
              <Text
                style={{
                  fontWeight: 'bold',
                }}                
              >
                {new Date (item.starTime).getHours()}:{new Date (item.starTime).getMinutes()} - {new Date (item.endTime).getHours()}:{new Date (item.endTime).getMinutes()}
              </Text>
              <Text>
                {item.title}
              </Text>
              </View>
              <View
              style={{
                paddingRight: 20,
                flex: 1,
                alignItems: 'flex-end',
                // position: 'absolute', // add if dont work with above
              }}
              >
            {/* <TouchableOpacity */}
            {/* activeOpacity={0.8}
            onPress={ths => {
              // console.log('settings editButt ' + JSON.stringify(ths))
              // this.onPressSettings()
            }}
            // onPress={thiz => console.log('settings editbutt ' + thiz)}
            style={[styles.favourite]}
          > */}
    
    
          {/* {rowData.going ? 
                          <Icon
                          // raised
                          type='FontAwesome'
                          name={'check'}
                          // color={editButt ? '#FFFFFF' : '#C2185B'}
                          color={'#C2185B'}
                          // containerStyle={{
                          //   // backgroundColor: editButt ? '#C2185B' : '#FFFFFF',
                          //   // backgroundColor: '#FFFFFF'
          
                          // }}
                        />
            :
            null
            } */}
            
    
          {/* </TouchableOpacity> */}
              </View>
            </View>
          </Card>
          </TouchableOpacity>
    
  
    

      {/* </Animated.View> */}
  
    </ScrollView>
    

  )))
}


  render = () => {
    const { category, title, body, image, location, author, fullName, avatar, timetable, id, moreButt } = this.state;
    let { onPress, onPressFavourite, isFavourite, isInterested, activity, favourites, user} = this.props
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  ];
  console.log('POOOPOOOP')

    return (
      <Animated.View>
      {this.setState.moreButt = false}
      {/* {this.props.moreButt[id] = } */}
    <Card
    containerStyle={{
      borderRadius:40,
      borderWidth:10,
      overflow: 'hidden',
      borderColor:'transparent',
      // padding:40,
      // backgroundColor:'black'
    }}
    // style={{
    //   borderRadius:30,
    //   borderWidth:10,
    //   overflow: 'hidden',
    //   borderColor:'transparent',              
    // }}
  >
  {/* <CardItem> */}
    <View style={{
      flex: 1,
      height:170,
      alignItems: 'stretch',
      marginTop:-16,
      marginLeft:-16,
      marginRight:-16,
      marginBottom: 5,


    }}>
      <FastImage
        style={{
          flex: 1,
          backgroundColor: 'transparent',
          overflow: 'hidden',
          // width:300
         }}
        source={{
          uri: image,
          priority: FastImage.priority.high,
        }}
        resizeMode={FastImage.resizeMode.cover}
        // onLoad ={() => console.log('IMAGELOADED')} 
      />

      {JSON.stringify(this.props.user) != '{}'  && author != this.props.user.uid ?
      <View>
      {/* {!!this.props.isFavourite(item, favourites, user) && */}
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={()=>onPressFavourite }
          style={[styles.favourite]}
        >
          <Icon
            raised
            type='font-awesome'
            name={'heart'}
            color={isFavourite ? '#FFFFFF' : '#C2185B'}
            containerStyle={{
              backgroundColor: isFavourite ? '#C2185B' : '#FFFFFF',
            }}
          />
        </TouchableOpacity>
      {/* } */}
      </View>             
        : 
        null
        }

    </View>

    <View style={{
                      // borderBottomEndRadius:40,
                      borderBottomLeftRadius:40,
                      borderBottomRightRadius:40,
                      borderWidth:10,
                      overflow:'hidden',
                      borderColor:'transparent',
                      flex:1
    }}>
      <View style={{
        flex: 1,
        flexDirection: "row",
        justifyContent: 'flex-end',
        marginTop: 10,
        marginLeft: 25,
        marginRight: 25
      }}>
        <Icon
          type='font-awesome'
          name={this.getMarker(category)}
          color="#00e9bf"
          size={25}
          style={{flex:1,justifyContent: 'flex-start'}}
        />
        <View style={{ flex: 5,  alignItems: 'stretch',marginLeft:12}}>
          <Text h4 numberOfLines={1} ellipsizeMode='tail' >{title}</Text>
          {/* Translate */}
          <Text>{category}</Text>
        </View>
        <View style={{
          flex:1,
          flexDirection: "row",
          justifyContent: 'flex-end'
        }}>

        {JSON.stringify(this.props.user) != '{}' && author != this.props.user.uid && JSON.stringify(this.props.user) != '{}' ?              
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => { Actions.conversations([author, fullName, avatar]) } }

            style={{
              // marginRight:20,
              // marginLeft: -20,
              // borderRightWidth:20,
              borderColor:'transparent',
              alignSelf:'center',
              alignItems:'center'
            }}
          >
            <Icon
              name={'commenting-o'}
              type='font-awesome'
              size={30}
              color="#333333"

            />
          </TouchableOpacity>
          :
          null}
            {/* SPACER */}
            <View
              style={{
                left: 0,
                right: 0,
                height: 1,
                marginRight: 20,
              }}
            />
            {/* --- --- --- */}
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => {
              Actions.timeline([location])
              console.log('onRegionChange call Map ' + JSON.stringify(location))
            }}
            style={{
              borderColor: 'transparent',
              alignSelf:'center',
              alignItems:'center'
            }}
          >
            <Icon
              name={'map-marker'}
              type='font-awesome'
              color="#333333"
              size={30}
            />
          </TouchableOpacity>

          
        </View>

        
      </View>
      {/* <View
        style={{
          borderBottomColor: 'lightgray',
          borderBottomWidth: 1,
          marginTop: 10,
          marginBottom: 10,
        }}
      /> */}

      <View
        style={{
          borderBottomColor: 'lightgray',
          borderBottomWidth: 1,
          marginTop: 10,
          marginBottom: 10,
          marginLeft: 25,
          marginRight:25
        }}
      />

        <Text style={{paddingRight:25, paddingLeft:25, paddingBottom: 10,                                borderBottomLeftRadius:40,
                      borderBottomRightRadius:40,
                      borderWidth:10,
                      overflow:'hidden',
                      borderColor:'transparent',
                      flex:1}}>{body.substring(0,100)}...</Text>


    </View>
    {/* </CardItem> */}
  </Card>
          {/* <ScrollView> */}

  {/* { this.state.moreButt ? <View> {this.getTimetable(item.timetable)} 
</View>
   :         
  <Icon
name={'ios-arrow-dropdown-circle'}
// raised
// name={'ios-arrow-down-outline'}
type='ionicon'
color='white'
size={45}
onPress={()=> this.setState({moreButt: !this.state.moreButt})}

/>   } */}

{/* {this.setState({moreButt:item})} */}

{this.state.moreButt ? <View>{this.getTimetable(timetable)}</View> : null}

{/* // {console.log('popeoojsgs ' + this.props.activities.forEach(item => console.log('buttses ' + id + ' ' + item.moreButt)))} */}


{JSON.stringify(this.props.user) != '{}' && this.props.user.uid != author ?
<View>
{!this.state.moreButt ? 
      <Icon
      name={'ios-arrow-dropdown-circle'}
      // raised
      // name={'ios-arrow-down-outline'}
      type='ionicon'
      color='white'
      size={45}
      onPress={()=> this.setState({moreButt: !this.state.moreButt})}
      // onPress={() => this.props.morebutts[id] = true}
    />
    :
    <Icon
    name={'ios-arrow-dropup-circle'}
    // raised
    // name={'ios-arrow-down-outline'}
    type='ionicon'
    color='white'
    size={45}
    onPress={()=> this.setState({moreButt: !this.state.moreButt})}
  
  /> 
}
</View>
:
null 
}


{/* </ScrollView> */}
  </Animated.View>
    );
  }
}

/* Export Component ==================================================================== */
export default ActivityCard;
