/**
 * Individual Activity Card Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

// Actions
import * as ActivityActions from '@redux/activities/actions';
import * as UserActions from '@redux/user/actions';
import * as ChatActions from '@redux/chat/actions';

import { Firebase, FirebaseRef } from '@constants/';

// Components
import ActivityCardRender from './CardView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  activities: state.activities,
  user: state.user,
  //favourites: (state.activity && state.activity.favourites) ? state.activity.favourites : null,
  favourites: state.activity.favourites
});

// Any actions to map to the component?
const mapDispatchToProps = {
  getChat: ChatActions.getChat,
  updateInterestedList: ActivityActions.updateInterestedList,
  updateActivitiesAfter: UserActions.updateActivitiesAfter,
  updateFavourites: ActivityActions.spotGFavs

};


// setting random timetable for activities

// function randomFunc () {
//   console.log('AllActivitiesPromise tim ')
//   const ref = FirebaseRef.child('activities')
//   //*  console.log("AllActivitiesRef " + ref)
//     ref.on('value', snapshot => {
//       snapshot.val().forEach(act => {
//         if(!act.timetable){
//           let date1 = new Date('2018-05-01');
//           let date2 = new Date('2018-11-30');
//           console.log('AllActivitiesPromise time ')

//           console.log('AllActivitiesPromise time ' + randDate(date1, date2))

//           let refy = FirebaseRef.child(`activities/${act.id}/timetable/`)
//           let stime = [randDate(date1,date2),randDate(date1,date2),randDate(date1,date2)]
//           let t0 = stime[0].getHours() + 2
//           let t1 = stime[1].getHours() + 2
//           let t2 = stime[2].getHours() + 2
//           let etime = [stime[0],stime[1],stime[2]]
//           // let etime = [t0/2,t1/2,t2/2]
//           etime[0].setHours(t0)
//           etime[1].setHours(t1)
//           etime[2].setHours(t2)

//           console.log('stime ' + stime[0] + ' endtime ' + etime[0].setHours(t0))


//           refy.child(0).set({starTime: `${stime[0]}`, endTime: `${etime[0]}` , price:  Math.random() , title: 'Introduction'})
//           refy.child(1).set({starTime: `${stime[1]}`, endTime: `${etime[1]}` , price:  Math.random() , title: 'Calcucalting volume of a heating unit'})
//           refy.child(2).set({starTime: `${stime[2]}`, endTime: `${etime[2]}` , price:  Math.random() , title: 'Field trip to Mars'})

//           console.log('Setting Timetable ' + JSON.stringify(({date: new Date( Math.random() ), starTime: ['13','00'], endTime:['14','30'] , price:  Math.random()})))

//       }
//       })

//     })
// }

function randDate(start, end){
  let date = new Date(+start + Math.random() * (end - start));
  let date2 = date
  let tt = date2.getHours() + 2
  date2.setHours(2)
  console.log(date + " AllActivitiesPromise " + date2 + " bb " + date2.setFullYear(2066))
  return date;
}

/* Component ==================================================================== */
class ActivityCard extends Component {
  static componentName = 'ActivityCard';

  static propTypes = {
    activity: PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      body: PropTypes.string.isRequired,
      image: PropTypes.string.isRequired,
//      category: PropTypes.number,
    }).isRequired,
//    favourites: PropTypes.object,
    user: PropTypes.shape({
      uid: PropTypes.string,
    }),
  }

  static defaultProps = {
    favourites: null,
    user: null,
    activity:{
      id: "0",
      title: "Title",
      body: "Body",
      image: "Image",
      category: 1
    }
  }

  constructor(props) {
    super(props);
    this.state = { activity: props.activity };
  }

  componentWillReceiveProps(props) {

    if(props.children[1]){
      this.setState({
        activities: props.children[1]
      })
    }
    

    if (JSON.stringify(props.favourites) === null) {
      console.log("poop is undefined")
      this.setState({ 
        favourites: props.favourites[props.user.uid]
     });
    } else {
      this.setState({ 
        favourites: props.favourites
     });
    }

    if (props.activity) {
      this.setState({ 
        activity: props.activity,
     });
    }
  }



  /**
    * On Press of Card
    */
  onPressCard = () => {
    // --- testy stuff start
    
    // randomFunc()

    // let date1 = new Date('2018-04-01');
    // let date2 = new Date('2018-06-30');
    
    // let resDate = randDate(date1, date2)
    // let tTime = resDate.getHours()
    // console.log('resDate month: ' + resDate.getMonth() + ' h: ' + resDate.getHours() + ' h + 2: ' + (tTime + 2) + ' min: ' + resDate.getMinutes())

    // console.log('AllActivitiesPromise time ')

    // console.log('AllActivitiesPromise time ' + randDate(date1, date2))

    // --- testy stuff end
    
    console.log('timetable tt 0 ' + JSON.stringify(this.props.activity.timetable))
    console.log('timetable tt 0 ' + JSON.stringify(this.props.activity.timetable[0].starTime))

    
    Actions.activityView({
      title: this.props.activity.title,
      activity: this.props.activity,
      user: this.props.user,
      localLanguage: this.props.localLanguage
    });
  }

  /**
    * When user taps to favourite a activity
    */
  onPressFavourite = () => {
    this.props.updateFavourites(this.props.activity, this.isFavourite())
  }

  /**
    * Check in Redux to find if this Activity ID is a Favourite
    */
  isFavourite = () => {
    const { favourites, activity, user} = this.props
    let newFav = false
    
    if (JSON.stringify(favourites) != '{}') {
      console.log("CrashFavsZ " + JSON.stringify(favourites))
      console.log("CrashFavsZ0 " + JSON.stringify(favourites[0]))
      
      favourites.forEach(fav => {
        console.log('FavsGGCompare ' + fav.id + ' ' + activity.id)
        if (fav.id === activity.id){
          newFav = true
        }
      })
      console.log('CrashFavsZz ' + JSON.stringify(newFav))
      return newFav
    } else {
      return
    }

  }

  // isInterested = () => {
  //   console.log('cardreceivepropsISISINteerested ' + JSON.stringify(this.props.user.interestedList))
  //   // console.log('cardreceivepropsISISINteerested ' + JSON.stringify(this.props.user.interestedList[0].classTitle))

  //   const {activity, user} = this.props
  //   let newInterest = false
  //   // console.log("CrashFavs " + JSON.stringify(favourites[0]) + " " + favourites[0])
  //   if (user && user.uid) {
  //     // console.log('cardddd ' + user.interestedList[0].classTitle)
  //     user.interestedList.forEach(inter => {
  //       console.log("isInterestedcards inter " + JSON.stringify(inter))
  //       if (inter.classTitle === activity.title){
  //         console.log("isInterestedcards " + JSON.stringify(inter.classTitle))

  //         activity.timetable.forEach(table =>{
  //           console.log(JSON.stringify(table.title) + " isInterestedcards " + JSON.stringify(inter.title))

  //           if(table.title === inter.title){
  //             console.log("isInterestedcards true " + JSON.stringify(inter))
  //             newInterest = true

  //           }
  //         })
  //       }
  //     })
  //     console.log('isInterestedcards is ' + newInterest)
  //     return newInterest
  //   } else {
  //     console.log('isInterestedcards is ' + newInterest)
  //     return
  //   }

  // }

  render = () => {
    const { activity } = this.state;
    const { user } = this.props;

    return (
      <ActivityCardRender
        title={activity.title}
        body={activity.body}
        image={activity.image}
        category={ activity.category}
        user={user}
        author={activity.author}
        fullName={activity.fullName}
        onPress={this.onPressCard}
        onPressFavourite={(user && user.uid) ? this.onPressFavourite : null}
        isFavourite={(user && user.uid && this.isFavourite()) && true}
        location={activity.location}
        avatar={activity.avatar}
        timetable={activity.timetable}
        updateInterestedList={this.props.updateInterestedList}
        id={activity.id}
        updateActivitiesAfter={this.props.updateActivitiesAfter}
        activities={this.state.activities}

      />
    );
  }
}

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(ActivityCard);