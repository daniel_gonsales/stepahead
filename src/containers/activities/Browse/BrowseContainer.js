/**
 * Activity Tabs Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { connect } from 'react-redux';

// The component we're mapping to
import ActivityTabsRender from './BrowseView';
import * as ActivityActions from '@redux/activities/actions'
import * as UserActions from '@redux/user/actions'

// What data from the store shall we send to the component?
const mapStateToProps = state => ({  
  category: state.activity.categories || [], //category
  user: state.user,
  filter: state.user.filter,
  activities: state.activity.activities
});


// Any actions to map to the component?
const mapDispatchToProps = {
  getActivities: ActivityActions.getActivities,
  updateActivitiesAfter: UserActions.updateActivitiesAfter

};

export default connect(mapStateToProps, mapDispatchToProps)(ActivityTabsRender);
