/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  InteractionManager,
  TouchableHighlight,
  FlatList,
  Animated
} from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';

// Consts and Libs
import { AppColors, AppSizes } from '@theme/';

// Containers
import ActivityListing from '@containers/activities/Listing/ListingContainer';
import ActivityCard from '@containers/activities/Card/CardContainer';

import LinearGradient from 'react-native-linear-gradient';
import SearchInput, { createFilter } from 'react-native-search-filter';
import { SearchBar } from 'react-native-elements'


// Components
import { Text } from '@ui/';
import Loading from '@components/general/Loading';

/* Styles ==================================================================== */
const styles = StyleSheet.create({
  // Tab Styles
  tabContainer: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: '#258ba1',
  },
  tabbarIndicator: {
    backgroundColor: '#FFF',
  },
  tabbarText: {
    color: '#FFF',
  },
  searchInput:{
    padding: 10,
    // borderColor: '#248ca0',
    borderColor: '#FFFFFF', 
    backgroundColor: '#FFFFFF', 
    borderWidth: 10,

    borderRadius: 10,
    // overflow: 'hidden',
    // borderColor:'transparent',
  }  
});

/* Component ==================================================================== */
let loadingTimeout;
class ActivityTabs extends Component {
  static componentName = 'ActivityTabs';

  static propTypes = {
    category: PropTypes.arrayOf(PropTypes.object).isRequired,
    filter: PropTypes.string,
  }

  static defaultProps = {
    category: [],
    user: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      visitedRoutes: [],
    };
  }


  /**
    * Wait until any interactions are finished, before setting up tabs
    */

  componentDidMount = () => {
    console.log('popopppopopopopopop')

    InteractionManager.runAfterInteractions(() => {
      this.setTabs();
    });

  }

  componentWillReceiveProps(props){
    console.log('popopppopopopopopop')

    if (JSON.stringify(props.activities[0]) === '{}'){
      console.log("BrowseView.props.activities are dundefined " + JSON.stringify(props.activities))
    }
  }

  componentWillMount(){

    if(this.props.user){
      console.log('BrowseView Say User is ' + this.props.user.fullName)
    }
  }

  componentWillUnmount = () => clearTimeout(loadingTimeout);

  /**
    * When category are ready, populate tabs
    */
  setTabs = () => {
    const routes = [];
    let idx = 0;
    this.props.category.forEach((category) => {

        routes.push({
          key: idx.toString(),
          id: category.id.toString(),
          title: category.title,
        });

      idx += 1;
    });

    this.setState({
      navigation: {
        index: 0,
        routes,
      },
    }, () => {
      // Hack to prevent error showing
      loadingTimeout = setTimeout(() => {
        this.setState({ loading: false });
      }, 100);
    });
  }

  /**
    * On Change Tab
    */
  handleChangeTab = (index) => {
    this.setTabs();
    this.setState({
      navigation: { ...this.state.navigation, index },
    });
  }

  /**
    * Header Component
    */
  renderHeader = props => (
    <TabBar
      {...props}
      style={styles.tabbar}
      indicatorStyle={styles.tabbarIndicator}
      /* Translate */
      renderLabel={scene => (
        <Text style={[styles.tabbarText]}>
        {this.props.localLanguage === 'English' ? 'NearMe' : 'Netalu no manis'}
        {/* {scene.route.title} */}
        </Text>
      )}
    />
  )

  componentWillMount = () => {
    //console.log("BrowseView mounted ")
    if (this.props.filter === undefined) {
      this.props.filter = 0
    }
  }

  /**
    * Which component to show
    */
  renderScene = ({ route }) => {
    // For performance, only render if it's this route, or I've visited before
    if (route === undefined){
      //Translate 
      route = {"key":"0","id":"0","title":"All"}

    }

    return (
      <View style={styles.tabContainer}>

          <ActivityListing
          /* Translate */
            category={route.title}
          />
          

      </View>
    );
  }

  searchUpdated(search) {
    console.log("Searching for " + search)
    this.setState({
      search: search
    })

    console.log('I do not leak!');
    console.log("ListingView Search " + search + " " + this.state.search)
    // if (search === this.state.search) {
      console.log("ListingView Search Res " + search + " " + this.state.search)
        this.props.getActivities(search)
    // }

      // setTimeout(() => {
      //   console.log('I do not leak!');
      //   console.log("ListingView Search " + search + " " + this.state.search)
      //   if (search === this.state.search) {
      //     console.log("ListingView Search Res " + search + " " + this.state.search)
      //       this.props.getActivities(search)
      //   }
      // }, 2500) 
  }

  render = () => {
    const { user } = this.props
    const { search } = this.state
    if (this.state.loading || !this.state.navigation) return <Loading />;
    return (
      <LinearGradient
      colors={['#008d9a', '#2f6ba1']}
      style={{height:AppSizes.screen.height}}
      start={{x: 0.5, y: 0.3}}
      end={{x: 0.7, y: 1.0}}
    >
      <View>

      <SearchBar
        round
        showLoading
        icon={{ type: 'material', color: 'white', name: 'search' }}
        containerStyle={{
          backgroundColor: 'rgba(0,0,0,0)',
          borderBottomColor: 'transparent',
          borderTopColor: 'transparent',
          borderLeftColor: 'transparent',
          borderRightColor: 'transparent',
          justifyContent: 'center',
          borderLeftWidth:10,
          borderRightWidth:10,

        }}
        inputStyle={{
          backgroundColor:'#239ba7',
          borderColor:'transparent',
          fontSize:18,
          textAlign: 'center',
          color:'white'

        }}
        placeholderTextColor='white'
        onChangeText={(term) => { this.searchUpdated(term) }} 
        placeholder= {search ? search : 'Search'} />
        
      </View>
        <Animated.View style={styles.tabContainer}>
          <ActivityListing
            category={this.props.filter}
          />
        </Animated.View>
      </LinearGradient>
    );
  }
}

/* Export Component ==================================================================== */
export default ActivityTabs;
