/**
 * List of Activities for a Category Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import * as ActivityActions from '@redux/activities/actions';
// Components
import Loading from '@components/general/Loading';
import ActivityListingRender from './ListingView';
import * as MenuView from '../../ui/Menu/MenuView';
import { Firebase, FirebaseRef } from '../../../constants/';


/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
    activities: state.activity.activities || [],
    user: state.user,
    localLanguage: state.translate.localLanguage,
    listingViewText: state.translate.listingViewText,
    favourites: state.activity.favourites 
  });

// Any actions to map to the component?
const mapDispatchToProps = {
    getActivities: ActivityActions.getActivities,
    setSearch: ActivityActions.setSearch,
    updateFavourites: ActivityActions.spotGFavs

};

/* Component ==================================================================== */
class CategoryListing extends Component {
  static componentName = 'CategoryListing';

  static propTypes = {
    //  activities: PropTypes.arrayOf(PropTypes.object),
      getActivities: PropTypes.func.isRequired,
  }

  static defaultProps = {
    activities: [],
  }

  state = {
    loading: false,
    activities: [],
  }


  componentWillMount = () => {
    console.log("Test View Log |||| 2")
    this.fetchActivities(),
    console.log("ListingContainer " + JSON.stringify(this.props))
    console.log("ListingContainerUser " + JSON.stringify(this.props.user))    
    console.log("ListingContainer Will Mount Propsff " + JSON.stringify(this.props.favourites))

    if (JSON.stringify(this.props.user) != 'undefined') {
      if( JSON.stringify(this.props.favourites) === null) {
      console.log("poop is undefined")
      this.setState({ 
        favourites: this.props.favourites[this.props.user.uid]
     });
    } else {
      this.setState({ 
        favourites: this.props.favourites
     });
    }}

    let morebutts = []
    this.props.activities.forEach(item => {
      morebutts[item.id] = false
      console.log('morebutses ' + item.id + ' ' + morebutts[item.id])
    })   

    this.setState({morebutts: morebutts})

  }


  componentDidMount = () => this.getThisCategoriesActivities(this.props.activities);
  //componentWillReceiveProps = props => {this.getThisCategoriesActivities(props.activities)}
  componentWillReceiveProps (props) {
    console.log("Test View Log |||| 3")
    console.log("ListingContainer Will Receive Props " + JSON.stringify(props))
    console.log("ListingContainer Will Receive Props " + JSON.stringify(props.user))
    console.log("ListingContainer Will Receive Propsff " + JSON.stringify(props.favourites))
    console.log("ListingContainer Will Receive State " + JSON.stringify(this.state.favourites))


 
    
    
    let activities = props.activities

    // if (JSON.stringify(props.user) != 'undefined') {
    //   if( JSON.stringify(props.favourites) === null) {
    //   console.log("poop is undefined")
    //   this.setState({ 
    //     favourites: props.favourites[props.user.uid]
    //  });
    // } else {
    //   this.setState({ 
    //     favourites: props.favourites
    //  });
    // }}

    this.setState({
      activities,
      user: props.user,
      loading: false,
    });
  }

  /**
    * Pick out activities that are in the current category
    * And hide loading state
    */
  getThisCategoriesActivities = (allActivities) => {
    console.log("Test View Log |||| 4")
    console.log("ListingContainerPropsAct " + JSON.stringify(allActivities))
    let activities
    if (allActivities.length > 0) {
      activities = allActivities

      this.setState({
        activities,
        loading: false,
      });
    }
  }

  /**
    * Fetch Data from API
    */
    fetchActivities = () => this.props.getActivities()
    .then(() => this.setState({ error: null, loading: false }))
    .catch(err => this.setState({ error: err.message, loading: false }))

    search(inp){
      console.log("ListingContainer search " + inp)
      this.props.getActivities(inp)
    }


    onPressCard = () => {
      // --- testy stuff start
      
      // randomFunc()
  
      // let date1 = new Date('2018-04-01');
      // let date2 = new Date('2018-06-30');
      
      // let resDate = randDate(date1, date2)
      // let tTime = resDate.getHours()
      // console.log('resDate month: ' + resDate.getMonth() + ' h: ' + resDate.getHours() + ' h + 2: ' + (tTime + 2) + ' min: ' + resDate.getMinutes())
  
      // console.log('AllActivitiesPromise time ')
  
      // console.log('AllActivitiesPromise time ' + randDate(date1, date2))
  
      // --- testy stuff end
      
      console.log('timetable tt 0 ' + JSON.stringify(this.props.activity.timetable))
      console.log('timetable tt 0 ' + JSON.stringify(this.props.activity.timetable[0].starTime))
  
      
      Actions.activityView({
        title: this.props.activity.title,
        activity: this.props.activity,
        user: this.props.user,
        localLanguage: this.props.localLanguage
      });
    }
  
    /**
      * When user taps to favourite a activity
      */
    onPressFavourite = ( id ) => {
      console.log("Test View Log |||| 5")
      console.log('onpressitemFavs ' + id + ' ' +   JSON.stringify(this.props.activities))      
      item = this.props.activities.filter(item => item.id === id)
      console.log('onpressitemFavs ' +  this.isFavourite(id) + ' ' + JSON.stringify(item))
      this.props.updateFavourites(item[0], this.isFavourite(id))
    }
  
    /**
      * Check in Redux to find if this Activity ID is a Favourite
      */
    isFavourite = (id) => {
      console.log("Test View Log |||| 6")
      let temp = this.props.favourites.filter(item => item.id === id)
      console.log('filterfavzz ' + JSON.stringify(temp))
      if (temp[0]){
        console.log('AlreadyFavouritete')
        return true
      } else {
        console.log('NewFavourite')        
        return false
      }
  
    }

    openDropdown = (id, option) => {
      console.log("Test View Log |||| 7")
      let temp = this.props.activities.filter(item => item.id === id)
      console.log('statemorbutts ' + this.state.morebutts[id])
      this.state.morebutts[id] = !option

      this.forceUpdate()

    }

    isDown = (id) => {
      console.log("Test View Log |||| 8")
      let temp = this.props.activities.filter(item => item.id === id)
      console.log('isDownTemp ' + id + ' ' + JSON.stringify(temp[0].moreButt))

      console.log('isDownTempState' + this.state.morebutts[id])

      return this.state.morebutts[id]
      }
    


  render = () => {
    if (this.state.loading) return <Loading />;

    return (
      <ActivityListingRender
        activities={this.state.activities}
        language={this.props.localLanguage}
        listingViewText={this.props.listingViewText}
        reFetch={this.fetchActivities}
        search={this.props.getActivities}
        setSearch={this.props.setSearch}
        user={this.props.user}
        isFavourite={this.isFavourite}
        updateFavourites={this.updateFavourites}
        onPressFavourite={this.onPressFavourite}
        onPress={this.onPressCard}
        favourites={this.state.favourites}
        openDropdown={this.openDropdown}
        isDown={this.isDown}
        morebutts={this.state.morebutts}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListing);