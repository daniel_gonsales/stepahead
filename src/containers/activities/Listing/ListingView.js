/**
 * Activity Listing Screen
 *  - Shows a list of activities
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ListView,
  RefreshControl,
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import { Container, Header, DeckSwiper, Text, Left, Body, CardItem, Thumbnail, Card } from 'native-base';
import { Icon } from 'react-native-elements';
import { Actions, ActionConst } from 'react-native-router-flux';
import SearchInput, { createFilter } from 'react-native-search-filter';

import FastImage from 'react-native-fast-image'


// import Card from '@ui/';


// Consts and Libs
import { AppColors, AppStyles, AppSizes } from '@theme/';
import { ErrorMessages } from '@constants/';

// Containers
import ActivityCard from '@containers/activities/Card/CardContainer';
import LinearGradient from 'react-native-linear-gradient';
// Components
import Error from '@components/general/Error';

const styles = StyleSheet.create({
  searchInput:{
    padding: 10,
    borderColor: '#248ca0',
    borderWidth: 0
  },
  favourite: {
    position: 'absolute',
    top: -150,
    right: 25,
  },
  container: {
    flex: 1,
    marginTop: -180,
    flex: 1,
    height: 340,
    // alignItems: 'stretch',
    // marginTop:-16,
    // marginLeft: 10,
    // marginRight: 10,
    // marginBottom: 5,
    backgroundColor: 'transparent'
  },
})

const cards = [
  {
    text: 'Google',
    name: 'Alphabet',
    uri: "https://www.google.co.in/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png",
    description: "Google LLC is an American multinational technology company that specializes in Internet-related services and products, which include online advertising technologies, search engine, cloud computing, software, and hardware."
  },
  {
    text: 'React',
    name: 'Facebook',
    uri: "https://cdn-images-1.medium.com/max/512/1*qUlxDdY3T-rDtJ4LhLGkEg.png",
    description: "React is a JavaScript library for building user interfaces. It is maintained by Facebook, Instagram and a community of individual developers and corporations."
  },
  {
    text: 'NativeBase',
    name: 'GeekyAnts',
    uri: "https://nativebase.io/assets/img/front-page-icon.png",
    description: "NativeBase is an open source framework to build React Native apps over a single JavaScript codebase for Android and iOS."
  },

];

const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

/* Component ==================================================================== */
class ActivityListing extends Component {
  static componentName = 'ActivityListing';

  static propTypes = {
    //activities: PropTypes.arrayOf(PropTypes.object).isRequired,
    reFetch: PropTypes.func,
  }

  static defaultProps = {
    reFetch: null,
  }

  constructor() {
    super();

    this.state = {
      isRefreshing: true,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  }

  componentWillReceiveProps(props) {
    // Translate - Done - Activities Header Title
    console.log("ListingView received props: " + JSON.stringify(props))
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(props.activities),
      isRefreshing: false,
      nothingMsg: 'Looks like there is nothing around',
      title: props.listingViewText.title,
      activities: props.activities
    });
  }

  componentWillMount = () => {
    this.setState({
      activities: this.props.activities,
      moreButt: false
    })
    console.log('ListingUser ' + JSON.stringify(this.props.user))
}

  /**
    * Refetch Data (Pull to Refresh)
    */
  reFetch = () => {
    if (this.props.reFetch) {
      this.setState({ isRefreshing: true });

      this.props.reFetch()
        .then(() => {
          this.setState({ isRefreshing: false });
        });
    }
  }

  searchUpdated(search) {
    console.log("Searching for " + search)
    this.setState({
      search: search
    })
      setTimeout(() => {
        console.log('I do not leak!');
        console.log("ListingView Search " + search + " " + this.state.search)
        if (search === this.state.search) {
          console.log("ListingView Search Res " + search + " " + this.state.search)
          //this.props.setSearch(search)
          this.props.search(search)
          //Actions.activitiesListing()
        }
      }, 2500);    
  }

  getMarker(category){
    switch(category){
      case'Falling':{
        return('lemon-o')
      }
      case'Jumping':{
        return('leaf')
      }
      case'Running':{
        return('truck')
      }
      default:{
        return('random')
      }
    }
  }

  getTimetable(timetable){
    return(timetable.map(item => (

        <ScrollView key={Math.random()} style={{}}>
        {/* <Animated.View style={{
          marginTop: 5,
          marginBottom: 0,
          // backgroundColor:'#278ba1'
        }}> */}

          <TouchableOpacity
          activeOpacity={0.5}
          style={{
            borderRadius:28,
            borderWidth:4,
            overflow: 'hidden',
            borderColor:'transparent',
          }}
          onPress={(tit) => {
             // console.log('TimelineWasPressed ' + rowData)
      
      
            //this.props.updateInterestedList(rowData, title, this.props.activities)
            // this.props.updateActivitiesAfter(this.props.activities)
      
          }}
          // color={'#C2185B'}
          // onPress={this.props.updateInterestedList(rowData)}
          // style={isInterested ? { 
          //   marginRight:10,
          //   alignSelf:'center',
          //   // alignItems:'center'
          //   backgroundColor: '#D3D3D3'
      
          //         }
          //         :
          //         {backgroundColor: '#FFFFFF'}
      
          //       }
        >
      <Card
      containerStyle={{
    //   borderRadius:40,
    //   borderWidth:25,
    //   overflow: 'hidden',
    //   borderColor:'transparent',
      }}
      >
              <View
                style={{
                  flexDirection:'row',
                  alignItems:'center',
                  flex: 1
                  // backgroundColor: isInterested ? '#C2185B' : '#FFFFFF',
                }}
              >
      
              {/* <View
                style={isInterested ? {
                  flexDirection:'row',
                  alignItems:'center',
                  backgroundColor: '#D3D3D3'
                }
              :
                {
                  flexDirection:'row',
                  alignItems:'center',
                  color: 'red'
                }
              }
              >               */}
      
                <View
                  style={{
                    // flexDirection:'row'
                    alignItems:'center',
                    paddingLeft:20

                  }}
                >
                  <Text
                    style={{
                      alignItems: 'center',
                    }}
                  >
                    {new Date (item.starTime).getDate()} 
                  </Text>
                  <Text style={{
                    color: 'red'
                    }}
                  >
                    {monthNames[new Date (item.starTime).getMonth()]} 
                  </Text>
                </View>
      
                <View
                  style={{
                    marginLeft: 20,
                  }}
                >
                <Text
                  style={{
                    fontWeight: 'bold',
                  }}                
                >
                  {new Date (item.starTime).getHours()}:{new Date (item.starTime).getMinutes()} - {new Date (item.endTime).getHours()}:{new Date (item.endTime).getMinutes()}
                </Text>
                <Text>
                  {item.title}
                </Text>
                </View>
                <View
                style={{
                  paddingRight: 20,
                  flex: 1,
                  alignItems: 'flex-end',
                  // position: 'absolute', // add if dont work with above
                }}
                >
              {/* <TouchableOpacity */}
              {/* activeOpacity={0.8}
              onPress={ths => {
                // console.log('settings editButt ' + JSON.stringify(ths))
                // this.onPressSettings()
              }}
              // onPress={thiz => console.log('settings editbutt ' + thiz)}
              style={[styles.favourite]}
            > */}
      
      
            {/* {rowData.going ? 
                            <Icon
                            // raised
                            type='FontAwesome'
                            name={'check'}
                            // color={editButt ? '#FFFFFF' : '#C2185B'}
                            color={'#C2185B'}
                            // containerStyle={{
                            //   // backgroundColor: editButt ? '#C2185B' : '#FFFFFF',
                            //   // backgroundColor: '#FFFFFF'
            
                            // }}
                          />
              :
              null
              } */}
              
      
            {/* </TouchableOpacity> */}
                </View>
              </View>
            </Card>
            </TouchableOpacity>
      
    
      

        {/* </Animated.View> */}
    
      </ScrollView>
      

    )))
  }

  render = () => {
    //const { activities } = this.props;
    const { isRefreshing, dataSource, nothingMsg, activities, search } = this.state;
    const { favourites, user } = this.props
    // const {cards} = this.props

    if(!activities.length > 0){
      return null;
    }

    return (
      <ScrollView>
      <Container style={{
        borderRadius:40,
        borderWidth:10,
        borderBottomEndRadius:40,
        borderBottomLeftRadius:40,
        overflow: 'hidden',
        borderColor:'transparent',
        height: AppSizes.screen.height + 40,
        flex: 1,
        backgroundColor:'transparent'
      }}>
        {/* <View style={{
                          // borderRadius:40,
                          // borderWidth:10,
                          // borderBottomEndRadius:40,
                          // borderBottomLeftRadius:40,
                          // overflow: 'hidden',
                          // borderColor:'transparent',
                          // height: AppSizes.screen.height
                          flex: 1 
        }}> */}
          <DeckSwiper
            dataSource={activities}
            renderItem={item =>
              <Animated.View>
              <Card
              containerStyle={{
                borderRadius:140,
                borderWidth:10,
                overflow: 'hidden',
                borderColor:'transparent',
                // padding:40,
                // backgroundColor:'black'
              }}
              style={{
                borderRadius:30,
                borderWidth:10,
                overflow: 'hidden',
                borderColor:'transparent',              
              }}
            >
            {/* <CardItem> */}
              <View style={{
                flex: 1,
                height:170,
                alignItems: 'stretch',
                marginTop:-16,
                marginLeft:-16,
                marginRight:-16,
                marginBottom: 5,
                backgroundColor: 'transparent',
              }}>
                <FastImage
                  style={{
                    flex: 1
                   }}
                  source={{
                    uri: item.image,
                    priority: FastImage.priority.high,
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                  // onLoad ={() => console.log('IMAGELOADED')} 
                />
    
                {JSON.stringify(this.props.user) != '{}'  && item.author != this.props.user.uid ?
                <View>
                {/* {!!this.props.isFavourite(item, favourites, user) && */}
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={()=>this.props.onPressFavourite(item.id)}
                    style={[styles.favourite]}
                  >
                    <Icon
                      raised
                      type='font-awesome'
                      name={'heart'}
                      color={this.props.isFavourite(item.id) ? '#FFFFFF' : '#C2185B'}
                      containerStyle={{
                        backgroundColor: this.props.isFavourite(item.id) ? '#C2185B' : '#FFFFFF',
                      }}
                    />
                  </TouchableOpacity>
                {/* } */}
                </View>             
                  : 
                  null
                  }
    
              </View>
    
              <View style={{
                                // borderBottomEndRadius:40,
                                borderBottomLeftRadius:40,
                                borderBottomRightRadius:40,
                                borderWidth:2,
                                overflow:'hidden',
                                borderColor:'transparent',
                                flex:1
              }}>
                <View style={{
                  flex: 1,
                  flexDirection: "row",
                  justifyContent: 'flex-end',
                  marginTop: 10,
                  marginLeft: 15,
                  marginRight: 15 
                }}>
                  <Icon
                    type='font-awesome'
                    name={this.getMarker(item.category)}
                    color="#00e9bf"
                    size={25}
                    style={{flex:1,justifyContent: 'flex-start'}}
                  />
                  <View style={{ flex: 5,  alignItems: 'stretch',marginLeft:12}}>
                    <Text h4 numberOfLines={1} ellipsizeMode='tail' >{item.title}</Text>
                    {/* Translate */}
                    <Text>{item.category}</Text>
                  </View>
                  <View style={{
                    flex:1,
                    flexDirection: "row",
                    justifyContent: 'flex-end'
                  }}>
    
                  {JSON.stringify(this.props.user) != '{}' && item.author != this.props.user.uid && JSON.stringify(this.props.user) != '{}' ?              
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => { Actions.conversations([item.author, item.fullName, item.avatar]) } }
    
                      style={{
                        // marginRight:20,
                        // marginLeft: -20,
                        // borderRightWidth:20,
                        borderColor:'transparent',
                        alignSelf:'center',
                        alignItems:'center'
                      }}
                    >
                      <Icon
                        name={'commenting-o'}
                        type='font-awesome'
                        size={30}
                        color="#333333"
    
                      />
                    </TouchableOpacity>
                    :
                    null}
                      {/* SPACER */}
                      <View
                        style={{
                          left: 0,
                          right: 0,
                          height: 1,
                          marginRight: 20,
                        }}
                      />
                      {/* --- --- --- */}
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={() => {
                        Actions.timeline({location: item.location})
                        console.log('onRegionChange call Map ' + JSON.stringify(item.location))
                      }}
                      style={{
                        borderColor: 'transparent',
                        alignSelf:'center',
                        alignItems:'center'
                      }}
                    >
                      <Icon
                        name={'map-marker'}
                        type='font-awesome'
                        color="#333333"
                        size={30}
                      />
                    </TouchableOpacity>
    
                    
                  </View>
    
                  
                </View>
                {/* <View
                  style={{
                    borderBottomColor: 'lightgray',
                    borderBottomWidth: 1,
                    marginTop: 10,
                    marginBottom: 10,
                  }}
                /> */}
    
                <View
                  style={{
                    borderBottomColor: 'lightgray',
                    borderBottomWidth: 1,
                    marginTop: 10,
                    marginBottom: 10,
                    marginLeft: 25,
                    marginRight:25
                  }}
                />
    
                  <Text style={{paddingRight:25, paddingLeft:25, paddingBottom: 10,                                borderBottomLeftRadius:40,
                                borderBottomRightRadius:40,
                                borderWidth:10,
                                overflow:'hidden',
                                borderColor:'transparent',
                                flex:1}}>{item.body.substring(0,100)}...</Text>
    
    
              </View>
              {/* </CardItem> */}
            </Card>
                    {/* <ScrollView> */}

            {/* { this.state.moreButt ? <View> {this.getTimetable(item.timetable)} 
      </View>
             :         
            <Icon
        name={'ios-arrow-dropdown-circle'}
        // raised
        // name={'ios-arrow-down-outline'}
        type='ionicon'
        color='white'
        size={45}
        onPress={()=> this.setState({moreButt: !this.state.moreButt})}
      
      />   } */}

      {this.props.morebutts[item.id] ? <View style={{
                borderRadius:40,
                borderWidth:10,
                borderBottomEndRadius:40,
                borderBottomLeftRadius:40,
                overflow: 'hidden',
                borderColor:'transparent',
      }}>{this.getTimetable(item.timetable)}</View> : null}

      {JSON.stringify(this.props.user) != '{}' && this.props.user.uid != item.author ?
      <View>
       {!this.props.isDown(item.id) ? 
                <Icon
                name={'ios-arrow-dropdown-circle'}
                // raised
                // name={'ios-arrow-down-outline'}
                type='ionicon'
                color='white'
                size={45}
                // onPress={()=> this.setState({moreButt: !this.state.moreButt})}
                onPress={() => this.props.openDropdown(item.id, this.props.isDown(item.id) )}
              
              />
              :
              <Icon
              name={'ios-arrow-dropup-circle'}
              // raised
              // name={'ios-arrow-down-outline'}
              type='ionicon'
              color='white'
              size={45}
              // onPress={()=> this.setState({moreButt: !this.state.moreButt})}
              onPress={() => this.props.openDropdown(item.id, this.props.isDown(item.id) )}

            
            /> 
       }
       </View>
       :
       null 
    }


{/* </ScrollView> */}
            </Animated.View>
            }
          />
        {/* </yView> */}
      </Container>
      </ScrollView>
    );
  }
}

/* Export Component ==================================================================== */
export default ActivityListing;
