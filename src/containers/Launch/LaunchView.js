/**
 * Launch Screen
 *  - Shows a nice loading screen whilst:
 *    - Preloading any specified app content
 *    - Checking if user is logged in, and redirects from there
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  //Image,
  Alert,
  StatusBar,
  StyleSheet,
  ActivityIndicator,
  AsyncStorage,
  ImageBackground

} from 'react-native';
import { Actions } from 'react-native-router-flux';

// Consts and Libs
import { AppStyles, AppSizes } from '@theme/';
import { login } from '../../redux/user/actions';

/* Styles ==================================================================== */
const styles = StyleSheet.create({
  launchImage: {
    width: AppSizes.screen.width,
    height: AppSizes.screen.height,
  },
});

/* Component ==================================================================== */
class AppLaunch extends Component {
  static componentName = 'AppLaunch';

  static propTypes = {
    login: PropTypes.func.isRequired,
    getActivities: PropTypes.func.isRequired,
    getCategory: PropTypes.func.isRequired,
    checkGeoFire: PropTypes.func.isRequired,
    getFavourites: PropTypes.func
  }

  constructor() {
    super();
    console.ignoredYellowBox = ['Setting a timer'];
  }


  componentDidMount = async () => {
    const values = await AsyncStorage.getItem('api/credentials');

    // Show status bar on app launch
    StatusBar.setHidden(false, true);
    

//    let ref = new Firebase("https://stepahead-b61d0.firebaseio.com/")

    // Preload content here
    Promise.all([
    this.props.getActivities(),
    this.props.getCategory(),
    ]).then(res => {
      // Once we've preloaded basic content,
      // - Try to authenticate based on existing token
      this.props.setLocalLanguage(1),
      this.props.login()

      
        // Logged in, show index screen
        .then( res => 
          this.props.getCategory(1)

        .then( res => {
          Actions.app({ type: 'reset' })
        })
        .then(
          this.props.getActivities(),
          this.props.getFavourites(),
          this.props.setLocalLanguage(1),
          this.props.checkGeoFire(),
          // this.props.getConversations(),
          //this.props.getInterestedList(),
          this.props.checkNewMessages()

        ),

        )
        .catch(() => Actions.authenticate({ type: 'reset' }));
    }).catch(err => (Alert.alert(err.message),console.log("Error is " + err.message)));
  }

  render = () => (
    <View style={[AppStyles.container]}>
      <ImageBackground
        source={require('../../images/launch.jpg')}
        style={[styles.launchImage, AppStyles.containerCentered]}
      >
        <ActivityIndicator
          animating
          size={'large'}
          color={'#C1C5C8'}
        />
      </ImageBackground>
    </View>
  );
}

/* Export Component ==================================================================== */
export default AppLaunch;
