/**
 * Authenticate Screen
 *  - Entry screen for all authentication
 *  - User can tap to login, forget password, signup...
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  // Icon,
  TouchableOpacity,
  TouchableHighlight,
  AppRegistry,
  // Text,
} from 'react-native';
import { Icon, Button } from 'native-base';
// import { Icon } from 'native-base';

import { Actions } from 'react-native-router-flux';

// Consts and Libs
import { AppStyles, AppSizes, AppColors } from '@theme/';

// Components
import { Spacer, Text, Button as ButtonUI } from '@ui/';

import LinearGradient from 'react-native-linear-gradient';

/* Styles ==================================================================== */
const styles = StyleSheet.create({
  background: {
    backgroundColor: 'transparent',
    height: AppSizes.screen.height,
    width: AppSizes.screen.width,
  },
  logo: {
    width: AppSizes.screen.width * 0.65,
    resizeMode: 'contain',
  },
  whiteText: {
    color: '#FFF',
  },
  linearGradient: {
    flex: 1
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    padding: 10
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
}
});

/* Component ==================================================================== */
class Authenticate extends Component {
  static componentName = 'Authenticate';

  render = () => (
    <LinearGradient colors={['#507ddf', '#12e8c0']} style={styles.linearGradient} start={{x: 0.0, y: 0.1}} end={{x: 1.5, y: 1.0}}>
      <View style={[AppStyles.containerCentered, AppStyles.container, styles.background]}>
        <Image
          source={require('../../images/sa_logo.png')}
          style={[styles.logo]}
        />

        <View>
        <Button bordered dark
          style={{
            width:260,
            // borderLeftWidth:60,
            // marginLeft:60,
            borderColor: 'rgba(255,255,255,0.2)',
            backgroundColor: 'rgba(255,255,255,0.2)',
            paddingLeft: 60,
            borderRadius:3
          }}
          onPress={Actions.login}
          >
          <View style={{flexDirection:'row', justifyContent:'center'}}>
          <Icon
            type="MaterialIcons"
            name="lock-outline"
            style={{color:'rgba(255,255,255,0.8)'}}

          />
          <View style={{flexGrow: 1, justifyContent:'center'}}>
            <Text style={{
              //fontSize: 15,
              fontWeight: 'bold',
              color:'rgba(255,255,255,0.8)',
            }}>Login</Text>
          </View>
          </View>

        </Button>
        </View>

        <Spacer size={10} />

      <View>
        <Button bordered dark
          style={{
            width:260,
            borderColor:'rgba(255,255,255,0.2)',
            backgroundColor: 'rgba(255,255,255,0.2)',
            paddingLeft:60,
            borderRadius:3,

          }}
          onPress={Actions.signUp}
          >
          <View style={{flexDirection:'row'}}>
          <Icon
        type="MaterialIcons"
        name="group-add"
        style={{color:'rgba(255,255,255,0.8)'}}
          />
          <View style={{flexGrow: 1, justifyContent:'center'}}>
            <Text style={{
              fontSize: 15,
              fontWeight: 'bold',
              color:'rgba(255,255,255,0.8)'
            }}>Sign Up</Text>
          </View>
          </View>

        </Button>
      </View>

        <Spacer size={15} />

        <Text p style={[AppStyles.textCenterAligned, styles.whiteText]}>
          - or -
        </Text>

        <Spacer size={10} />

        <View style={[AppStyles.row, AppStyles.paddingHorizontal]}>
          <View style={[AppStyles.flex1]} />
          <View style={[AppStyles.flex2]}>
            <ButtonUI
              small
              /* Translate */
              title={'Skip'}
              onPress={Actions.app}
              raised={false}
              backgroundColor={'rgba(255,255,255,0.3)'}
            />
          </View>
          <View style={[AppStyles.flex1]} />
        </View>

        <Spacer size={40} />
      </View>
    </LinearGradient>
  )
}

/* Export Component ==================================================================== */
export default Authenticate;
