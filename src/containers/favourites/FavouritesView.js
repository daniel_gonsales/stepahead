/**
 * Activity View Screen
 *  - The individual activity screen
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  ScrollView,
  StyleSheet,
} from 'react-native';

// Consts and Libs
import { AppStyles, AppSizes, Color } from '@theme/';

// Components
import { Card, Spacer, Text } from '@ui/';

import { Button } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
//import * as UserActions from '@redux/user/actions'

import LinearGradient from 'react-native-linear-gradient';
/* Styles ==================================================================== */
const styles = StyleSheet.create({
  featuredImage: {
    left: 0,
    right: 0,
    height: AppSizes.screen.height * 0.2,
    resizeMode: 'cover',
  },
});

// const dayNames = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

/* Component ==================================================================== */
class FavouritesView extends Component {
  static componentName = 'FavouritesView';

  static propTypes = {
    activity: PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      body: PropTypes.string.isRequired,
      category: PropTypes.string.isRequired,
      image: PropTypes.string,
      age: PropTypes.arrayOf(PropTypes.string),
      location: PropTypes.object,
      pice: PropTypes.arrayOf(PropTypes.string),
      author: PropTypes.string,
      fullName: PropTypes.string
    }).isRequired,
  }

  usersOnlyButtons(author, fullName, localLanguage, avatar){
    console.log('useronlybuttons ' + avatar)
    return(
      <View>
        <Spacer size={20} />

        <Button
        large = {false}
        backgroundColor = '#276a90'
        icon={{name: 'commenting-o', type: 'font-awesome'}}
        onPress={function(){ 
          console.log('ChatbuttonpressedActivity ' + author + ' ' + fullName + ' ' + avatar)
          Actions.conversations([author, fullName, avatar])
          
        }}
        // onPress={function(){ Actions.chat([author])}}        
        title={localLanguage === 'English' ? 'Message': 'Rakstit izziņu' } />

        <Spacer size={20} />

        {/**<Button
        large = {false}
        backgroundColor =  "#276a90"
        icon={{name: 'star-o', type: 'font-awesome'}}
        title={localLanguage === 'English' ? 'Reviews': 'Atsauksmes' } />

        <Spacer size={20} />**/}
      </View>
    )
  }

  componentWillReceiveProps(prop){
  }

  componentWillMount(){
  }

  render = () => {
      const { category, author, title, body, image, fullName, localLanguage, timetable, avatar } = this.props.activity;
      const user = this.props.user.uid
    console.log(JSON.stringify(timetable[0].starTime) + " " + author + " ActivityViewprops " + user)
    console.log(JSON.stringify(new Date(timetable[0].starTime).getHours()))
    let time = new Date(timetable[0].starTime)
    return (
      <LinearGradient
      colors={['#008d9a', '#2f6ba1']}
      style={{height:AppSizes.screen.height}}
      start={{x: 0.5, y: 0.3}}
      end={{x: 0.7, y: 1.0}}
    >
      <ScrollView style={[AppStyles.container]}>
        {image !== '' &&
          <Image
            source={{ uri: image }}
            style={[styles.featuredImage]}
          />
        }

        <Card containerStyle={{
            borderRadius:10,
            overflow: 'hidden',
            borderColor:'transparent',
          }}>
          <Text>{fullName}</Text>
          <Text h3>{title}</Text>
          <Text>{body}</Text>
            <Spacer size={10} />

            <Text h3>Description</Text>
            <Text>More description goes here. This is a test description.</Text>
            <Spacer size={10} />

            <Text h3>Timetable</Text>
            <Text>{time.getHours()}:{time.getMinutes()}-{time.getHours() + 1}:{time.getMinutes() < 29 ? <Text> {time.getMinutes()+30} </Text> : <Text>{time.getMinutes()-30}</Text> }</Text>

            {this.props.user && this.props.user.email && this.props.user.uid != author ?
                this.usersOnlyButtons(author, fullName, localLanguage, avatar)
              : console.log("ActivityView Didn't log in")}

        </Card>

      </ScrollView>
      </LinearGradient>
    );
  }
}

/* Export Component ==================================================================== */
export default FavouritesView;