/**
 * Receipe Tabs Screen
 *  - Shows tabs, which contain receipe listings
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  InteractionManager,
  TouchableHighlight,
  FlatList,
} from 'react-native';
import { TabViewAnimated, TabBar } from 'react-native-tab-view';

// Consts and Libs
import { AppColors } from '@theme/';

// Containers
import ActivityListing from '@containers/favourites/Listing/ListingContainer';
import ActivityCard from '@containers/favourites/Card/CardContainer';

import LinearGradient from 'react-native-linear-gradient';

// Components
import { Text } from '@ui/';
import Loading from '@components/general/Loading';

import { Actions } from 'react-native-router-flux';


/* Styles ==================================================================== */
const styles = StyleSheet.create({
  // Tab Styles
  tabContainer: {
    flex: 1,
  },
  tabbar: {
    backgroundColor: '#258ba1',
  },
  tabbarIndicator: {
    backgroundColor: '#FFF',
  },
  tabbarText: {
    color: '#FFF',
  },
});

/* Component ==================================================================== */
let loadingTimeout;
class FavouriteTabs extends Component {
  static componentName = 'FavouriteTabs';

  static propTypes = {
    category: PropTypes.arrayOf(PropTypes.object).isRequired,
    filter: PropTypes.string,
  //  getAvtivities: PropTypes.func
  }

  static defaultProps = {
    category: [],
    user: null,
  }

  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      visitedRoutes: [],
    };
  }


  /**
    * Wait until any interactions are finished, before setting up tabs
    */

  componentDidMount = () => {
    InteractionManager.runAfterInteractions(() => {
      this.setTabs();
    });

  }

  componentWillReceiveProps(props){
    // if (localLanguage === 'English') {
    //   Actions.refresh({title: 'Near Me'}) 
    // } else {
    //   Actions.refresh({title: 'මා ළඟ'}) 
    // }


    console.log(JSON.stringify(this.props))

    console.log("Favourites BrowseView " + JSON.stringify(this.props.languagez))
    if (JSON.stringify(props.activities[0]) === '{}'){
      console.log("BrowseView.props.activities are dundefined " + JSON.stringify(props.activities))
    }
  }

  componentWillUnmount = () => clearTimeout(loadingTimeout);

  /**
    * When category are ready, populate tabs
    */
  setTabs = () => {
    const routes = [];
    let idx = 0;
    this.props.category.forEach((category) => {

        routes.push({
          key: idx.toString(),
          id: category.id.toString(),
          title: category.title,
        });

      idx += 1;
    });

    this.setState({
      navigation: {
        index: 0,
        routes,
      },
    }, () => {
      // Hack to prevent error showing
      loadingTimeout = setTimeout(() => {
        this.setState({ loading: false });
      }, 100);
    });
  }

  /**
    * On Change Tab
    */
  handleChangeTab = (index) => {
    this.setTabs();
    this.setState({
      navigation: { ...this.state.navigation, index },
    });
  }

  /**
    * Header Component
    */
  renderHeader = props => (
    console.log("BrowseViewRouteTitle " + scene.route.title),
    <TabBar
      {...props}
      style={styles.tabbar}
      indicatorStyle={styles.tabbarIndicator}
      /* Translate - ??? */
      renderLabel={scene => (
        <Text style={[styles.tabbarText]}>
        {this.props.localLanguage === 'English' ? scene.route.title : console.log("BrowseViewRouteTitle " + scene.route.title)}
        </Text>
      )}
    />
  )

  componentWillMount = () => {
    console.log("FavouriteView mounted ")
    if (this.props.filter === undefined) {
      this.props.filter = 'All'
    }
    if (JSON.stringify(this.props.user) === '{}'){
      console.log("No user")
      Actions.login()
    }
  }

  render = () => {
    const { localLanguage } = this.props;

    if (this.state.loading || !this.state.navigation) return <Loading />;
    return (
      <LinearGradient
        colors={['#248ca0', '#248ca0']}
        style={{flex: 1, marginBottom: -5}}
        start={{x: 0.0, y: 0.1}}
        end={{x: 1.5, y: 1.0}}
      >
        <View style={styles.tabContainer}>
        {this.props.user.uid ?
                    <ActivityListing
                    category={this.props.filter}
                  />
          : null}

        </View>
      </LinearGradient>
    );
  }
}

/* Export Component ==================================================================== */
export default FavouriteTabs;
