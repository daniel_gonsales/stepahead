/**
 * Activity Tabs Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { connect } from 'react-redux';

// The component we're mapping to
import FavouriteTabsRender from './BrowseView';
import * as ActivityActions from '@redux/activities/actions'

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  // category: state.activity.categories || [], //category
  user: state.user,
  filter: state.user.filter,
  activities: state.activity.activities,
  localLanguage: state.translate.localLanguage,
});


// Any actions to map to the component?
const mapDispatchToProps = {
  getAvtivities: ActivityActions.getAvtivities
};

export default connect(mapStateToProps, mapDispatchToProps)(FavouriteTabsRender);
