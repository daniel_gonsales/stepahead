/**
 * List of Activities for a Category Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import * as ActivityActions from '@redux/activities/actions';
// Components
import Loading from '@components/general/Loading';
import FavouriteListing from './ListingView';
import * as MenuView from '../../ui/Menu/MenuView';
import { Firebase, FirebaseRef } from '../../../constants/';


/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
    activities: state.activity.favouritesShort,
    user: state.user,
    favourites: state.activity.favourites,
    localLanguage: state.translate.localLanguage,
    listingViewText: state.translate.listingViewText

  });

// Any actions to map to the component?
const mapDispatchToProps = {
    getActivities: ActivityActions.spotGFavs,
};

/* Component ==================================================================== */
class CategoryListing extends Component {
  static componentName = 'CategoryListing';

  static propTypes = {
  //    activities: PropTypes.arrayOf(PropTypes.object),
      getActivities: PropTypes.func.isRequired,
  }

  static defaultProps = {
    activities: [],
  }

  state = {
    loading: false,
    activities: [],
  }


  componentWillMount = () => {
    console.log("FavouritesListingContainer " + JSON.stringify(this.props))
    this.fetchActivities()
  }

  componentWillReceiveProps(props) {
    console.log("FavouritesListingContainer " + JSON.stringify(this.props))

  }

  componentDidMount = () => this.getThisCategoriesActivities(this.props.activities);
  componentWillReceiveProps = props => {this.getThisCategoriesActivities(props.activities)}

  /**
    * Pick out activities that are in the current category
    * And hide loading state
    */
  getThisCategoriesActivities = (allActivities) => {
    console.log("FavouritesListingContainer " + typeof this.props.activities + " " + JSON.stringify(this.props.activities))

    let activities
    if (allActivities.length > 0) {
      activities = allActivities

      this.setState({
        activities,
        loading: false,
      });
    }
  }

  /**
    * Fetch Data from API
    */
    fetchActivities = () => this.props.getActivities()
    .then(() => this.setState({ error: null, loading: false }))
    .catch(err => this.setState({ error: err.message, loading: false }))

  render = () => {
    if (this.state.loading) return <Loading />;

    return (
      <FavouriteListing
        activities={this.state.activities}
        reFetch={this.fetchActivities}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryListing);