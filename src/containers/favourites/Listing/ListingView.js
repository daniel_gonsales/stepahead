/**
 * Activity Listing Screen
 *  - Shows a list of activities
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ListView,
  RefreshControl,
} from 'react-native';
import { Container, Header, DeckSwiper, Text, Left, Body, Icon } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';


// Consts and Libs
import { AppColors, AppStyles, AppSizes } from '@theme/';
import { ErrorMessages } from '@constants/';

// Containers
import FavouriteCard from '@containers/favourites/Card/CardContainer';

import LinearGradient from 'react-native-linear-gradient';
// Components
import Error from '@components/general/Error';

/* Component ==================================================================== */
class FavouriteListing extends Component {
  static componentName = 'FavouriteListing';

  static propTypes = {
    activities: PropTypes.arrayOf(PropTypes.object).isRequired,
    reFetch: PropTypes.func,
  }

  static defaultProps = {
    reFetch: null,
  }

  constructor() {
    super();

    this.state = {
      isRefreshing: true,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  }

  componentWillReceiveProps(props) {
    // Translate - Done - Activities Header Title
    console.log("ListingView Favs received props: " + JSON.stringify(props))
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(props.activities),
      isRefreshing: false,
      nothingMsg: 'Looks like there is nothing around',
      // title: props.listingViewText.title,
      activities: props.activities
    });
  }

  componentWillMount = () => {
    this.setState({
      activities: this.props.activities
    })
}

  // componentWillReceiveProps(props) {
  //   // Translate - Done - Favourites Header Title
  //   console.log("ListingView received props: " + JSON.stringify(props))
  //   if (props.localLanguage === 'English'){
  //     Actions.refresh({title: 'Favourites'})
  //   } else {
  //     Actions.refresh({title: 'Patik'})
  //   }
  //   this.setState({
  //     dataSource: this.state.dataSource.cloneWithRows(props.activities),
  //     isRefreshing: false,
  //     nothingMsg: 'looks like there is nothing'
  //   });
  // }

  // componentWillMount = () => {
  //   this.setState({
  //     activities: this.props.activities
  //   })
  // }

  /**
    * Refetch Data (Pull to Refresh)
    */
  reFetch = () => {
    if (this.props.reFetch) {
      this.setState({ isRefreshing: true });

      this.props.reFetch()
        .then(() => {
          this.setState({ isRefreshing: false });
        });
    }
  }


  renderFavs() {
    return(
      <FavouriteCard 
        // activity={act}
        key={Math.random()}>
        activities={this.props.activities} 
      </FavouriteCard>  
    )
  }

  render = () => {
    const { activities, user, localLanguage } = this.props;
    const { isRefreshing, dataSource, nothingMsg } = this.state;

    if (!isRefreshing && (!activities || activities.length < 1)) {
      return <Error text={ErrorMessages.activity404} />;
    }

    if(!activities.length > 0){
      return null;
    }

    return(
    <LinearGradient
      colors={['#008d9a', '#2f6ba1']}
      style={{height:AppSizes.screen.height}}
      start={{x: 0.5, y: 0.3}}
      end={{x: 0.7, y: 1.0}}
    >
     {this.renderFavs()}
      </LinearGradient>
    )
  }
}

/* Export Component ==================================================================== */
export default FavouriteListing;
