/**
 * Individual Activity Card Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

// Actions
import * as ActivityActions from '@redux/activities/actions';
import * as UserActions from '@redux/user/actions'
import * as ChatActions from '@redux/chat/actions';


// Components
import FavouriteCardRender from './CardView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  activities: state.favouritesShort,
  user: state.user,
  favourites: state.activity.favourites,
  localLanguage: state.translate.localLanguage
});

// Any actions to map to the component?
const mapDispatchToProps = {
  getChat: ChatActions.getChat
  
};

/* Component ==================================================================== */
class ActivityCard extends Component {
  static componentName = 'ActivityCard';

  static propTypes = {
    // activity: PropTypes.shape({
    //   id: PropTypes.string.isRequired,
    //   title: PropTypes.string.isRequired,
    //   body: PropTypes.string.isRequired,
    //   image: PropTypes.string.isRequired,
    //   avatar: PropTypes.string
    // }).isRequired,

    user: PropTypes.shape({
      uid: PropTypes.string,
    }),
  }

  static defaultProps = {
    favourites: null,
    user: null,
    activity:{
      id: "0",
      title: "Title",
      body: "Body",
      image: "Image",
      category: 1
    }
  }

  constructor(props) {
    console.log("CardContainer constructor props " + JSON.stringify(props))
    super(props);
    this.state = { activity: props.activity };
  }

  componentWillReceiveProps(props) {
    console.log("CardContainerFavs gets " + JSON.stringify(props))
    if (props.activity) {
      this.setState({ activity: props.activity });
    }
  }

  componentDidMount(){
    console.log("CardContainerFavs gets " + JSON.stringify(this.props))

  }

  componentWillMount(){
    console.log("CardContainerFavs gets " + JSON.stringify(this.props))
    console.log("CardContainerFavs gets " + JSON.stringify(this.props.children[1]))

    let activities = this.props.children[1]

  }

  /**
    * Check in Redux to find if this Activity ID is a Favourite
    */
  isFavourite = () => {
    const { favourites, activity } = this.props;

    if (activity && activity.id && favourites) {
      if (favourites.length > 0 && favourites.indexOf(activity.id) > -1) return true;
    }

    return false;
  }

  render = () => {
    const { activity } = this.state;
    const { user } = this.props;
    console.log('CrappyCall')
    return (
      <FavouriteCardRender
        user={user}
        activities={this.props.children[1]}
        favourites={this.props.favourites}
      />
    );
  }
}

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(ActivityCard);
