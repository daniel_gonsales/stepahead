/**
 * Activity View Screen
 *  - The individual activity screen
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  ListView,
  Animated
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Thumbnail } from 'native-base';
import FastImage from 'react-native-fast-image'
import { Actions } from 'react-native-router-flux';


// Consts and Libs
import { AppStyles } from '@theme/';

// Components
import { Card, Text } from '@ui/';

import * as UserActions from '@redux/user/actions'

/* Styles ==================================================================== */
const styles = StyleSheet.create({
  favourite: {
    position: 'absolute',
    top: -160,
    right: -10,
  },
  container: {
  //   flex: 1,
  //   marginTop: -180,
  //   // flex: 1,
    height: 640,
  //   // alignItems: 'stretch',
  //   // marginTop:-16,
  //   marginLeft: 10,
  //   marginRight: 10,
  //   // marginBottom: 5,
  //   backgroundColor: 'transparent'
  },
});

/* Component ==================================================================== */
class FavouriteCard extends Component {
  static componentName = 'FavouriteCard';

  // static propTypes = {
  //   image: PropTypes.string.isRequired,
  //   title: PropTypes.string.isRequired,
  //   body: PropTypes.string.isRequired,
  //   category: PropTypes.string.isRequired,

  //   //user: PropTypes.string.isRequired,

  //   onPress: PropTypes.func,
  //   onPressFavourite: PropTypes.func,
  //   isFavourite: PropTypes.bool,
  // }

  // static defaultProps = {
  //   onPress: null,
  //   onPressFavourite: null,
  //   isFavourite: null,
  // }

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      favouritesClone: ds.cloneWithRows(this.props.activities)
    };
    console.log('DataSource ' + JSON.stringify(this.props.activities))

  }

  getMarker(category){
    console.log('FindIcon ' + JSON.stringify(category))
    switch(category){
      case'Falling':{
        return('lemon-o')
      }
      case'Jumping':{
        return('leaf')
      }
      case'Running':{
        return('truck')
      }
      default:{
        return('random')
      }
    }
  }

  onPressCard (id) {
    let temp = this.props.favourites.filter(item => item.id === id)
    console.log('OnPressFavsCard ' + JSON.stringify(temp))
    console.log('OnPressFavsCard ' + JSON.stringify(temp[0].author))

    Actions.favouritesView({
      title: temp[0].title,
      activity: temp[0],
      user: this.props.user,
      localLanguage: this.props.localLanguage
    });
  }

  getCard(id, title, category, image, author, fullName, avatar,){
    return(
      <View style={{flex:0.52}}>
      <TouchableOpacity activeOpacity={0.8} onPress={() => this.onPressCard(id)} style={{flex:1}}>
        <Card
          containerStyle={{
            borderRadius: 10,
            overflow: 'hidden',
            borderColor:'transparent',
            padding: 0
          }}
          // onPress={}
        >
          <View style={{
            flex: 1,
            height:110,
            alignItems: 'stretch',
            backgroundColor: 'transparent'
          }}>
            <FastImage
              // resizeMode="contain"
              style={{
                flex: 1,
                margin: -5
               }}
              source={{
                uri: image,
                priority: FastImage.priority.high,
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
              <View style={{
                position: 'absolute',
                top: 8,
                left: 8,}}>
                  <Icon
                    type='font-awesome'
                    name={this.getMarker(category)}
                    color="#00e9bf"
                    size={25}
                    style={{
                      flex:1
                    }}
                  />
                </View>
          </View>

          <View style={{
            flexDirection: "row",
            alignItems: 'center',
            padding: 10,
            flex:1
          }}>

          <View>
            <Text numberOfLines={1} ellipsizeMode='tail' h5>{title}</Text>
            <Text numberOfLines={1} ellipsizeMode='tail'>{category}</Text>
          </View>

          <View style={{
            flex: 1,
            alignItems: 'flex-end'
          }}>
            <TouchableOpacity
                  activeOpacity={0.5}
                  onPress={() => {
                    console.log('ChatbuttonpressedActivity ' + author + ' ' + fullName + ' ' + avatar)
                    Actions.conversations([author, fullName, avatar])
                  }}
                  style={{
                    flexGrow: 1,
                    alignItems: 'center',
                    justifyContent:'center',
                  }}
                >
                  <Icon
                    name={'commenting-o'}
                    type='font-awesome'
                    size={25}
                    color="#333333"
                  />
            </TouchableOpacity>
          </View>
            </View>

        </Card>
        </TouchableOpacity>
      </View>
    )
  }

  getRow(rowData){
    return(
      <View style={{flex:1,flexDirection: 'row'}}>

      {/* -------------Card-1------------- */}
        {this.getCard(rowData.id1, rowData.title1, rowData.category1, rowData.image1, rowData.author1, rowData.fullName1, rowData.avatar1)}

      {/* -------------Card-2------------- */}
      {rowData.title2 ?
        this.getCard(rowData.id2, rowData.title2, rowData.category2, rowData.image2, rowData.author2, rowData.fullName2, rowData.avatar2)
      :
      <View style={{flex:0.5}}></View>
      }
      </View>
    )
  }

  render = () => {
    return(
    <ListView
    style={styles.container}
    dataSource={this.state.favouritesClone}
    renderRow={rowData =>
        this.getRow(rowData)
      }
      />
    )
  }
}

/* Export Component ==================================================================== */
export default FavouriteCard;
