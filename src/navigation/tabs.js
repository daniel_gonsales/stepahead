/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene } from 'react-native-router-flux';

// Consts and Libs
import { AppConfig } from '@constants/';
import { AppStyles, AppSizes } from '@theme/';

// Components
import { TabIcon } from '@ui/';
import { NavbarMenuButton } from '@containers/ui/NavbarMenuButton/NavbarMenuButtonContainer';

// Scenes
//import Placeholder from '@components/general/Placeholder';
import Error from '@components/general/Error';
import StyleGuide from '@containers/StyleGuideView';
import Activities from '@containers/activities/Browse/BrowseContainer';
import ActivityView from '@containers/activities/ActivityView';

import Favourites from '@containers/favourites/Browse/BrowseContainer'
import FavouriteView from '@containers/favourites/FavouritesView'

// import Chat from '@components/chat/Chat';
import Chat from '@components/chat/chatContainer';
import MapExtended from '@components/general/MapExtended';
import Settings from '@components/general/Settings';

import Conversations from '@containers/chat/Conversations'

const navbarPropsTabs = {
  ...AppConfig.navbarProps,
  renderRightButton: () => <NavbarMenuButton />,
  sceneStyle: {
    ...AppConfig.navbarProps.sceneStyle,
    paddingBottom: 45,
    // paddingTop: 0
  },
};

/* Routes ==================================================================== */
const scenes = (
  <Scene tabs key={'tabBar'}  tabBarIconContainerStyle={AppStyles.tabbar} pressOpacity={0.95} 
  titleStyle={{ color: 'white', alignSelf: 'center'}}
  showLabel={true}
  >
    <Scene
      {...navbarPropsTabs}
      key={'activities'}
      title={'Near me'}
      showLabel={true}

      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
      <Scene
        {...navbarPropsTabs}
        key={'activitiesListing'}
        component={Activities}
        title={'Near me'}
        showLabel={true}
        analyticsDesc={'Activities: Browse Activities'}
      />
      <Scene
        {...AppConfig.navbarProps}
        key={'activityView'}
        component={ActivityView}
        getTitle={props => ((props.title) ? props.title : 'Near me')}
        analyticsDesc={'Near me: View user'}
      />
    </Scene>

    <Scene
      key={'timeline'}
      {...navbarPropsTabs}
      title={'Map'}
      component={MapExtended}
      icon={props => TabIcon({ ...props, icon: 'map', title:'map' })}
//      icon={TabIcon = (props) => {return(<View>{props.title}</View>)}}
      analyticsDesc={'Map'}
      showLabel={true}
      titleStyle={{ color: 'white', alignSelf: 'center'}}
    />

<Scene
      key={'conversationsz'}
      {...navbarPropsTabs}
      title={'Messages'}
      icon={props => TabIcon({ ...props, icon: 'message' })}
    >
      <Scene
        {...navbarPropsTabs}
        key={'conversations'}
        component={Conversations}
        title={'Messages'}
        analyticsDesc={'Messages'}
      />
      <Scene
      key={'chat'}
      {...navbarPropsTabs}
      title={'Messages'}
      showLabel={true}
      component={Chat}
      analyticsDesc={'Messages'}
      />
    </Scene>    

    <Scene
      {...navbarPropsTabs}
      key={'favourites'}
      title={'Near me'}
      icon={props => TabIcon({ ...props, icon: 'star' })}
    >
      <Scene
        {...navbarPropsTabs}
        key={'favouritesListing'}
        component={Favourites}
        title={'Favourites'}
        analyticsDesc={'Favourites: Browse Favourites'}
      />
      <Scene
        {...AppConfig.navbarProps}
        key={'favouritesView'}
        component={FavouriteView}
        getTitle={props => ((props.title) ? props.title : 'Favourites')}
        analyticsDesc={'Favourites: View user'}
      />
    </Scene>

    <Scene
      {...navbarPropsTabs}    
      key={'styleGuide'}
      title={'Settings'}
      component={Settings}
      icon={props => TabIcon({ ...props, icon: 'settings' })}
      analyticsDesc={'StyleGuide: Style Guide'}
    />

  </Scene>
);

export default scenes;
