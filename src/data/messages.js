module.exports = [
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'Yes, I love it!',
    createdAt: new Date(Date.UTC(2017, 7, 30, 17, 20, 0)),
    user: {
      _id: 1,
      name: 'Developer',
    },
    sent: true,
    received: true,
    // location: {
    //   latitude: 48.864601,
    //   longitude: 2.398704
    // },
  },
  {
    _id: Math.round(Math.random() * 1000000),
    text: 'Do you like StepAhead app?',
    createdAt: new Date(Date.UTC(2017, 7, 30, 17, 20, 0)),
    user: {
      _id: 2,
      name: 'Janis B.',
    },
  },
];
