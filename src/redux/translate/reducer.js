/**
 * Translate Reducer
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
// Set initial state
export const initialState = {};

const menuView_en = {
  authMenu_0 : ' ',
  authMenu_1 : ' ',
  unauthMenu_0 : 'Login',
  unauthMenu_1 : 'Sign Up',
  catFilterTitle : 'Category',
  ageTitle : 'Age',
  minPTitle : 'Min. Price',
  maxPTitle: 'Max. Price',
  radius : 'Radius',
  loggedInAs : 'Logged in as:',
}

const menuView_lv = {
  authMenu_0 : 'Atjaunot Datus',
  authMenu_1 : 'Mainit Paroli',
  unauthMenu_0 : 'Zaloguj Się',
  unauthMenu_1 : 'ဆိုင်းအပ်',
  catFilterTitle : 'Categorija',
  ageTitle : 'Vecums',
  minPTitle : 'Min. Cena',
  maxPTitle: 'Max. Cena',
  radius : 'Strauss',
  loggedInAs : 'Ielogojies ka:',
}

const listingViewText_en = {
  nothingMsg: 'Looks like there is nothing around',
  title: 'Near Me' 
}

const listingViewText_lv = {
  nothingMsg: 'Izskatas ka šeit nekā nav',
  title: 'ප්රියතම'
}

const settings_en = {
  lang: 'Language',
  wifi: 'WiFi Only'
}

const settings_lv = {
  lang: 'Valoda',
  wifi: 'Izmantot tikai WiFi'
}

export default function activityReducer(state = initialState, action) {
  switch (action.type) {

    case 'SETLANGUAGE' : {
      console.log("setlanguage " + JSON.stringify(action.data))
        let menu
        let listingViewText
        let settings
        if (action.data === 'English'){
          menu = menuView_en
          listingViewText = listingViewText_en
          settings = settings_en
        } else {
          menu = menuView_lv
          listingViewText = listingViewText_lv
          settings = settings_lv
        }

        return {
          ...state,
          localLanguage: action.data,
          menuViewText: menu,
          listingViewText: listingViewText,
          settingsText: settings
        }
      }

      default:
      return state;

    }
}