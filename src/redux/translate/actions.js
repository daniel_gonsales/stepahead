import { Firebase, FirebaseRef } from '@constants/';

  export function changeLanguage(lang) {
    return dispatch => new Firebase.Promise((resolve) => { 
      // let uid = Firebase.auth().currentUser.uid   
  
      console.log("setLocalLanguage ttt" + lang)

      if (lang === 'English') {
        console.log("Latingo")
      lang = 'Latvian'

      if (Firebase.auth().currentUser) {
        const ref = FirebaseRef.child(`users/${Firebase.auth().currentUser.uid   }/settings/language`)
        ref.set(lang)
      }

      return dispatch({
        type: 'SETLANGUAGE',
        data: lang,
      });

    } else if (lang === 'Latvian') {
      console.log("Englezlo")
       lang = 'English'

       if (Firebase.auth().currentUser) {
        const ref = FirebaseRef.child(`users/${Firebase.auth().currentUser.uid   }/settings/language`)
        ref.set(lang)
      }

    return dispatch({
      type: 'SETLANGUAGE',
      data: lang,
    })
    } else if (lang === 1){
      console.log("setLocalLanguage ttt " + lang)
      if(Firebase.auth().currentUser){
        console.log("GetLanguage " + Firebase.auth().currentUser.uid   )
        const ref = FirebaseRef.child(`users/${Firebase.auth().currentUser.uid   }/settings/language`)
        ref.on('value', snapshot => {
          console.log("GetLanguage " + snapshot.val())

          if (JSON.stringify(snapshot.val() === 'null')){
            ref.set('English')
            return dispatch({
              type: 'SETLANGUAGE',
              data: 'English'
            })
          }
          
          return dispatch({
            type: 'SETLANGUAGE',
            data: snapshot.val(),
          })

        })
      } else {
        return dispatch({
          type: 'SETLANGUAGE',
          data: 'English',
        })
      }
    } 
    })
  }