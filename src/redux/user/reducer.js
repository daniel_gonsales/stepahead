/**
 * User Reducer
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */

// Set initial state
const initialState = {};

export default function userReducer(state = initialState, action) {
  switch (action.type) {
    case 'USER_LOGIN': {
      console.log("Userredux " + JSON.stringify(action.data))
      if (action.data) {
        const input = action.data;
        
        return {
          ...state,
          firstName: input.firstName,
          lastName: input.lastName,
          signedUp: input.signedUp,
          role: input.role,
          filter: input.filter,
          radius: input.radius,
          lastLocation: input.lastLocation,
          priceRange: input.priceRange,
          agefilter: input.agefilter,
          conversations: input.conversations,
          fullName: input.fullName,
          language: input.lang,
          avatar: input.avatar,
          phone: input.phone,
          uid: input.uid,
          email: input.email,
          emailVerified: input.emailVerified
          
        };
      }
      // return {};
    }
    case 'UPDATE_ACTIVITIES_AFTER': {
      console.log('updateActivitiesAfterReducer ' + JSON.stringify(action.data))
      return {
        ...state
      }
    }
    case 'USER_DETAILS_UPDATE': {
      let lang
      console.log("USER DETAILS " + JSON.stringify(action.data))
      if (!action.data.phone) {
        console.log('NOPHONIES')
        action.data.phone = '000'
      }
      if (!action.data.settings) {
        console.log('NOLANGUAGES')
        lang = 'English'
      } else {
        lang = action.data.settings.language
      }
      if (action.data) {
        const input = action.data;
        return {
          ...state,
          firstName: input.firstName,
          lastName: input.lastName,
          signedUp: input.signedUp,
          role: input.role,
          filter: input.filter,
          radius: input.radius,
          lastLocation: input.lastLocation,
          priceRange: input.priceRange,
          agefilter: input.agefilter,
          conversations: input.conversations,
          fullName: input.fullName,
          language: lang,
          avatar: input.avatar,
          phone: input.phone,

        };
      }
      return {};
    }
    case 'USER_LOGOUT': {
      return {};
    }

    case 'ADD_MESSAGE': {
      return{
        ...state
      }
    }

    case 'PRICE_RANGE_UPDATE' : {

      return {
        ...state
      }
    }

    case 'RADIUS_UPDATE': {
      return{
        ...state
      }
    }

    case 'UPDATE_USER_DATA': {
      return{
        ...state
      }
    }

    case 'LOCATION_UPDATE': {
      return{
        ...state
      }
    }

    case 'LANGUAGE_UPDATE': {
      return{
        ...state
      }
    }

    case 'UPDATE_USER_DATA_ACTIVITIES': {
      return{
        ...state
      }
    }

    // case 'UPDATE_INTERESTED_LIST': {
    //   console.log('UpdateInterestedListeducer ' + JSON.stringify(action.data))
    //   return{
    //     ...state
    //   }
    // }

    // case 'GET_INTERESTED_LIST': {
    //   console.log('getinterestedinlistReducer ' + JSON.stringify(action.data))
    //   let interestedList = []
    //   num = 0
    //   if (action.data && typeof action.data === 'object' && action.data != undefined) {
    //     console.log('interestingListGotthrough')
    //     // interestList = action.data.map(item => ({
    //     //   title: item.title,
    //     //   startTime: item.starTime,
    //     //   endTime: item.endTime,
    //     //   classTitle: item.classTitle,
  
    //     // }))

    //     // action.data.forEach(item => {
    //     //   interestedList[num].title = item.title
    //     //   interestedList[num].starTime = item.starTime
    //     //   interestedList[num].endTime = item.endTime
    //     //   interestedList[num].classTitle = item.classTitle
    //     //   num +=1
    //     // })
    //   }
    //   console.log('interestListEnd ' + JSON.stringify(action.data))
    //   return{
    //     ...state,
    //     interestedList: action.data
    //   }
    // }

    default:
      return state;
  }
}