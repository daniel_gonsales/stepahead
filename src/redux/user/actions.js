/**
 * User Actions
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { AsyncStorage } from 'react-native';
import { ErrorMessages, Firebase, FirebaseRef } from '@constants/';
import * as ActivityActions from '../activities/actions';
import { Actions } from 'react-native-router-flux';
let counter = 2


/**
  * Get Login Credentials from AsyncStorage
  */
async function getCredentialsFromStorage() {
  const values = await AsyncStorage.getItem('api/credentials');
  const jsonValues = JSON.parse(values);
  

  // Return credentials from storage, or an empty object
  if (jsonValues.email || jsonValues.password) return jsonValues;
  return {};
}

/**
  * Save Login Credentials to AsyncStorage
  */
async function saveCredentialsToStorage(email = '', password = '') {
  await AsyncStorage.setItem('api/credentials', JSON.stringify({ email, password }));
}

/**
  * Remove Login Credentials from AsyncStorage
  */
async function removeCredentialsFromStorage() {
  await AsyncStorage.removeItem('api/credentials');
}

let setDefData = (UID) =>{
  console.log('setDefData User gets ' + UID)
  return new Promise ((res,err) =>{
    let a = false
    if (UID != undefined){
      let ref = FirebaseRef.child(`users/${UID}`)
      ref.once('value', snap => {
        console.log('setDefData User val ' + snap.val() + JSON.stringify(snap.val()))
        console.log('setDefData User val Fname ' + JSON.stringify(snap.val().fullName))
        
        if (JSON.stringify(snap.val()) === 'null' || JSON.stringify(snap.val()) === 'undefined' || JSON.stringify(snap.val().fullName) === undefined || JSON.stringify(snap.val().fullName) === 'undefined'){
          ref.update({
            'fullName': 'Your Name',
            'filter': 'All',
            'locations': {'latitude': 56.64520000000001, 'longitude': 23.103898333333337, 'radius':200},
            'priceRange': {min: 0, max: 5000},
            'lastLocation': [23.103898333333337,56.64520000000001],
            'agefilter': 0,
            'radius': 200,
            'settings': {'language': 'English'},
            'phone': '+371',
            'avatar': 'https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/profile-02.jpg?alt=media&token=11c2efbe-e5c3-4593-815b-7adaa070ddb2'
            // 'language': 'English'
          })
          a = true
        }
      })
      console.log('setDefData User = ' + a)
      res (a)
    }
  })
}

/**
  * Get this User's Details
  */
export function getUserData(dispatch) {
  console.log('getUserDataFire ')
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;

  return setDefData(UID)
  .then(res => {
    const ref = FirebaseRef.child(`users/${UID}`);
    const refConv = FirebaseRef.child(`conversations/${UID}`)
  
  
    //--MyActivities--collect allMyActivities
    const refAct = FirebaseRef.child(`activities`)
    const refMyAct = FirebaseRef.child(`users/${UID}/myActivities`)
  console.log('poopoo')

  
    refAct.on('value', snapAct => {
      snapAct.forEach(allActivitiesActivity => {
        let exists = []
        let key = allActivitiesActivity.key
        let temp1 = allActivitiesActivity
        allActivitiesActivity = temp1.val()
        refMyAct.on('value', snapMyAct => {
          snapMyAct.forEach(myActivity => {
            let temp2 = myActivity
            myActivity = temp2.val()
            if (allActivitiesActivity.author === UID && myActivity.author === UID && key === myActivity.activityId) {
              console.log('already My activity')
              exists.push(allActivitiesActivity)
            }
          })
        })
        console.log('GetUserDataNewActivities ' + JSON.stringify(exists) + '\nAllActivities ' + JSON.stringify(allActivitiesActivity) + '\nkey ' + key + '\nUID ' + UID)
        if (JSON.stringify(exists) === '[]' && allActivitiesActivity.author === UID) {
          console.log('NewMyActivity' + key + ' author ' + allActivitiesActivity.author + ' UID ' + UID)
  
          let refmyActivities = FirebaseRef.child(`users/${UID}/myActivities`).push()
          refmyActivities.set({activityId: key, author: UID})
        }
      })
    })
  
    return ref.on('value', (snapshot) => {
      return refConv.on('value', (snap) => {
      const userData = snapshot.val() || [];
      const userConv = snap.val()
  
      return dispatch({
        type: 'USER_DETAILS_UPDATE',
        data: userData
      });
    })
    });
  })

}

// /**
//   * Login to Firebase with Email/Password
//   */
// export function login(formData = {}, verifyEmail = false) {
//   console.log('LoginActions')
//   if (Firebase === null) {
//     return () => new Promise((resolve, reject) =>
//       reject({ message: ErrorMessages.invalidFirebase }));
//   }

//   // Reassign variables for eslint ;)
//   let email = formData.Email || '';
//   let password = formData.Password || '';
//   let userdata = {}


//   return async (dispatch) => {
//     // When no credentials passed in, check AsyncStorage for existing details
//     if (!email || !password) {
//       const credsFromStorage = await getCredentialsFromStorage();
//       if (!email) email = credsFromStorage.email;
//       if (!password) password = credsFromStorage.password;
//     }

//     // Update Login Creds in AsyncStorage
//     if (email && password) saveCredentialsToStorage(email, password);

//     // We're ready - let's try logging them in
//     return Firebase.auth()
//       .signInWithEmailAndPassword(email, password)
//       .then((res) => {
//         if (res && res.uid) {
//           // Update last logged in data + default search radius
//           let UID = res.uid
//           let refUserDbase = FirebaseRef.child(`users/${UID}`)
//           let fireData = res
//           setDefData(UID)
//           .then(res =>{

//           refUserDbase.update({
//             lastLoggedIn: Firebase.database.ServerValue.TIMESTAMP,
//             })            
//             console.log('UdataUdataTa ')

//             refUserDbase.once('value', snapDb => {
//               console.log('UdataUdataTa val ' + JSON.stringify(snapDb.val()))
//               let input = snapDb.val()
//               userdata.fullName = snapDb.val().fullName
//               userdata.filter= input.filter
//               userdata.radius= input.radius
//               userdata.lastLocation= input.lastLocation
//               userdata.priceRange= input.priceRange
//               userdata.agefilter= input.agefilter
//               userdata.conversations= input.conversations
//               userdata.fullName= input.fullName
//               userdata.language= input.lang
//               userdata.avatar= input.avatar
//               userdata.phone= input.phone
//               userdata.uid= fireData.uid
//               userdata.email= fireData.email
//               userdata.emailVerified= fireData.emailVerified
              
//             })
//             console.log('UserDatata aftery ' + JSON.stringify(userdata))
//             return(userdata)
//           })
//           .then(res =>{ 
//             console.log('Userdata resres ' + JSON.stringify(res))
//             // Send verification Email - usually used on first login
//             if (verifyEmail) {
//               Firebase.auth().currentUser
//                 .sendEmailVerification()
//                 .catch(() => console.log('Verification email failed to send'));
//             }
//             // Send to Redux
//             return dispatch({
//               type: 'USER_LOGIN',
//               data: userdata,
//             });  
//           })
//         }
//       })

//       .catch((err) => {   
//         var errorCode = err.code;
//         var errorMessage = err.message;
//         console.log("Login Error " + errorMessage);
//        });
//   };
// }

/**
  * Login to Firebase with Email/Password
  */
  export function login(formData = {}, verifyEmail = false) {
    if (Firebase === null) {
      return () => new Promise((resolve, reject) =>
        reject({ message: ErrorMessages.invalidFirebase }));
    }
  
    // Reassign variables for eslint ;)
    let email = formData.Email || '';
    let password = formData.Password || '';
  
    return async (dispatch) => {
      // When no credentials passed in, check AsyncStorage for existing details
      if (!email || !password) {
        const credsFromStorage = await getCredentialsFromStorage();
        if (!email) email = credsFromStorage.email;
        if (!password) password = credsFromStorage.password;
      }
  
      // Update Login Creds in AsyncStorage
      if (email && password) saveCredentialsToStorage(email, password);
  
      // We're ready - let's try logging them in
      return Firebase.auth()
        .signInWithEmailAndPassword(email, password)
        .then((res) => {
          if (res && res.uid) {
            // Update last logged in data + default search radius
            let UID = res.uid
            setDefData(res.uid)
            .then(res =>{
            // emptyP.then(res => ActivityActions.spotGFavs(dispatch))
            FirebaseRef.child(`users/${UID}`).update({
              lastLoggedIn: Firebase.database.ServerValue.TIMESTAMP,
            //  radius: 60,
  
            })            
            })
  
  
  
            // Send verification Email - usually used on first login
            if (verifyEmail) {
              Firebase.auth().currentUser
                .sendEmailVerification()
                .catch(() => console.log('Verification email failed to send'));
            }
            console.log('getUserDataSpotGs')
            // ActivityActions.spotGFavs(dispatch)
  
            getUserData(dispatch)
            // ActivityActions.spotGFavs()
          }
  
          // Send to Redux
          return dispatch({
            type: 'USER_LOGIN',
            data: res,
          });
        }).catch((err) => {   
          var errorCode = err.code;
          var errorMessage = err.message;
          console.log("Login Error " + errorMessage);
         });
    };
  }

let emptyP = new Promise(res => res(console.log('emptyPP')))

export function updateRadius(rad){
  console.log('updateraduser ' + JSON.stringify(Firebase.auth().currentUser))
  if (JSON.stringify(Firebase.auth().currentUser) != 'null'){
    const ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/radius");
    return dispatch => new Firebase.Promise((resolve) => {
      ref.set(rad).then(res => console.log("radiusChanged"))
  
      ref.on('value', snapshot => {
        console.log("changeRadius changed radius from " + rad + " to " + JSON.stringify(snapshot.val()))
      })
  
    return resolve(dispatch({
      type: 'RADIUS_UPDATE',
      data: "",
    }));
    })
  } else {return}

}

export function upDatePriceRange(val, path){
  if (val.text) {
    val = parseInt(val.text)
  } else {
    val = 0
  }

  const ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/priceRange/" + path);
  return dispatch => new Firebase.Promise((resolve) => {
  ref.on('value', snapshot => {
    console.log("updatePriceRange gets " + JSON.stringify(snapshot.val()))
    return (snapshot.val())
  })
  ref.set(val)

  return resolve(dispatch({
    type: 'PRICE_RANGE_UPDATE',
    data: "",
  }));
  })
}

export function updateLocation (longitude, latitude) {
  console.log("UpdateLocation Gets " + longitude + " " + latitude)
  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  const ref = FirebaseRef.child("users/" + UID + "/locations");
  return dispatch => new Firebase.Promise((resolve) => {
    ref.child('longitude').on('value', snapshot => {
      console.log("UpdateLocation Long " + snapshot.val())
    })

    ref.child('longitude').set(longitude)
    ref.child('latitude').set(latitude)

    //ref.set(rad).then(res => console.log("radiusChanged"))

    //ref.on('value', snapshot => {
    //  console.log("changeRadius changed radius from " + rad + " to " + JSON.stringify(snapshot.val()))
    //})

  return resolve(dispatch({
    type: 'LOCATION_UPDATE',
    data: "o",
  }));
  })  
}

export function updateUserData(data, path) {
  console.log("updateUserData Gets " + JSON.stringify(data) + JSON.stringify(path))
  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  const ref = FirebaseRef.child("users/" + UID + `/${path}`);
  console.log('updateUserData ref is ' + ref)

  return dispatch => new Firebase.Promise((resolve) => {  
    ref.set(data)
    return resolve(dispatch({
      type: 'UPDATE_USER_DATA',
      data: "o",
    }));   
    
  })
}

export function updateUserDataActivities (fullName, avatar) {
  console.log('updateUserDataActivities ' + fullName + '\n' + avatar)
  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  //--MyActivities-- myActivitiesData
  const refMyAct = FirebaseRef.child(`users/${UID}/myActivities`)
  const refMyConv = FirebaseRef.child(`users/${UID}/conversations`)
  refMyAct.on('value', snapMyAct => {
    // if (snapMyAct)
    snapMyAct.forEach(myActivity => {
      let temp = myActivity.val()
      myActivity = temp

      const refActivity = FirebaseRef.child(`activities/${myActivity.activityId}`)
      console.log('updateUserDataActivities ' + refActivity + '\n' + fullName + ' ' + avatar)
      refActivity.update({avatar: avatar, fullName: fullName})
    })

  })  
  //-- --
  refMyConv.on('value', snapMyConv => {
    snapMyConv.forEach(myConversation => {
      let temp = myConversation.val()
      myConversation = temp
      let refOtherConv = FirebaseRef.child(`conversations/${myConversation.receiverId}/${myConversation.conversation}`)
      refOtherConv.update({receiverFullName: fullName, avatar: avatar})
    })
  })

  return dispatch => new Firebase.Promise((resolve) => {  
    return resolve(dispatch({
      type: 'UPDATE_USER_DATA_ACTIVITIES',
      data: "o",
    }));   
    
  })
}


/**
  * Sign Up to Firebase
  */
export function signUp(formData = {}) {
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  const email = formData.Email || '';
  const password = formData.Password || '';
  const firstName = formData.FirstName || '';
  const lastName = formData.LastName || '';

  return () => Firebase.auth()
    .createUserWithEmailAndPassword(email, password)
    .then((res) => {
      // Setup/Send Details to Firebase database
      if (res && res.uid) {
        FirebaseRef.child(`users/${res.uid}`).set({
          firstName,
          lastName,
          signedUp: Firebase.database.ServerValue.TIMESTAMP,
          lastLoggedIn: Firebase.database.ServerValue.TIMESTAMP,
        });
      }
    });
}

/**
  * Reset Password
  */
export function resetPassword(formData = {}) {
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  const email = formData.Email || '';
  return () => Firebase.auth().sendPasswordResetEmail(email);
}

/**
  * Update Profile
  */
export function updateProfile(formData = {}) {
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;

  const email = formData.Email || '';
  const firstName = formData.FirstName || '';
  const lastName = formData.LastName || '';

  // Set the email against user account
  return () => Firebase.auth().currentUser
    .updateEmail(email)
      .then(() => {
        // Then update user in DB
        FirebaseRef.child(`users/${UID}`).update({
          firstName, lastName,
        });
      });
}

/**
  * Logout
  */
export function logout() {
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  return dispatch => Firebase.auth()
    .signOut()
    .then(() => {
      removeCredentialsFromStorage();
      ActivityActions.resetFavourites(dispatch);
      dispatch({ type: 'USER_LOGOUT' });
    });
}

/*
  * Filter
  */
export function filter(){
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));

      const UID = Firebase.auth().currentUser.uid;
      if (!UID) return false;

  }  
}

export function updateUserLanguage(language){
  console.log("updateUserLanguages " + language)
  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  console.log(UID)
  console.log("users/" + UID + "/settings/language")
  const ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/settings/language");
  console.log("ref is " + ref)
  
  return dispatch => new Firebase.Promise((resolve) => {
    console.log("hey there")
    ref.set(language).then(res => console.log("languageChanged"))

    ref.on('value', snapshot => {
      console.log("changeLanguage changed language from " + language + " to " + JSON.stringify(snapshot.val()))
    })

  return resolve(dispatch({
    type: 'LANGUAGE_UPDATE',
    data: "",
  }));
  })
}

export function getInterestedList(){
  console.log('getInterestedList')
  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  console.log(UID)
  const ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/interestedList");
  console.log("ref is " + ref)
  
  return dispatch => new Firebase.Promise((resolve) => {
    console.log("hey there")
    // ref.set(language).then(res => console.log("languageChanged"))

    ref.on('value', snapshot => {
      console.log("getInterestedList val " + JSON.stringify(snapshot.val()))
      let interestedList = []
      let num = 0
      snapshot.forEach(item => {
        // interestedList[num].title = item.title
        // interestedList[num].starTime = item.starTime
        // interestedList[num].endTime = item.endTime
        // interestedList[num].classTitle = item.classTitle
        // num +=1
      //   let listy = item.map(itm => ({
      //     title: itm.title,
      //     classTitle: itm.classTitle,
      //     startTime: itm.starTime,
      //     endTime: itm.endTime
      //   })
      // )

        interestedList.push(item.val())
      })

    //   interestedList = snapshot.map(item => ({
    //     title: item.title,
    //     classTitle: item.classTitle,
    //     startTime: item.starTime,
    //     endTime: item.endTime
    //   })
    // )


      console.log('getInterestedListForEachFormat ' + JSON.stringify(interestedList))

      return resolve(dispatch({
        type: 'GET_INTERESTED_LIST',
        data: interestedList,
      }))
    })
  })
}

export function updateActivitiesAfter(activities){
  console.log('AGoGoTo AGoGoTo ' + JSON.stringify(activities))
  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  console.log(UID)
  return dispatch => new Firebase.Promise((resolve) => {
    console.log('AGoGoTo')
    let refUserAct = FirebaseRef.child(`users/${UID}/interestedList`)
    console.log('AGoGoTo ' + refUserAct)
    refUserAct.on('value', snapUserAct => {
      snapUserAct.forEach(userAct => {
        let temp = userAct.val()
        userAct = temp
        console.log('AGoGoTo ' + JSON.stringify(userAct))
  
        activities.forEach(activity => {
          console.log('AGoGoTo activity ' + JSON.stringify(activity))
          if(activity.title === userAct.classTitle) {
            console.log('thisUserWantsToGoTo ' + activity.title + ' really? ' + userAct.classTitle + '\n' + userAct.title)
            activity.timetable.forEach(time => {
              // let temp = time.val()
              // time = temp
              console.log('AGoGoTo tt ' + JSON.stringify(time))
              console.log('AGoGoTo cc ' + userAct.classTitle + '\n' + time.title + ' really? ' + userAct.title)
  
              if(time.title === userAct.title){
                console.log('AGoGoTo iffy ' + time.key + ' ' + userAct.classTitle + '\n' + time.title + ' really? ' + userAct.title)
                time.going = 'true'
                // console.log('AGoGoTo CHa ' + JSON.stringify(filterData.activities[1]))
              }
            })
          }
        })
        return resolve(dispatch({
          type: 'UPDATE_ACTIVITIES_AFTER',
          data: activities,
        }))
      })
    })
  })


}

// export function updateInterestedList(activity, classTitle, activities){
//   console.log("updateInterestedList " + JSON.stringify(activity))
//   console.log("updateInterestedList " + JSON.stringify(activities))

//   const UID = Firebase.auth().currentUser.uid;
//   if (!UID) return false;
//   console.log(UID)
//   console.log("users/" + UID + "/interestedList")
//   const refp = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/interestedList").push();
//   const ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/interestedList");
//   console.log("ref is " + ref)
  
//   return dispatch => new Firebase.Promise((resolve) => {
//     console.log("hey there updateInterestedList")

//     ref.on('value', snapshot => {
//       console.log("updateInterestedList has " + JSON.stringify(snapshot.val()))

//       if(snapshot.val() === null){
//         console.log('snapshot.val().length = 0')
        
//           refp.set({startTime: activity.starTime, endTime: activity.endTime, title: activity.title, classTitle: classTitle})
//         return resolve(dispatch({
//           type: 'UPDATE_INTERESTED_LIST',
//           data: '',
//         }))
        
//       } else {
//         console.log('snapshot.val().length > 0')
//         console.log('snapshot.val() ' + JSON.stringify(snapshot.val()))
//         console.log('snapshot.val() ' + snapshot.numChildren() )

//         let count = 0
//         // let 
//         snapshot.forEach( act => {
//           console.log(activity.title + ' updateInterestedList ' + act.val().title + ' || ' + classTitle + ' ' + act.val().classTitle)
//           if(act.val().title === activity.title && act.val().classTitle === classTitle){
//             count = act.key
//             console.log('county ' + count + ' keyy ' + act.key)
//           }
//         })
//         console.log('updateInterestedList double ' + count)
//         if(count === 0){
//           console.log('updateInterestedList double No ')
//           refp.set({startTime: activity.starTime, endTime: activity.endTime, title: activity.title, classTitle: classTitle})
//           return resolve(dispatch({
//             type: 'UPDATE_INTERESTED_LIST',
//             data: '',
//           }))
//         } else {
//           console.log('updateInterestedList doubleB is ')

//           let tempy = snapshot
//           console.log('temp before A ' + JSON.stringify(tempy))
//           tempy.child(count).remove
//           console.log('FAKTA ' + activities.length)
//           for(let a = 0; a < activities.length; a++){
//               for(let b = 0; b < 3; b++){
//                 console.log('totostuff ' + activities[a].title  + ' ?===? ' + classTitle + '\n' + activities[a].timetable[b].title  + ' ?===? ' +  activity.title)
//                 if(activities[a].title === classTitle && activities[a].timetable[b].title === activity.title){
//                   console.log('found duplicate ' + activities[a].timetable[b].title + ' ' + activity.title)
//                   activities[a].timetable[b].going = 'false'
//                 }
//             }
//         }
//           // activities.forEach(activ => {
//           //   console.log('A before B ' + JSON.stringify(activ))
//           //   if (activ.title === activity.classTitle) {
//           //     console.log('A before BB ')// + JSON.stringify())
//           //     activ.timetable.forEach(tim => {
//           //       console.log('A before BB ' + JSON.stringify(tim))

//           //       if (tim.title === activity.title) {
//           //         console.log('updateInterestedList before going ' + JSON.stringify(activ))
//           //         console.log('updateInterestedList before going ' + activ.going)
//           //         activ.going === 'false'
//           //         console.log('updateInterestedList after going ' + activ.going)
//           //       }
//           //     })
//           //   }
//           // })
//           let temp = activities.filter(act => act.title === activity.classTitle)
//           console.log('EPTA ' + JSON.stringify(temp))

//           console.log('temp after ' + JSON.stringify(tempy))
//           // ref.child(count).remove()
//           return resolve(dispatch({
//             type: 'UPDATE_INTERESTED_LIST',
//             data: activities,
//           }))
//         }

//       }
//     })
// })
// }
