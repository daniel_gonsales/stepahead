/**
 * User Actions
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { AsyncStorage } from 'react-native';
import { ErrorMessages, Firebase, FirebaseRef } from '@constants/';
import * as ActivityActions from '../activities/actions';

/*
  * getChat
  */

  export function getChat(receiver, fullName, avatar, conversation, userFullName, userAvatar, allMsg){
    console.log('getChat gets \nUID ' + receiver + '\nreceiverfullName ' + fullName  + '\navatar ' + JSON.stringify(avatar) + '\nConversation ' + conversation + '\nUserFullName ' + userFullName + '\nuserAvatar ' + userAvatar)
    const UID = Firebase.auth().currentUser.uid;
    if (!UID) return false;
    let messages
    let currConversation

    if (receiver === Firebase.auth().currentUser.uid){
      console.log('GetChat trying to write to self ' + receiver + ' ' + Firebase.auth().currentUser.uid)
      return dispatch => new Firebase.Promise((resolve) => {

      return dispatch({
        type: 'GET_CHAT',
        data: ''
      });
    })
    }

    if (Firebase === null) {
      console.log('GetChat Firebase === null')
      return () => new Promise((resolve, reject) =>
        reject({ message: ErrorMessages.invalidFirebase }));
    }

    //getConversation/MessagesPath
    //ifThereAreMessages get them           tempy.push(msg)
    //check if users/UID/conversations/convid exists, update all fields 
    //check if conversations/receiver/convid exists, update all fields
    //check if conversations/UID/convid exists, update all fields
    //return messages and currentconv

    return dispatch => new Firebase.Promise((resolve) => {

    let refMsg = FirebaseRef.child(`messages/${conversation}`)
    let oldmsg

    refMsg.on('value', snapMsg => {

      checkConversations(UID,receiver)
      .then (res => {
        console.log('CheckConversationsPromise resy ' + JSON.stringify(res))
        currConversation = res
        if (JSON.stringify(res) === undefined) {
          currConversation = `${UID}-${receiver}`
          console.log('currConversation was null ' + currConversation)
        }
        // --
         updateConvData(currConversation,UID,userFullName,userAvatar,receiver,fullName,avatar)
        console.log('worksesy ' + JSON.stringify(getConversationData(currConversation)._55))

        Promise.all([getConversationData(currConversation),getConversationData(currConversation,true)])
        .then(res => {
          console.log('GetConversationData PromiseS \n1. ' + JSON.stringify(res[0]) + '\n2. ' + JSON.stringify(res[1]))
          if(JSON.stringify(res[0])){
            messages = res[0].reverse()
            console.log('GetConversationData res true ' + JSON.stringify(messages))
          }

          let tempy = []
          if (JSON.stringify(res[1])){
            console.log('GetChatConversation length is ' + res[1].length + ' ' + JSON.stringify(res[1]))
            tempy = res[1].reverse()
            console.log('GetChatConversations Length ' + tempy.length)
            tempy.splice(0,2)
            console.log('GetChatConversations Length ' + tempy.length)
            oldmsg = tempy
            }

            return dispatch({
              type: 'GET_CHAT',
              data: [messages, currConversation, oldmsg]          
            })

          })
        })
      })
    })
  }

  let getUserData = (uid, data) => {
    return new Promise ((res, err) => {
  //*    console.log(data.length + " Redux/Activities/Actions.getUserData data: " + data)
      if(uid){
        const ref = FirebaseRef.child(`users/${uid}/${data}`)
        //console.log("GetUserData " + data + " " + ref)
        ref.once('value', snapshot => {
  //*        console.log("GetUserData receives " + snapshot.val())
          res(snapshot.val())
        })
      }
    })
  }
  
  let getConversationData = (conversation, allMsg) => {
    return new Promise ((res, err) => {
    ref = FirebaseRef.child(`messages/${conversation}`)
    console.log("GetChatConversation " + ref)
    if(allMsg){

      ref.orderByKey().once('value', snapshot => {
        let tempy = []
        if(snapshot.val() != null) {
          Object.values(snapshot.val()).forEach(msg => {
            tempy.push(msg)
            res(tempy)
          })
        } else {
          res(undefined)
        }
      })
    } else {
      ref.once('value', snapval => console.log(JSON.stringify(snapval.val())))
      ref.orderByKey().limitToLast(2).once('value', (snapshot) => {
        console.log("what's heree " + JSON.stringify(snapshot.val()))
        let tempy =[]
        if(snapshot.val() != null) {
          Object.values(snapshot.val()).forEach(msg => {
            tempy.push(msg)
            res(tempy)
          })
        } else {
          res(undefined)
        }
      })
    }
  })
}

  let checkConversations = ( UID, receiver) => {
    return new Promise ((res, err) => {
      let path
      let conv1 = `${UID}-${receiver}`
      let conv2 = `${receiver}-${UID}`
      let refConvU = FirebaseRef.child(`conversations/${UID}`)
      let refConvR = FirebaseRef.child(`conversations/${receiver}`)
      console.log('CheckConversationsPromise refU ' + refConvU)
      console.log('CheckConversationsPromise refR ' + refConvR)
      
      refConvU.on('value', snapConvU => {
        snapConvU.forEach(conversUs => {
          if (conversUs.val().conversation === conv1){
            console.log('CheckConversationsPromise path1 ' + conv1)
            path = conv1
            res(path)
          }
          if (conversUs.val().conversation === conv2){
            console.log('CheckConversationsPromise path1 ' + conv2)            
            path = conv2
            res(path)
            
          }
        })

        if (!path){
          refConvR.on('value', snapConvR => {
            snapConvR.forEach(conversUs => {
              if (conversUs.val().conversation === conv1){
                console.log('CheckConversationsPromise path1 Reciver ' + conv1)
                path = conv1
                res(path)
                
              }
              if (conversUs.val().conversation === conv2){
                console.log('CheckConversationsPromise path2 Reciver ' + conv2)            
                path = conv2
                res(path)
                
              }
            })
          })
        }

        res(path)
        console.log('CheckConversationsPromise results in ' + path)
        console.log('CheckConversationsPromise results in no path? ')
      })

    })
  }

  let updateConvData = (conversation, UID, userFullName, userAvatar, receiverId, receiverFullName, receiverAvatar, mode) => {
    return new Promise ((res, err) => {
      console.log('updateConvDataPromise gets \nconversation ' + conversation + ' \nUID ' + UID + ' \nuserFullName ' + userFullName + ' \nuserAvatar ' + userAvatar + ' \nreceiverId ' + receiverId + ' \nreceiverFullName ' + receiverFullName + '\nreceiverAvatar ' + receiverAvatar)      
      let refConvU = FirebaseRef.child(`conversations/${UID}/${conversation}`)
      let refConvR = FirebaseRef.child(`conversations/${receiverId}/${conversation}`)
      let refUserConv = FirebaseRef.child(`users/${UID}/conversations/${conversation}`)
      console.log('updateConvDataPromise R ' + refConvR)
      console.log('updateConvDataPromise U ' + refConvU)
      console.log('pipip ' + mode)

      if (mode === 1) {
        refConvU.once('value', userNewSnap => {
          let refLastMessage = FirebaseRef.child(`messages/${userNewSnap.val().conversation}/${userNewSnap.val().lastmessage}`)

          refConvR.update({conversation: conversation, receiverId: UID, receiverFullName: userFullName ,avatar: userAvatar})
          refConvU.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar})
          if(userNewSnap.val().messageCounter > 0) {
            refLastMessage.once('value', snapLastMessage => {
              console.log('lastmsgTimeis ' + snapLastMessage.val().createdAt)
              let tempTime = new Date(snapLastMessage.val().createdAt)
              console.log('lastmsgTimeis Day/Month/Year Hours:Minutes ' + tempTime.getDate() + '/' + tempTime.getMonth() + '/' + tempTime.getFullYear() + ' ' + tempTime.getUTCFullYear() + ' ' + tempTime.getHours() + ':' + tempTime.getMinutes())
              if (snapLastMessage.location){
                refUserConv.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar, messageCounter: userNewSnap.val().messageCounter, lastmessage: 'location', lastmessageTime: snapLastMessage.val().createdAt, location: snapLastMessage.val().location})          
              } else {
              refUserConv.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar, messageCounter: userNewSnap.val().messageCounter, lastmessage: snapLastMessage.val().text, lastmessageTime: snapLastMessage.val().createdAt})          
              }
            })
          } else {
            refLastMessage.once('value', snapLastMessage => {
              console.log('lastmsgActions ' + JSON.stringify(snapLastMessage.val()))
              if (JSON.stringify(snapLastMessage.val()) != 'null') {
                if(snapLastMessage.val().location){
                  refUserConv.update({lastmessage:'location', lastmessageTime: snapLastMessage.val().createdAt, location: snapLastMessage.val().location})
                } else {
                  refUserConv.update({lastmessage:snapLastMessage.val().text, lastmessageTime: snapLastMessage.val().createdAt})

                }
              }
            })
            refUserConv.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar})    
                  
          }
        })
      } else {
        refConvR.once('value', receiverSnap => {
          if(receiverSnap.val() != null && receiverSnap.val().messageCounter) {
            refConvR.update({conversation: conversation, receiverId: UID, receiverFullName: userFullName ,avatar: userAvatar})
            refConvU.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar, messageCounter: 0})
            refUserConv.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar, messageCounter: 0})
  
          } else {
            refConvR.update({conversation: conversation, receiverId: UID, receiverFullName: userFullName ,avatar: userAvatar, messageCounter: 0})
            refConvU.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar, messageCounter: 0})
            refUserConv.update({conversation: conversation, receiverId: receiverId, receiverFullName: receiverFullName, avatar: receiverAvatar, messageCounter: 0})
  
          }
        })
      }
      
    })
  }

export function addMessage(dispatch, receiverid, receiverFullName, senderFullName, avatar, conversation){
  console.log("AddMessage gets " + JSON.stringify(dispatch))
  console.log("AddMessage gets local " + JSON.stringify(dispatch[0].location))
  
  console.log('AddMessage gets conv ' + conversation)
  // console.log("AddMessage gets " + JSON.stringify(avatar))
  // console.log("AddMessage gets " + conversation + " " + receiverid, + " " + receiverFullName + " " + senderFullName)
  // console.log('AddMessage gets ' + dispatch + ' ' + receiverid + ' ' + receiverFullName + ' ' + receiverFullName + ' ' +senderFullName + ' ' + conversation + ' ' + avatar)

let message = {}

  if (dispatch[0].location){
    message = {
      _id: Math.round(Math.random() * 1000000),
      location: dispatch[0].location,
      time: Date.now(),
      createdAt: Date.now(),
      to: {
          _id: receiverid, 
          name: receiverFullName,
          //avatar: dispatch[0].user.avatar
      },
      user: {
        _id: dispatch[0].user._id,
        name: senderFullName,
        avatar: avatar
  
      }
    }
  } else {
    message = {
      _id: Math.round(Math.random() * 1000000),
      text: dispatch[0].text,
      time: Date.now(),
      createdAt: Date.now(),
      to: {
          _id: receiverid, 
          name: receiverFullName,
          //avatar: dispatch[0].user.avatar
      },
      user: {
        _id: dispatch[0].user._id,
        name: senderFullName,
        avatar: avatar
  
      }
    }
  }

  console.log('addMessageseses ' + JSON.stringify(message))

  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  const UID = Firebase.auth().currentUser.uid;

  if (JSON.stringify(message.user) === '{}' && Firebase.auth().currentUser === null) {  
    return dispatch => new Firebase.Promise((resolve) => {})
    }

  let ref = FirebaseRef.child(`messages/${conversation}`).push()
  let refConvers = FirebaseRef.child(`conversations/${receiverid}/${conversation}`)
  let refConversSender = FirebaseRef.child(`conversations/${UID}/${conversation}`)

  refConversSender.update({lastmessage: ref.key})

  refConvers.once('value', snapshot => {
    console.log('addMessage Conversation ' + snapshot.val())
      console.log('Conversations/messages ' + JSON.stringify(snapshot.val()))

      if (snapshot.val() === null){
        console.log('conversation chat stuff No conversations' + conversation + ' ' + senderFullName + ' ' + UID + ' ' + ref.key + ' ' + avatar )
        refConvers.set({conversation: `${conversation}`, receiverFullName: senderFullName, receiverId: UID, avatar: avatar, messageCounter: 1, lastmessage: ref.key})      
      } else {
        console.log('Conversation Messages are ' + JSON.stringify(snapshot.val().messageCounter))
        console.log('addmessage old data ' + JSON.stringify(snapshot.val()))
        newmsgCount = snapshot.val().messageCounter + 1
        refConvers.update({messageCounter: newmsgCount, lastmessage: ref.key})

      }

  })

return dispatch => new Firebase.Promise((resolve) => {
   ref.set(message)

   return dispatch({
    type: 'ADD_MESSAGE',
    // data: [messages.reverse(), currConversation]
    data: message
  })
})

}

let getNewConversations = (UID) => {
  return new Promise ((res, err) => {
  refNewConv = FirebaseRef.child(`conversations/${UID}`)
  refUserConv = FirebaseRef.child(`users/${UID}/conversations`)
  let newConverses = []
  let userConverses = []

  console.log("GetNewConversations \n" + refNewConv + '\n' + refUserConv)
  refUserConv.once('value', userConv => {
    refNewConv.once('value', newConv => {
      console.log('NewConvsz ' + JSON.stringify(userConv.val()))
      console.log('UserConvsz ' + JSON.stringify(newConv.val()))
      
      userConv.forEach(uConv => {
        userConverses.push(uConv)
      })
      newConv.forEach(fConv => {
        newConverses.push(fConv)
      })
      res({newConv: newConverses, userConv: userConverses})
    })
  })

})
}


export function getConversations(userFullName, userAvatar) {
  if (Firebase === null) {
    return () => new Promise((resolve, reject) =>
      reject({ message: ErrorMessages.invalidFirebase }));
  }

  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;

  const refOldConv = FirebaseRef.child(`users/${UID}/conversations`);
  const refNewConv = FirebaseRef.child(`conversations/${UID}`)
  let exists = []

  return dispatch => new Firebase.Promise((resolve) => {
  //get NewConversations and UserConversations
  getNewConversations(UID)
    .then(res => {
      console.log('getNewConvs res is \n' +JSON.stringify(res) + '\nnew ' + JSON.stringify(res.newConv) + '\nusr ' + JSON.stringify(res.userConv))
  //forEachNewConversation compare with eachUserConversation if newConv is not in UserConv create UserConv if newConvAlready exists updateData in UserConv
      res.newConv.forEach(nconv => {
        console.log('foreachies N ' + nconv.val().conversation)
        let exists2 = []
        let temp1 = nconv
        nconv = temp1.val()

        res.userConv.forEach(uConv =>{
          console.log('foreachies U ' + uConv.val().conversation)
          let temp2 = uConv
          uConv =temp2.val()

          console.log('foreachies ' + JSON.stringify(nconv) + '\n' + JSON.stringify(uConv))
          console.log('foreachies ' + nconv.conversation + ' ?===? ' + uConv.conversation)
          if(nconv.conversation === uConv.conversation) {
            console.log('UpdateConversationdataGetConversations')
            exists2.push(nconv)
          //   console.log(
          //     'UpdateConversationdataGetConversations\n' +
          //     'newConv ' + newConv.val().conversation + '\n' +
          //     'UID ' + UID + '\n' +
          //     'userFullName ' + newConv.val().userFullName + '\n' +
          //     'userAvatar' + 'USERAVATARNEEDED' + '\n' +
          //     'receiverId' + newConv.val().receiverId + '\n' +
          //     'receiverFullName ' + newConv.val().receiverFullName + '\n' +
          //     'receiverAvatar' + newConv.val().receiverAvatar
          // )
            // -- 
             updateConvData(nconv.conversation, UID, userFullName, userAvatar, nconv.receiverId, nconv.receiverFullName, nconv.avatar, 1)
            // updateConvData(newConv.conversation, UID, newConv.fullName, newConv.)
          }

        })
        console.log('foreachies exists2 ' + JSON.stringify(exists2))

        if(JSON.stringify(exists2) === '[]') {
          console.log('newConversation ' + nconv.conversation)
          // --
           updateConvData(nconv.conversation, UID, userFullName, userAvatar, nconv.receiverId, nconv.receiverFullName, nconv.avatar, 1)
        }
      })

      return dispatch({
        type: 'GET_CONVERSATIONS',
        data:  ''
      });
    })
  })
}
