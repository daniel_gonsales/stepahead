/**
 * Chat Reducer
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */

// Set initial state
const initialState = {};

export default function chatReducer(state = initialState, action) {
  switch (action.type) {

    case 'ADD_MESSAGE': {
      console.log('addmessagereducer ' + JSON.stringify(action.data))
      return{
        ...state,
        oldMessages: action.data
      }
    }
    
    case 'GET_CHAT': {
      console.log('getChat reducer msg ' + JSON.stringify(action.data[0]))
      console.log('getChat reducer conv ' + JSON.stringify(action.data[1]))
      console.log('getChat reducer oldmsg ' + JSON.stringify(action.data[2]))
      
      
      return {
        ...state,
        messages: action.data[0],
        currentChat: action.data[1],
        oldMessages: action.data[2]

      }
    }

    case 'GET_OLD_CHAT': {
      console.log('getOldChat Reducer ' + JSON.stringify(action.data))
      console.log('getOldChat Reducer ' + JSON.stringify(action.data[0]))

      return {
        ...state,
        oldMessages: action.data[0]
      }
    }

    case 'GET_CONVERSATIONS' : {
      console.log('conversationz reducer ' + JSON.stringify(action.data[0],action.data[1]))
      console.log('conversationz reducer ' + JSON.stringify(action.data[0]))

      return {
        ...state,
        conversationz: action.data
      }
    }    

    default:
      return state;
  }
}