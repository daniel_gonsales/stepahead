/**
 * Activity Actions
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { Firebase, FirebaseRef } from '@constants/';
import { login, filter } from '../user/actions';
import { tempoFil } from '../../containers/ui/Menu/MenuView';
import geoFire from 'geofire';
import SearchInput, { createFilter } from 'react-native-search-filter';

/**
  * Get this User's Favourite Activities
  */
export function spotGFavs(newFavourites, isFavourite) {
  console.log("SpotGFavs isFavs " + isFavourite)
  console.log('SpotGFavs UID ' + getUID()._55)
  return dispatch => new Firebase.Promise((resolve) => {    
    const ref = FirebaseRef.child(`favourites/${getUID()._55}/`)
    let favourites = []
    let UID = getUID()._55

    if (newFavourites && isFavourite === true){
      console.log('Removing Fav from Base ' + isFavourite + ' ' + JSON.stringify(newFavourites))
      let refFavs = FirebaseRef.child(`favourites/${UID}`)
      let id = newFavourites.id
      refFavs.once('value', snapFavs => {

        snapFavs.forEach((fav,num) => {
          if (fav.val().id === id) {
            console.log('UpdateFavsDuplicate Promise id ' + id + ' key ' + fav.key + ' ' + refFavs.child(fav.key))
            refFavs.child(fav.key).remove()
            
          }
        })
      })
    }

    if (newFavourites && isFavourite === false){
      console.log('Pushing new Fav to Base ' + isFavourite + ' ' + JSON.stringify(newFavourites))
      let resFavsPush = FirebaseRef.child(`favourites/${UID}`).push()
      resFavsPush.set(newFavourites)
    }

    ref.on('value', (snapshot) => { 
      console.log("spotGFavs receives snap " + JSON.stringify(snapshot.val()))

      if(snapshot.val()) {
        snapshot.forEach((item, num) => {
          let temp = item.val()
          item = temp
          console.log('SpotGFavsRed num ' + num + ' ' + JSON.stringify(item))
          favourites.push(
            {
              id: item.id,
              title: item.title,
              body: item.body,
              category: item.category,
              image: item.image,
              author: item.author,
              location: item.location,
              fullName: item.fullName,
              timetable: item.timetable,
              avatar: item.avatar,       
            }
          ) 
        })
      }

      let favouritesShort = []
      

      if( JSON.stringify(snapshot.val()) != 'null'){
        for (let i = 0; i < favourites.length; i ++){
          let tt = i + 1
          if (i % 2 === 0 ) {
            console.log('SpotGFavsZZ counters '+ i + ' ' + tt + ' length ' + favourites.length + ' ' + i % 2)
  
            if(JSON.stringify(favourites[tt]) === null || JSON.stringify(favourites[tt]) === undefined) {
              console.log('SpotGFavs data/ no second column '+ favourites[i].title)
              favouritesShort.push({
                title1: favourites[i].title,
                image1: favourites[i].image, 
                category1: favourites[i].category,
                author1: favourites[i].author,
                fullName1: favourites[i].fullName,
                id1: favourites[i].id,
                avatar1: favourites[i].avatar 
             })
            }else{
              console.log('SpotGFavs data/ has second column '+ favourites[i].title + ' ' + favourites[tt].title)
              favouritesShort.push({
                title1: favourites[i].title,
                image1: favourites[i].image, 
                category1: favourites[i].category,
                author1: favourites[i].author,
                fullName1: favourites[i].fullName,
                avatar1: favourites[i].avatar,
                id1: favourites[i].id, 
                title2: favourites[tt].title, 
                image2: favourites[tt].image, 
                category2: favourites[tt].category,
                author2: favourites[tt].author,
                fullName2: favourites[tt].fullName,
                avatar2: favourites[tt].avatar,
                id2: favourites[tt].id, 
  
              })
              console.log('SpotGFavsZZ tt ' + JSON.stringify(favouritesShort))
            }
            }
                
        }
      }

      console.log('SpotGFavsZZ resShort ' + JSON.stringify(favouritesShort)) 
      console.log('SpotGFavsZZ resLong ' + JSON.stringify(favourites)) 

      return dispatch({
        type: 'SPOTGFAVS',
        data: [favouritesShort, favourites]
      });
    })
  })
}

/**
  * Reset a User's Favourite Activities in Redux (eg for logou)
  */
export function resetFavourites(dispatch) {
  console.log('FavsReplaceCalled')
  return dispatch({
    type: 'FAVOURITES_REPLACE',
    data: {},
  });
}

/**
  * Get Category
  */
export function getCategory(dispatch) {
  if (Firebase === null) return () => new Promise(resolve => resolve()); 

  return dispatch => new Firebase.Promise((resolve) => {
    let UID = null
    let categoryNum
    let category
    let ref = FirebaseRef.child('categories')

    return ref.once('value').then((snapshot) => {

      if(Firebase.auth().currentUser != null){
        UID = Firebase.auth().currentUser.uid

        let ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid);
        ref.on("value", function(snapshot) {
          console.log('pofjsaiohfsa ' + snapshot.val())
          if(snapshot.val() === null) {
            categoryNum = null
            // ref.set({
            //   'locations':{
            //     'longitude':0,
            //     'latitude':0,
            //     'radius': 200
            //   }
            // })
          } else {
            categoryNum = snapshot.val().filter  
          }
        }, function (errorObject) {
          console.log("The read failed: " + errorObject.code);
        })

        //categoryNum = getUserData(UID, "filter")

        let temp = snapshot.val() || []
        
        if(categoryNum != null) {
          let tempF = temp.filter(fil => 
            fil.id === categoryNum)

          let tempV = temp[0] 
          category = tempF.concat(temp[0])
          console.log("UserID true, Redux/Activities/Actions.getUserData getCategory category: " + JSON.stringify(category))
        } else {
          category = 0
        }

          
//*      console.log("Categories are 1 " + JSON.stringify(category));
//----//If dispatch = null or user wasn't authorized
      } else {
        category = snapshot.val() || [];
//*        console.log("UserID false, Redux/Activities/Actions.getUserData getCategory category: " + JSON.stringify(category));
        
      }
    
      return resolve(dispatch({
        type: 'CATEGORIES_REPLACE',
        data: category,
      }));
    });
    
  });
}

let getUserData = (uid, data) => {
  return new Promise ((res, err) => {
//*    console.log(data.length + " Redux/Activities/Actions.getUserData data: " + data)
    if(uid && data != 'all'){
      const ref = FirebaseRef.child(`users/${uid}/${data}`)
      //console.log("GetUserData " + data + " " + ref)
      ref.on('value', snapshot => {
//*        console.log("GetUserData receives " + snapshot.val())
        res(snapshot.val())
      })
    } 

    if(data === 'all') {
      const ref = FirebaseRef.child(`users/${uid}`)
      //console.log("GetUserData " + data + " " + ref)
      ref.on('value', snapshot => {
//*        console.log("GetUserData receives " + snapshot.val())
        res(snapshot.val())
      })    
    }
  })
}

getAllActivities = new Promise ((res, err) => {
  const ref = FirebaseRef.child('activities')
  console.log("AllActivitiesRef " + JSON.stringify(ref))
  ref.on('value', snapshot => {
    console.log("AllActivitiesRef " + JSON.stringify(snapshot.val()))
    res(snapshot.val())
  })
})

let getUID = () =>{
  return new Promise((res, err) => {
    if(Firebase.auth().currentUser != null){
//*      console.log("Redux/Activities/Actions.getUID User is: " + JSON.stringify(Firebase.auth().currentUser.uid))
      res(Firebase.auth().currentUser.uid)
  
    } else {
//*      console.log("Redux/Activities/Actions.getUID User is: undefined ")
      res(undefined)
    }
  })
}

let setDefData = (UID) =>{
  console.log('setDefData gets ' + UID)
  return new Promise ((res,err) =>{
    let a = false
    if (UID != undefined){
      let ref = FirebaseRef.child(`users/${UID}`)
      ref.once('value', snap => {
        if (JSON.stringify(snap.val()) === 'null' || JSON.stringify(snap.val()) === 'undefined'){
          ref.update({
            'fullName': 'Your Name',
            'filter': 'All',
            'locations': {'latitude': 56.64520000000001, 'longitude': 23.103898333333337, 'radius':200},
            'priceRange': {min: 0, max: 5000},
            'lastLocation': [23.103898333333337,56.64520000000001],
            'agefilter': 0,
            'radius': 200,
            'settings': {'language': 'English'},
            'phone': '+371',
            'avatar': 'https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/profile-02.jpg?alt=media&token=11c2efbe-e5c3-4593-815b-7adaa070ddb2'
            // 'language': 'English'
          })
          a = true
        }
      })
      res (a)
    }
  })
}

/**
  * Get Activities
  */
  export function getActivities(dispatch) {
    console.log("Redux/Activities/Actions.getActivities Was called " + dispatch)
    const filterData = {
      UID: null,
      radius: null,
      filter: null,
      locations: [],
      nearMeIDs: null,
      activities: [],
      ageFilter: null,
      priceRange: {},
      search: dispatch,
      searchres: [],
      userData: []
    }

    const ref = FirebaseRef.child('activities')
    return dispatch => new Firebase.Promise((resolve) => {
       ref.on('value', (snapshot) => {
        getUID()
        .then((res) => {
          console.log('GetActivitiesReduxer resy1 ' + JSON.stringify(res))
//*          console.log("User ID is  " + res)
          if (res === undefined || res === null) {
            filterData.filter = 'All'
            filterData.radius = 600
            filterData.location = {latitude: 56.64520000000001, longitude: 23.103898333333337}
            filterData.priceRange = {min: 0, max: 5000}
            filterData.ageFilter = 0
            return filterData.radius
          } else {
            filterData.UID = res

            return setDefData(res)
            .then( res => {
              console.log("Astha "+ JSON.stringify(res))
              return (
                Promise.all([
                  getUserData(filterData.UID, "filter").then(res => filterData.filter = res),
                  getUserData(filterData.UID, "radius").then(res => filterData.radius = res),
                  getUserData(filterData.UID, "locations").then(res => filterData.location = res),
                  getUserData(filterData.UID, "agefilter").then(res => filterData.ageFilter = res),
                  getUserData(filterData.UID, "priceRange").then(res => filterData.priceRange = res)
  
                ])
                .then(res => {
                  console.log("ActionsMiddata\nFilter: " + filterData.filter + "\nRadius: " + filterData.radius + "\nLocation: " + JSON.stringify(filterData.location)+ '\nAgeFilter: ' + filterData.ageFilter + '\nPriceRange ' + JSON.stringify(filterData.priceRange))
                  return filterData.radius
                })
              )   
            }
           
            )


          }
        })
        .then( res => {
          console.log("Astha "+ JSON.stringify(filterData))
          console.log("Data Sent To NearMe " + filterData.radius + " " + JSON.stringify(filterData.location))
          filterData.nearMeIDs = nearMeFunction(filterData.radius, filterData.location)
         console.log("ActionsNearMeIDs " + filterData.nearMeIDs)

          return filterData.nearMeIDs
        })
        .then( res => {
          let a = 0
          console.log("Astha Response |||| "+JSON.stringify(res))
          filterData.activities = getAllActivities
//           res.forEach(geoFireRefID => {     
//             //console.log("Activity Georef " + getAllActivities._45[a].geoFire)  
//             console.log("Activity Georef " + getAllActivities._55[a].geoFire + " georefID " + JSON.stringify(geoFireRefID))
//             //let tampe = getAllActivities._55.filter(fil => fil.geoFire === geoFireRefID)
//             //filterData.activities[a] = getAllActivities._55.filter(fil => fil.geoFire === geoFireRefID)[a]
// //            filterData.activities[a] = tampe
//             console.log("Astha Log  Activity 1: "+JSON.stringify(filterData.activities))
//             a +=1
            
//             //console.log("Astha Log  Activity: "+JSON.stringify(tampe))
//           })
          console.log("Activities filtered by NearMe: " + JSON.stringify(filterData.activities))
          console.log("")
          return filterData.activities
        })
        .then( res => {
          if(filterData.filter === 'All' ) {
            filterData.activities = res
          console.log("Filter = 0 => AllActivities: " + JSON.stringify(filterData.activities))

            return filterData.activities
          } else {
            filterData.activities = res.filter(activityCategory => activityCategory.category === filterData.filter)
           console.log("Activities filtered by Filter: " + JSON.stringify(filterData.activities))

            return filterData.activities
          }
        })
        .then(res => {
          if (filterData.ageFilter && filterData.ageFilter != "All") {
            filterData.activities = res.filter(activityAge => activityAge.age[2] === filterData.ageFilter)
           console.log("AgeActivitates " + JSON.stringify(filterData.activities))
          } else if (filterData.ageFilter === "All" || !filterData.ageFilter) {
            console.log("All Ages or No Ages Defined" +res)
            filterData.activities = res
          }
          return filterData.activities
          })
        .then(res => {
          if (filterData.UID != null){
            console.log("thereissomePriceRange" + JSON.stringify(filterData.priceRange))
            console.log("thereissomePriceRange" + JSON.stringify(filterData.activities))

            filterData.activities.forEach(act => console.log('SHRISE ' + act.price))
            
            filterData.activities = res.filter(priceFilter => priceFilter.price > filterData.priceRange.min && priceFilter.price < filterData.priceRange.max)

          } else {
                        console.log("thereisnoPriceRange")
          }
        })
        .then(res => {
          console.log("ActivityActions1 " + JSON.stringify(filterData.activities))
          console.log("filterData.search " + JSON.stringify(filterData.search))
          if (filterData.search != undefined) {
            filterData.activities.forEach(ser => {
              if (ser.body.toLowerCase().includes(filterData.search.toLowerCase()) || ser.title.toLowerCase().includes(filterData.search.toLowerCase())){
                console.log("ActivityActions Search " + ser.id + " has something ")
                filterData.searchres.push(ser.id)
              }

            })

            if (filterData.searchres.length > 0) {
              let temp = filterData.activities
              let a = 0

              filterData.activities = []
  
              filterData.searchres.forEach(fory => {
                console.log("filterData.Search " + JSON.stringify(fory))
                filterData.activities[a] = temp.filter(fil => fil.id === fory)[0]
                a +=1
              })
            } else {
              console.log("ActivityActions Search Empty")
              filterData.activities = []
            }


          } else { filterData.search = undefined}
          console.log("ActivityActions2 " + JSON.stringify(filterData.activities))
          // res(filterData)
        })
        .then(res => {
          console.log('AGoGoToActivities')
          //let refUserAct = FirebaseRef.child(`users/${filterData.UID}/interestedList`)
          let refUserAct = FirebaseRef.child(`users/${filterData.UID}/myActivities`)
          console.log('AGoGoTo ' + refUserAct)
          refUserAct.on('value', snapUserAct => {
            snapUserAct.forEach(userAct => {
              let temp = userAct.val()
              userAct = temp
              console.log('AGoGoTo ' + JSON.stringify(userAct))
              console.log("ActivityActions2 " + JSON.stringify(filterData.activities))
// 
              filterData.activities.forEach(activity => {
                // console.log('AGoGoTo activity ' + JSON.stringify(activity))
                console.log('AGoGoTo activity ' + activity.title + ' ' + userAct.classTitle)
                // console.log('AGoGoTo activity ' + JSON.stringify(activity))

                if(activity.title === userAct.classTitle) {
                  console.log('thisUserWantsToGoTo ' + activity.title + ' really? ' + userAct.classTitle + '\n' + userAct.title)
                  activity.timetable.forEach(time => {
                    // let temp = time.val()
                    // time = temp
                    console.log('AGoGoTo tt ' + JSON.stringify(time))
                    console.log('AGoGoTo cc ' + userAct.classTitle + '\n' + time.title + ' really? ' + userAct.title)
// 
                    if(time.title === userAct.title){
                      console.log('AGoGoTo iffy ' + time.key + ' ' + userAct.classTitle + '\n' + time.title + ' really? ' + userAct.title)
                      time.going = 'true'
                      // console.log('AGoGoTo CHa ' + JSON.stringify(filterData.activities[1]))
                    }
                  })
                }
              })
            })
          })
        })
        .then(res => {
          console.log(`FilterData in the end: \n
          UID: ${filterData.UID}\n
          Radius: ${filterData.radius}\n
          NearMeIDs: ${filterData.nearMeIDs}\n
          Filter: ${filterData.filter}\n
          AgeFilter: ${filterData.ageFilter}\n
          PriceRange: ${JSON.stringify(filterData.priceRange)}\n
          Activities: ${JSON.stringify(filterData.activities)}\n
          Search: ${filterData.search}
          }`)

          return resolve(dispatch({
            type: 'ACTIVITIES_REPLACE',
            data: filterData.activities
          }));
        })
      })

  })
}

  export function nearMeFunction(radius, location) {
    console.log("nearMeFunction gets " + JSON.stringify(location))
    checkGeoFireRef()
    if (radius === undefined || radius === 0 || radius === null) {
      radius = 600
      console.log("radius was null or undefined")
    }
    
 
    if (location === null || JSON.stringify(location) === 'null') {
            console.log("Location was null or undefined")

      let temp = []
      temp.latitude = 0
      temp,longitude = 0
      location = temp

    } else {

    }
    console.log("NearMe receives radius " + radius)
      const ref = FirebaseRef.child('activityLocations')
      let geoFireRef = new geoFire(ref)
  
      // let lat = 37.421998333333335
      // let lon = -122.08400000000002

      // let lat = location.latitude
      // let lon = location.longitude

      let lat = location.latitude  //  56.64520000000001
      let  lon = location.longitude //  24.103898333333337


      let num = 0
  
      let tempy = []
  
      let geoQuery = geoFireRef.query({
        center: [lat, lon],
        radius: radius
      })

      ref.on('value', snapshot => {
        console.log("NearMe activityLocations " + snapshot.val().length + JSON.stringify(snapshot.val()))
      })

      // geoQuery.updateCriteria({
      //   center: [lat, lon],
      //   radius: radius
      // });
      if (typeof geoQuery !== "undefined") {
        operation = "Updating";

        geoQuery.on("key_entered", (key, location, distance) => {
          console.log(key + " isOSOS located at [" + location + "] which is within the query (" + distance.toFixed(2) + " km from center)");
          tempy.push(key)
        });

        geoQuery.on("key_exited", (key,location,distance) => {
          console.log(key + " isOSOS located at [" + location + "] which has exited the query (" + distance.toFixed(2) + " km from center)");
        })   
      } else {
        operation = "Creating";
  
        geoQuery = geoFireRef.query({
          center: [lat, lon],
          radius: radius
        });
  
        geoQuery.on("key_entered", function(key, location, distance) {
          console.log(key + " is CREATINGOLI located at [" + location + "] which is within the query (" + distance.toFixed(2) + " km from center)");
        });

        geoQuery.on("key_exited", (key,location,distance) => {
          console.log(key + " isOSOS located at [" + location + "] which has exited the query (" + distance.toFixed(2) + " km from center)");
        })   
      }
      console.log(operation + " the query: centered at [Latitude: " + lat + ", Longitude: " + lon + "] with radius of " + radius + "km")
      //console.log(typeof tempy + " tempity " + tempy)
      tempy.forEach(t => console.log("contentsisis " + t))

      // setTimeout(() => {
      //   console.log('I do not leak!');
      // }, 5000);
  
      return (tempy)
  }

  export function checkGeoFireRef() {
    const ref = FirebaseRef.child('activities')
    const refG = FirebaseRef.child('activityLocations')
    const geoFireRef = new geoFire(refG)
    
    console.log("CheckGeoref " + JSON.stringify(Firebase.auth()))
  
    return dispatch => new Firebase.Promise((resolve) => {
      return ref.once('value', (snapshot) => {

        snapshot.val().forEach( act => {
          console.log("geofire check " + JSON.stringify(act.geoFire))
//          if (act.geoFire === "fff") {
            // if (act.geoFire != "z") {
            console.log("Activity geofire " + act.title + " has no georef " + act.geoFire)
            let lat = parseFloat(act.location.latitude);
            let lon = parseFloat(act.location.longitude);
            let myID = act.title //+ "@" + refG.push().key;
            console.log(myID + ": setting position to [" + lat + "," + lon + "]");

            geoFireRef.set(myID, [lat, lon]).then(function() {
              console.log(myID + ": setting position to to [" + lat + "," + lon + "]");
              let poo = FirebaseRef.child(`activities/${act.id}/geoFire`)
              poo.on('value', (snapshot) => console.log("poopty " + snapshot.val()))
              FirebaseRef.child(`activities/${act.id}/geoFire`).set(myID)
            });
          // }
      })

        return resolve(dispatch({
          type: 'GEOFIRE_CHECK',
          data: "O",
        }));
  })
})
}

export function updateInterestedList(activity, classTitle, activities){
  console.log("updateInterestedList " + JSON.stringify(activity))
  console.log("updateInterestedList " + JSON.stringify(activities))

  const UID = Firebase.auth().currentUser.uid;
  if (!UID) return false;
  console.log(UID)
  console.log("users/" + UID + "/interestedList")
  const refp = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/interestedList").push();
  const ref = FirebaseRef.child("users/" + Firebase.auth().currentUser.uid + "/interestedList");
  console.log("ref is " + ref)
  
    console.log("hey there updateInterestedList")

    return ref.on('value', snapshot => {
      console.log("updateInterestedList has " + JSON.stringify(snapshot.val()))

      if(snapshot.val() === null){
        console.log('snapshot.val().length = 0')
        
          refp.set({startTime: activity.starTime, endTime: activity.endTime, title: activity.title, classTitle: classTitle})
          return dispatch => new Firebase.Promise((resolve) => {

          return resolve(dispatch({
            type: 'UPDATE_INTERESTED_LIST',
            data: '',
          }))
        })        
      } else {
        console.log('snapshot.val().length > 0')
        console.log('snapshot.val() ' + JSON.stringify(snapshot.val()))
        console.log('snapshot.val() ' + snapshot.numChildren() )

        let count = 0
        // let 
        return snapshot.forEach( act => {
          console.log(activity.title + ' updateInterestedList ' + act.val().title + ' || ' + classTitle + ' ' + act.val().classTitle)
          if(act.val().title === activity.title && act.val().classTitle === classTitle){
            count = act.key
            console.log('county ' + count + ' keyy ' + act.key)
          }
        })
        console.log('updateInterestedList double ' + count)
        if(count === 0){
          console.log('updateInterestedList double No ')
          refp.set({startTime: activity.starTime, endTime: activity.endTime, title: activity.title, classTitle: classTitle})
          return dispatch => new Firebase.Promise((resolve) => {

          return resolve(dispatch({
            type: 'UPDATE_INTERESTED_LIST',
            data: '',
          }))
        })
        } else {
          console.log('updateInterestedList doubleB is ')

          let tempy = snapshot
          console.log('temp before A ' + JSON.stringify(tempy))
          tempy.child(count).remove
          console.log('FAKTA ' + activities.length)
          return dispatch => new Firebase.Promise((resolve) => {
            for(let a = 0; a < activities.length; a++){
              for(let b = 0; b < 3; b++){


                console.log('totostuff ' + activities[a].title  + ' ?===? ' + classTitle + '\n' + activities[a].timetable[b].title  + ' ?===? ' +  activity.title)
                if(activities[a].title === classTitle && activities[a].timetable[b].title === activity.title){
                  console.log('found duplicate ' + activities[a].timetable[b].title + ' ' + activity.title)
                  ref.child(count).remove()
                  activities[a].timetable[b].going = 'false'
                  return resolve(dispatch({
                    type: 'UPDATE_INTERESTED_LIST',
                    data: activities,
                  }))
                }
            }
        }                    
          })

          // activities.forEach(activ => {
          //   console.log('A before B ' + JSON.stringify(activ))
          //   if (activ.title === activity.classTitle) {
          //     console.log('A before BB ')// + JSON.stringify())
          //     activ.timetable.forEach(tim => {
          //       console.log('A before BB ' + JSON.stringify(tim))

          //       if (tim.title === activity.title) {
          //         console.log('updateInterestedList before going ' + JSON.stringify(activ))
          //         console.log('updateInterestedList before going ' + activ.going)
          //         activ.going === 'false'
          //         console.log('updateInterestedList after going ' + activ.going)
          //       }
          //     })
          //   }
          // })
          let temp = activities.filter(act => act.title === activity.classTitle)
          console.log('EPTA ' + JSON.stringify(temp))

          console.log('temp after ' + JSON.stringify(tempy))
          // ref.child(count).remove()
          return resolve(dispatch({
            type: 'UPDATE_INTERESTED_LIST',
            data: activities,
          }))
        }

      }
    })

}