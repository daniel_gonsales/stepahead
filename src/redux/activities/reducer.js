/**
 * Activity Reducer
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import Store from './store';

import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Platform,
  // Image,
  ImageBackground,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import { Icon, SearchBar } from 'react-native-elements';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

// Set initial state
export const initialState = Store;

export default function activityReducer(state = initialState, action) {
  const styles = StyleSheet.create({
    map: {
      ...StyleSheet.absoluteFillObject,
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      //paddingHorizontal: 10
    },
    button: {
      alignItems: 'center',
      backgroundColor: '#DDDDDD',
      padding: 10
    }
  });

  
  switch (action.type) {
    case 'FAVOURITES_REPLACE': {
      console.log(typeof action.data + " Favourites Reducer say " + JSON.stringify(action.data))
      return {
        ...state,
        favourites: action.data || [],
      };
    }
    case 'CATEGORIES_REPLACE': {
      return {
        ...state,
        categories: action.data || [],
      };
    }

    case 'FAVOURITES_UPDATE': {
      return {
        ...state,
        
      }
    }

    case 'FILTERS_REPLACE' : {            
      return {
        ...state,
        filter: action.data,
      };
    }

      case 'SPOTGFAVS': {
        let favourites = [];
        console.log('SpotGFavsReducer 00 ' + JSON.stringify(action.data[0]))
        console.log('SpotGFavsReducer 11 ' + JSON.stringify(action.data[1]))
        return {
          ...state,
          favourites: action.data[1],
          favouritesShort: action.data[0]
        };
      }

    case 'ACTIVITIES_REPLACE': {
      let activities = [];
      let markers = []
      console.log("Activities Reducer Gets " + JSON.stringify(action.data))
      console.log("Activities Reducer Gets0 " + JSON.stringify(action.data[0]))
      console.log("Activities Reducer Gets1 " + JSON.stringify(action.data[1]))


      // Pick out the props I need
      if (action.data && typeof action.data === 'object' && action.data != undefined) {
        activities = action.data.map(item => ({
          id: item.id,
          title: item.title,
          body: item.body,
          category: item.category,
          image: item.image,
          author: item.author,
          location: item.location,
          fullName: item.fullName,
          timetable: item.timetable,
          avatar: item.avatar,
          moreButt: false

        }))
      }
      // console.log('updatebutts ' + activities[0].moreButt)

      return {
        ...state,
        activities,
        // search: action.data[1]
      };
    }

    case 'GEOFIRE_CHECK': {
      return {
        ...state
      }
    }

    case 'UPDATE_INTERESTED_LIST': {
      console.log('UpdateInterestedListeducer ' + JSON.stringify(action.data))

      if (action.data && typeof action.data === 'object' && action.data != undefined) {
        activities = action.data.map(item => ({
          id: item.id,
          title: item.title,
          body: item.body,
          category: item.category,
          image: item.image,
          author: item.author,
          location: item.location,
          fullName: item.fullName,
          timetable: item.timetable,
          avatar: item.avatar,
          moreButt: false

        }));
      }
      // console.log('updatebutts ')

      // console.log('updatebutts ' + activities[0].moreButt)

      return{
        ...state,
        activities
      }
    }

    default:
      return state;
  }
}