/**
 * Recipe Default Store
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
export default {
  "activities" : [ {
    "age" : [ "13", "" ],
    "author" : "fqUTuliykAcoBXmbHnuhiN7TiDN2",
    "body" : "Do you have too many weak eyes and a couple of strong legs, join us, lets run with scissors together",
    "category" : 3,
    "geoFire" : "Running with scissors@-L6Lyqy9M6Gqk-KeOvWA",
    "id" : "0",
    "image" : "https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/josh-9b56c7710613268d40cd83495b8f60cb8952a582-s900-c85.jpg?alt=media&token=1f7fbcdb-a4e3-419c-af14-e2b3a9c7c714",
    "location" : {
      "latitude" : 37.421998333333335,
      "longitude" : -78.08400000000002
    },
    "price" : [ "20", "USD" ],
    "title" : "Running with scissors"
  }, {
    "age" : [ "20", "" ],
    "author" : "fqUTuliykAcoBXmbHnuhiN7TiDN2",
    "body" : "Are you tired of base jumps finishing way too soon, come and experience endless falling, we'll tie a rock to your neck and set you free on your first endless jump",
    "category" : 1,
    "geoFire" : "Endless Fall@-L6Lyu5Ge-Bca7cYer-f",
    "id" : "1",
    "image" : "https://firebasestorage.googleapis.com/v0/b/react-native-starter-app.appspot.com/o/image-2.jpg?alt=media&token=6ed1740b-529b-4772-9a92-615e92b544b2",
    "location" : {
      "latitude" : 37,
      "longitude" : -122
    },
    "price" : "",
    "title" : "Endless Fall"
  }, {
    "age" : [ "6", "" ],
    "author" : "vGFl52Ba70fFXkNBoy03tchMvS33",
    "body" : "Fall down mutch? Come to us and we'll teach you how to fall without breaking anything! Fisrst push down the stairs is free",
    "category" : 1,
    "geoFire" : "Professional falling down the stairs@-L6Lyw2bcIxNdoXkQBVM",
    "id" : "2",
    "image" : "https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/josh-9b56c7710613268d40cd83495b8f60cb8952a582-s900-c85.jpg?alt=media&token=1f7fbcdb-a4e3-419c-af14-e2b3a9c7c714",
    "location" : {
      "latitude" : 33.421998333333335,
      "longitude" : -129.08400000000003
    },
    "price" : [ "15", "EUR" ],
    "title" : "Professional falling down the stairs"
  }, {
    "age" : [ "18", "" ],
    "author" : "fqUTuliykAcoBXmbHnuhiN7TiDN2",
    "body" : "Have your morning jogs became more of a chore than fun exciting way to start your day? Come visit us we'll show you fun and excitement, but remember it's all about jogging, we're not responsible for any sideffects of standing or walking during ou course",
    "category" : 3,
    "geoFire" : "Jogging naked on hot coals@-L6Lyy23yxfE109iO72m",
    "id" : "3",
    "image" : "https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/170918161647-11-what-a-shot-0919-overlay-tease.jpg?alt=media&token=e07c18cc-c06d-4c36-9947-1cef81a73a9a",
    "location" : {
      "latitude" : 42.421998333333335,
      "longitude" : -102.08400000000002
    },
    "price" : [ "0", "" ],
    "title" : "Jogging naked on hot coals"
  }, {
    "age" : [ "", "" ],
    "author" : "vGFl52Ba70fFXkNBoy03tchMvS33",
    "body" : "Love running with sacks full of potatoes? Love throwing stuff? Come excercise with us! We won't quit till we mash!",
    "category" : 3,
    "geoFire" : "Potatoes tag@-L6Lyzyb3x7GqDwgKb-B",
    "id" : "4",
    "image" : "https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/images.jpeg?alt=media&token=6a186f61-2fe4-46c4-879c-9a89d8672786",
    "location" : {
      "latitude" : -34.421998333333335,
      "longitude" : -72.08400000000002
    },
    "price" : [ "", "" ],
    "title" : "Potatoes tag"
  }, {
    "age" : [ "", "" ],
    "author" : "vGFl52Ba70fFXkNBoy03tchMvS33",
    "body" : "Are you all out of ideas? Have tried a lot of standart sports and nothing seemed interesting or challenging enough? Come to us, Ministry Of Silly Jumps! We'll get you started with all kinds of silly jumps and tricks you can do to attract attention or to spicen up your love life",
    "category" : 2,
    "geoFire" : "Ministry Of Silly Jumps@-L6Lz16foKTQT1HXgaVP",
    "id" : "5",
    "image" : "https://firebasestorage.googleapis.com/v0/b/stepahead-b61d0.appspot.com/o/images.jpeg?alt=media&token=6a186f61-2fe4-46c4-879c-9a89d8672786",
    "location" : {
      "latitude" : -15.421998333333335,
      "longitude" : -122.08400000000002
    },
    "price" : [ "", "" ],
    "title" : "Ministry Of Silly Jumps"
  } ],
  "activityLocations" : {
    "Endless Fall@-L6Lyu5Ge-Bca7cYer-f" : {
      ".priority" : "9q94rzdk9g",
      "g" : "9q94rzdk9g",
      "l" : [ 37, -122 ]
    },
    "Jogging naked on hot coals@-L6Lyy23yxfE109iO72m" : {
      ".priority" : "9xx3m48wn3",
      "g" : "9xx3m48wn3",
      "l" : [ 42.421998333333335, -102.08400000000002 ]
    },
    "Ministry Of Silly Jumps@-L6Lz16foKTQT1HXgaVP" : {
      ".priority" : "3m30mbt0ke",
      "g" : "3m30mbt0ke",
      "l" : [ -15.421998333333335, -122.08400000000002 ]
    },
    "Potatoes tag@-L6Lyzyb3x7GqDwgKb-B" : {
      ".priority" : "63usptwj34",
      "g" : "63usptwj34",
      "l" : [ -34.421998333333335, -72.08400000000002 ]
    },
    "Professional falling down the stairs@-L6Lyw2bcIxNdoXkQBVM" : {
      ".priority" : "9junns7ckb",
      "g" : "9junns7ckb",
      "l" : [ 33.421998333333335, -129.08400000000003 ]
    },
    "Running with scissors@-L6Lyqy9M6Gqk-KeOvWA" : {
      ".priority" : "dq8kzhkyyy",
      "g" : "dq8kzhkyyy",
      "l" : [ 37.421998333333335, -78.08400000000002 ]
    }
  },
  "categories" : [ {
    "id" : 0,
    "title" : "All"
  }, {
    "id" : 1,
    "title" : "Falling"
  }, {
    "id" : 2,
    "title" : "Jumping"
  }, {
    "id" : 3,
    "title" : "Running"
  } ],
  "favourites" : {

  },
  "messages" : {
    "vGFl52Ba70fFXkNBoy03tchMvS33-fqUTuliykAcoBXmbHnuhiN7TiDN2" : {
      "-L5oBb2rResUP3wDLv7H" : {
        "_id" : 786761,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "createdAt" : 1519146660330,
        "text" : "llls",
        "time" : 1519146660330,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      },
      "-L5oBdu8JEEQojt9GXue" : {
        "_id" : 832926,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "createdAt" : 1519146671997,
        "text" : "aaa",
        "time" : 1519146671997,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      },
      "-L5oC5D77k2-Ub0FBbNZ" : {
        "_id" : 324970,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "createdAt" : 1519146787963,
        "text" : "sss",
        "time" : 1519146787963,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      },
      "-L5oD8YVcgMCzG3f-68u" : {
        "_id" : 55893,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "createdAt" : 1519147063763,
        "text" : "another try",
        "time" : 1519147063763,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      }
    },
    "vGFl52Ba70fFXkNBoy03tchMvS33-undefined" : {
      "-L5i85k4Rp0fSadXTcdF" : {
        "_id" : 998245,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "id" : 1,
        "text" : "asdas",
        "time" : 1519045077420,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      }
    },
    "vGFl52Ba70fFXkNBoy03tchMvS33-vGFl52Ba70fFXkNBoy03tchMvS33" : {
      "0" : {
        "_id" : 0,
        "received" : true,
        "sent" : true,
        "text" : "Hi",
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "Developer"
        }
      },
      "-L5tjim0LXPit7ni8uYx" : {
        "_id" : 344124,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "createdAt" : 1519239751337,
        "text" : "ggg",
        "time" : 1519239751337,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      },
      "-L5tjjjC1H6k6HNA5OK6" : {
        "_id" : 254967,
        "author" : {
          "id" : "vGFl52Ba70fFXkNBoy03tchMvS33",
          "name" : "dispatch[0].user.name"
        },
        "createdAt" : 1519239755252,
        "text" : "bbh",
        "time" : 1519239755252,
        "user" : {
          "_id" : "vGFl52Ba70fFXkNBoy03tchMvS33"
        }
      }
    }
  },
  "users" : {
    "I6xsWSTKMtPaIahXkLaXRZHonx93" : {
      "filter" : 0,
      "lastLoggedIn" : 1516100277744,
      "settings" : {
        "language" : "EN"
      }
    },
    "Xg7GwibfLbNIfLs8B4LfFwluNiB3" : {
      "filter" : 3,
      "lastLoggedIn" : 1518544842295,
      "settings" : {
        "language" : "EN"
      }
    },
    "vGFl52Ba70fFXkNBoy03tchMvS33" : {
      "filter" : 0,
      "firstName" : "poo",
      "lastLocation" : [ 0.01, -0.01 ],
      "lastLoggedIn" : 1519817313079,
      "radius" : 600
    }
  }
}