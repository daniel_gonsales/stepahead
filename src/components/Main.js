import React from 'react';
import { StyleSheet, View } from 'react-native';
import { DefaultRenderer } from 'react-native-router-flux';

export default function Main ({ navigationState, onNavigate }) {
  console.log('navigationStateMain ' + JSON.stringify(navigationState))
  return (
    <View > 
      <DefaultRenderer navigationState={navigationState.children[0]} onNavigate={onNavigate} />
    </View> 
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingTop: 55
  },
  box: {
    flex: 1,
    marginBottom: 150,
    marginTop: 50,
    marginHorizontal: 25
  }
});
