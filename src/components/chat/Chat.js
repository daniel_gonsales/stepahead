import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {GiftedChat, Bubble, Actions as GiftedActions, Day, LoadEarlier} from 'react-native-gifted-chat';
import CustomActions from '@components/chat/CustomActions';
import ChatView from '@containers/chat/ChatView';
// import * as UserActions from '@redux/user/actions';
import * as ChatActions from '@redux/chat/actions';


// import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import { FirebaseRef } from '../../constants/index';

import { AppSizes } from '@theme'
import LinearGradient from 'react-native-linear-gradient';

import { Actions } from 'react-native-router-flux';


class Chat extends React.Component {
  componentWillReceiveProps(props) {

    if (props.oldMessages === undefined){
      this.setState({
        loadEarlier: false,
      })
    }

    if (!this.state.loadLast){
      console.log('LoadyLasty ' + this.state.loadLast)
      this.onSend(props.messages, true)
      this.setState({
        oldMessages: props.oldMessages,
        loadLast: true
      })
    }


    // console.log('Chat props state ' + JSON.stringify())

  }

  componentDidMount(props) {
    if (JSON.stringify(this.props.user) === '{}') {
      Actions.login()
    }

  }

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      loadEarlier: true,
      typingText: null,
      isLoadingEarlier: false,
      loadLast: false
    };

    this._isMounted = false;
    this.onSend = this.onSend.bind(this);
    // this.onReceive = this.onReceive.bind(this);
    this.renderCustomActions = this.renderCustomActions.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onLoadEarlier = this.onLoadEarlier.bind(this);

    this._isAlright = null;
  }

  componentWillMount(props) {

    console.log('getchatyy ')
    let receiverUID = this.props.data[0]
    let receiverFullName = this.props.data[1]
    let avatar = this.props.data[2]
    let conversation = this.props.currentChat
    if (this.props.data[0]) {
      // console.log(`receiverUID ${receiverUID}\nreceiverFullName ${receiverFullName}\navatar ${avatar}\nconversation ${conversation}`)
      this.setState({
        messages: [],
        writeToUid: receiverUID,
        writeToFullName: receiverFullName,
        currentChat: conversation,
        receiverUID: this.props.data[0],
        receiverFullName: this.props.data[1],
        receiverAvatar: this.props.data[2],
        conversation: this.props.data[3]

      });
    } else {
      console.log("No one to write to")
    }
    //show stored messages
    this._isMounted = true;
    this.props.getChat(this.props.data[0], this.props.data[1], this.props.data[2], this.state.currentChat, this.props.user.fullName, this.props.user.avatar)
  }

  componentWillUnmount() {
    this._isMounted = false;
    console.log("Chat will unmount")
    // Actions.conversations()
  }

  onLoadEarlier() {
    console.log('OnLoadEarlier oldP ' + JSON.stringify(this.state.oldMessages))
      if (JSON.stringify(this.props.user) === '{}') {
      this.setState((previousState) => {
        this._isMounted = false;
        Actions.app({ type: 'reset' })
        Actions.login()
      })
      } else {
          this.setState({isLoadingEarlier: true})

      }

    setTimeout(() => {
      if (this._isMounted === true) {
        this.setState((previousState) => {
          return {
            messages: GiftedChat.prepend(previousState.messages, this.props.oldMessages),
            loadEarlier: false,
            isLoadingEarlier: false,
          };
        });
      }
    }, 1000); // simulating network
  }

  onSend(messages = [], launch) {
    console.log('OnSandyDD ' + launch + ' ' + JSON.stringify(messages))
    // console.log('OnSandyDDLocation ' + launch + ' ' + JSON.stringify(location))

    // if ()
    
    this.setState((previousState) => {
      if (JSON.stringify(this.props.user) === '{}') {
        console.log("User didn't log in ")
        this._isMounted = false;
        Actions.app({ type: 'reset' })
        return
      } else {
        return {
          messages: GiftedChat.append(previousState.messages, messages),
        };
      }

    });
    if(!launch){
      console.log('Messages new ' + JSON.stringify(messages) + ' cChat ' + this.props.currentChat + ' writeto ' + this.state.writeToUid)
      this.props.addMessage(messages, this.state.writeToUid, this.state.writeToFullName, this.props.user.fullName, this.props.user.avatar, this.props.currentChat )
    }


  }

  renderCustomActions(props) {
    // if (Platform.OS === 'ios') {
      return (
        <CustomActions
          {...props}
        />
      );
    // }
    const options = {
      'Action 1': (props) => {
        alert('option 1');
      },
      'Action 2': (props) => {
        alert('option 2');
      },
      'Cancel': () => {},
    };
    // return (
    //   <GiftedActions
    //     {...props}
    //     options={options}
    //   />
    // );
  }

  renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
            backgroundColor: 'rgba(0,0,0,0.25)',
            borderRadius: 5
          },
          right: {
            backgroundColor: 'rgba(255,255,255,0.25)',
              borderRadius: 5,
          }
        }}
        textStyle={{
              right: {
                  color: 'white',
              },
              left: {
                  color: 'white',
              }
          }}
      />
    );
  }

  renderChatView(props) {
      return (
        <ChatView
          {...props}
        />
      );
  }

  renderDay(props) {
   return <Day {...props} textStyle={{color: 'white'}}/>
 }

 renderLoadEarlier(props) {
   return <LoadEarlier
            {...props}
            textStyle={{color: 'white'}}
            wrapperStyle={{ backgroundColor:'rgba(255,255,255,0.25)', borderRadius:5}}
            />
 }

  renderFooter(props) {
    if (this.state.typingText) {
      return (
        <View style={styles.footerContainer}>
          <Text style={styles.footerText}>
            {this.state.typingText}
          </Text>
        </View>
      );
    }
    return null;
  }

  renderSystemMessage(props) {
    return (
      <SystemMessage
        {...props}
        containerStyle={{
          marginBottom: 15,
        }}
        textStyle={{
          fontSize: 14,
        }}
      />
    );
  }

  render() {
    return (
      <LinearGradient
      colors={['#008d9a', '#2f6ba1']}
      style={{height:AppSizes.screen.height - 130}}
      start={{x: 0.5, y: 0.3}}
      end={{x: 0.7, y: 1.0}}
    >
      <GiftedChat
        messages={this.state.messages}
        onSend={this.onSend}
        loadEarlier={this.state.loadEarlier}
        onLoadEarlier={this.onLoadEarlier}
        isLoadingEarlier={this.state.isLoadingEarlier}

        user={{
          _id: this.props.user.uid, // sent messages should have same user._id
        }}

        renderActions={this.renderCustomActions}
        renderBubble={this.renderBubble}
        renderCustomView={this.renderChatView}
        renderFooter={this.renderFooter}
        renderDay={this.renderDay}
        renderLoadEarlier={this.renderLoadEarlier}
        renderSystemMessage={this.renderSystemMessage}

      />

      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});

// export default connect(mapStateToProps, mapDispatchToProps)(Chat);
export default Chat
