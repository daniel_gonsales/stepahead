/**
 * Activity Tabs Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { connect } from 'react-redux';
import React, { Component } from 'react';


// The component we're mapping to
import ChatView from './Chat';
import * as ChatActions from '@redux/chat/actions'

const mapStateToProps = state => ({  
    user: state.user,
    messages: state.chat.messages,
    currentChat: state.chat.currentChat,
    oldMessages: state.chat.oldMessages
});

const mapDispatchToProps = {
  getChat: ChatActions.getChat,
  addMessage: ChatActions.addMessage,
};

class ChatContainer extends Component {
    static componentName = 'ChatContainer';


    render = () => {
        // const { activity } = this.state;
        const { user, messages } = this.props;
    
        return (
          <ChatView/>
        );
      }    
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatView);
