import { Image, StyleSheet, View } from 'react-native';
import { Header } from 'react-navigation' ;
import LinearGradient from 'react-native-linear-gradient';

const GradientHeader = props => (
  <View style={{ backgroundColor: 'transparent' }}>
    <LinearGradient
      colors={['#00a8c3', '#00373f']}
      style={{flex:1, position:'absolute', top: 0 ,right:0, left:0, bottom:0}}
    />
    <Header {...props} style={{ backgroundColor: 'transparent' }}/>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
    paddingTop: 55
  },
  box: {
    flex: 1,
    marginBottom: 150,
    marginTop: 50,
    marginHorizontal: 25
  }
});

export default GradientHeader;
