import React, { Component } from 'react';
import {
  Image,              // Renders images
  StyleSheet,         // CSS-like styles
  Text,               // Renders text
  View,               // Container component
  ImageBackground,
  TouchableOpacity,
  Platform
} from 'react-native';
import { Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Chat from '@components/chat/Chat';

import { TabIcon } from '@ui/';

import FastImage from 'react-native-fast-image';


export default class CalloutExtended extends Component {
  render() {
    const { name, image, subtitle, onPressFavourite, isFavourite } = this.props;
    return (
      <View style={styles.container}>
          <View
            style={{
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                overflow: 'hidden',
            }}
          >
          {/* On Android FastImage doesn't work here */}
          { Platform.OS === "ios" ? 
                    <FastImage
                    style={styles.image}
                    source={{
                      uri: image,
                      priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                  >
                  <View
                    style={{
                        flex: 1,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        padding: 6,
                        paddingRight: 10,
                        paddingLeft: 10,
                    }}
                  >
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={onPressFavourite}
                  >
                    <Icon
                      type='font-awesome'
                      name={'music'}
                      color="#00e9bf"
                    />
                  </TouchableOpacity>
                    <TouchableOpacity
                      activeOpacity={0.8}
                      onPress={onPressFavourite}
                    >
                      <Icon
                        type='font-awesome'
                        name={'ellipsis-h'}
                        color="#00e9bf"
                      />
                    </TouchableOpacity>
                  </View>
                  </FastImage>
          : 
null
          }


            <View style={{
              backgroundColor: "#ffffff",
              padding:10,
              maxHeight: 55,
              borderRadius: 10,
              borderTopLeftRadius: 0,
              borderTopRightRadius: 0,
              flex:1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              width:200
            }}>
              <View style={{flex:1}}>
                <Text numberOfLines={1} ellipsizeMode='tail' style={styles.name}>{name}</Text>
                <Text numberOfLines={2} ellipsizeMode='tail' style={styles.subtitle}>{subtitle ? subtitle : name}</Text>
              </View>
              <View>
              <TouchableOpacity
                activeOpacity={0.8}
              //  onPress={ () => Actions.chat()}
              onPress={() => {Actions.conversations()}}
              >
                <Icon
                  type='font-awesome'
                  name={'commenting-o'}
                  color="#00e9bf"
                  size={30}
                />
              </TouchableOpacity>
              </View>
            </View>

          </View>

        <View style={styles.arrowBorder} />
        <View style={styles.arrow} />
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignSelf: 'flex-start',
    borderTopLeftRadius:10,
    borderTopRightRadius:10,
    borderRadius:10,
    overflow:'hidden',
    borderColor:'transparent'
  },
  // Callout bubble
  bubble: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    backgroundColor: '#fff',
    borderRadius: 6,
    borderColor: '#ccc',
    borderWidth: 0.5,
    //padding: 15,
    width: 150,
  },
  // Arrow below the bubble
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#fff',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -32,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: '#007a87',
    borderWidth: 16,
    alignSelf: 'center',
    marginTop: -0.5,
  },
  // Character name
  name: {
    fontSize: 16,
    marginBottom: 5,
  },
  subtitle: {
    fontSize: 12,
    color: 'gray',
    marginBottom: 5,
  },

  // Character image
  image: {
    width: 220,
    height: 150,
    // borderWidth:1,
    // overflow:'hidden',
    // alignItems: 'stretch',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
});
