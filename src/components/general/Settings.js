/**
 * Placeholder Scene
 *
    <Placeholder text={"Hello World"} />
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
//import React from 'react';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View,  Image, TouchableOpacity, StyleSheet, TextInput, ImageBackground, ScrollView} from 'react-native';
import { connect } from 'react-redux';


// Consts and Libs
import { AppStyles, AppSizes } from '@theme/';

// Components
// import { Text } from '@ui/';
import { Spacer } from '@ui/';


import { Container, Header, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Separator } from 'native-base';
import { Actions } from 'react-native-router-flux';
import * as UserActions from '@redux/user/actions'
import * as ActivityActions from '@redux/activities/actions'
import * as Translate from '@redux/translate/actions'


let language = ''

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  user: state.user,
  localLanguage: state.translate.localLanguage,
  settingsText: state.translate.settingsText,
  translate: state.translate

});

// Any actions to map to the component?
const mapDispatchToProps = {
  updateUserLanguage: UserActions.updateUserLanguage,
  setLocalLanguage: Translate.changeLanguage,
  settingsText: Translate.settingsText,
  updateUserData: UserActions.updateUserData,
  updateUserDataActivities: UserActions.updateUserDataActivities
};

const styles = StyleSheet.create({
  featuredImage: {
    width: AppSizes.screen.height * 0.2,
    height: AppSizes.screen.height * 0.2,
    borderRadius: AppSizes.screen.height * 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: AppSizes.navbarHeight
  },
});

/* Component ==================================================================== */
class Settings extends Component {
  static componentName = 'Settings';

  componentWillReceiveProps (props) {
    this.setState({
      wifit: props.settingsText.wifi,
      langt: props.settingsText.lang
    })
  }

  componentWillMount(props){
    this.setState({
      langt: this.props.settingsText.lang,
      wifit: this.props.settingsText.wifi,
      editButt: true,
      fullNameButt: false,
      phoneButt: false,
      firstNameButt: false,
      lastNameButt: false

    })
  }

  changePassword(){
    Actions.passwordReset();
  }

  updatelanguage(e) {
    if(this.props.localLanguage === 'English'){
      Actions.refresh({title: 'Uzstadijumi'})
      this.props.setLocalLanguage('English')

     } else {
      Actions.refresh({title: 'Settings'})
      this.props.setLocalLanguage('Latvian')

    }
  }

  onPressSettings(param){
  if (this.state.editButt) {
    this.setState({
      editButt: false
    })
    return
  } else {
    this.setState({
      editButt: true
    })
    return
  }

  }

  onPressEdit(param){
    let butt = param + 'Butt'
    console.log('butttttt ' + butt)
    console.log('butttttt state ' + this.state[butt])
    if(this.state[butt]){
      console.log('ButtStuff ' + this.state[param])
      if(this.state[param]){
        this.props.updateUserData(this.state[param], param)
        // this.props.updateUserDataActivities(text, this.props.user.avatar)
      } else {
        this.props.updateUserData(this.props.user[param], param)

      }
    }
    this.setState({
      [butt]: !this.state[butt]
    })
  }

getUnderlinedTitle(text){
  return(
    <View style={{
      flexDirection: 'row',
    }}
    >
      <View
        flex={0.05}
      >
      </View>

      <View
        borderBottomWidth= {2}
        borderColor= 'rgba(255,255,255,0.4)'
        flex={0.95}
      >
        <Text
          style={{
            fontSize:12,
            color:'rgba(255,255,255,0.6)',
            fontFamily: 'Roboto',
            fontWeight: 'bold',
            marginBottom: 10,
            borderColor: 'transparent'
          }}
        >
          {text}
        </Text>

      </View>
    </View>
  )
}

getTextWField(title, dataToEdit, editButt) {
  return(
<View>
  <List style={{
    borderBottomWidth:0
  }}
  //  listDividerBg='#FFFFFF'
  >

  {/* <ListItem itemDivider></ListItem> */}
  <ListItem style={{
      marginTop:10,
      // borderBottomWidth:0
      // marginBottom:20
      borderBottomWidth:0

     }}
  //onPress={this.updatelanguage.bind(this)}
   icon
  //  borderBottomWidth={5}
  >
    <Left>
      {/* <Icon name="ios-body" /> */}
    </Left>
    <Body style={{
      borderBottomWidth:0
     }}
    >
      <Text
        style={{
       //   marginTop:20   ,
       //   marginBottom:20
          color:'#eaeaea'
         }}
      >
        {/* Translate - Done - updateUserLang */}

        {/* Language */}
        {title}
      </Text>
    </Body>
    <Right>

      {editButt ?
        <TextInput
          style={{
            width:120,
            color:'#eaeaea',
            fontWeight:'bold'
          }}
          onChangeText={(text) => {
          // this.props.updateUserData({text}, dataToEdit)
          // this.props.updateUserDataActivities(text, this.props.user.avatar)
          console.log('updateddSettings ' + dataToEdit + ' ' + text)
          this.setState({
            [dataToEdit]: text
          })
          }}
          defaultValue={this.props.user[dataToEdit] ? `${this.props.user[dataToEdit]}` : ''}
        />
      :
      <Text style={{
        color:'#eaeaea',
        width:120

         }}
       >
         {this.props.user[dataToEdit]}
       </Text>
      }

      <Icon
        type="Entypo"
        name="edit"
        onPress={res => {
          console.log('EditPressedSettings ' + dataToEdit)
          this.onPressEdit(dataToEdit)
        }}
      />

    </Right>
  </ListItem>
</List>

</View>
  )
}

  langField(langt, localLanguage){
    return(
      <List
      >
           <ListItem
           onPress={this.updatelanguage.bind(this)}
           style={{
             marginTop:10   ,
             marginBottom:10

             }}
             numberOfLines={1}
             icon
           >

             <Left>
               {/* <Icon name="flag" /> */}
             </Left>
             <Body style={{
                borderBottomWidth:0
               }}

             >
               <Text
                 style={{
                //   marginTop:20   ,
                //   marginBottom:20
                   color:'white',
                  }}
                  numberOfLines={1}
               >
                {/* Translate - Done - updateUserLang */}

                 {/* Language */}
                 {langt}
               </Text>
             </Body>
             <Right>
               <Text style={{
                 color:'#eaeaea'
               }}>
               {localLanguage}
               </Text>
             <Icon name="arrow-forward" />
             </Right>
           </ListItem>
           </List>
    )
  }

  render = () => {
    const { category, title, body, image, onPress, isFavourite, localLanguage, fullName } = this.props;
    const { wifit, langt, editButt, fullNameButt, phoneButt, firstNameButt, lastNameButt} = this.state

    return(
      <View backgroundColor={'#1d677e'} style={{...StyleSheet.absoluteFillObject}}

    >
        {this.props.user.fullName ?
          <ScrollView style={{backgroundColor:'#1d677e',borderBottomColor:'transparent'}} >

            <View backgroundColor={'#1d677e'}>

        {/*
        |----------------------------------------------|
        |                 \Settings\        \FilterButt|
        |----------------------------------------------|
        |                    Header                    |
        |----------------------------------------------|
        |                                              |
        |                   Image                      |
         */}
<ImageBackground source={require('../../images/back-01.png')} style={{width: AppSizes.screen.width, height: 200}}>

        <View style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >

        <Image
          source={{
            uri: this.props.user.avatar,
            // overlayColor: 'white',
          }}
          // imageStyle={{ borderRadius: 5 }}
          style={[styles.featuredImage]}
          imageStyle={{
            borderRadius: AppSizes.screen.height * 0.3
          }}
        />

      </View>
        </ImageBackground>


        {/*----------------------------------------------*/}

        {/*
        |                                              |
        | Text: Profile                                |
        */}
          {this.getUnderlinedTitle('PROFILE')}
        {/*
        |----------------------------------------------|
        */}

            {/*
        | Text: User Name         editfield |editbutton|
        |                                              |
            */}
            {this.getTextWField('Username', 'fullName', fullNameButt)}

            {this.getTextWField('First name', 'firstName', firstNameButt)}

            {this.getTextWField('Lastname', 'lastName', lastNameButt)}


            {/*
        | Text: User Phone        editfield |editbutton|
        |                                              |
            */}
            {this.getTextWField('Phone', 'phone', phoneButt)}

        {/*
        |                                              |
        | Text: Settings                               |
        */}
        <Spacer size={20} />
        {this.getUnderlinedTitle('SETTINGS')}
      {/*
        |----------------------------------------------|
      */}
          {/* </View> */}

        <View>
          {this.langField(langt, localLanguage)}
        </View>
        </View>
        <View>
          <List>
          <ListItem
           onPress={this.changePassword.bind(this)}
           style={{
             marginTop:10   ,
             marginBottom:10

             }}
             numberOfLines={1}
             icon
           >

             <Left>
               
             </Left>
             <Body style={{
                borderBottomWidth:0
               }}>
               <Text
                 style={{
                //   marginTop:20   ,
                //   marginBottom:20
                   color:'white',
                  }}
                  numberOfLines={1}
               >
                {/* Translate - Done - updateUserLang */}

                 {/* Language */}
                 Change Password
               </Text>
             </Body>
             <Right>
             <Icon name="arrow-forward" />
             </Right>
           </ListItem>
          </List>
        </View>
        </ScrollView>
        

        :
        <View style={{backgroundColor:'#1d677e', height:100, marginTop:60}}>
          {this.langField(langt, localLanguage)}
        </View>
        }

      {/*
        |                                              |
        | Text: Language                       dropdown|
        |                                              |
      */}

      {/*
        |----------------------------------------------|
      */}
      </View>

    )
  }
}


/* Export Component ==================================================================== */
//export default Settings;
export default connect(mapStateToProps, mapDispatchToProps)(Settings);
