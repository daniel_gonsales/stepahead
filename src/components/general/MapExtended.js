import React, { Component } from 'react';
import {
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Platform,
  // Image,
  ImageBackground,
  TouchableHighlight
} from 'react-native';
import { Icon, SearchBar } from 'react-native-elements';


import MapView, { PROVIDER_GOOGLE, Circle } from 'react-native-maps';

import Callout from './Callout';
import CalloutExtended from './CalloutExtended';
import PropTypes from 'prop-types';
import { Firebase, FirebaseRef } from '../../constants/index';
//import geoFire from 'geofire';


import * as UserActions from '../../redux/user/actions';
import * as ActivityActions from '../../redux/activities/actions';
import * as ChatActions from '@redux/chat/actions';


import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import FastImage from 'react-native-fast-image';

// const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   paddingHorizontal: 10
  // },
//   button: {
//     alignItems: 'center',
//     backgroundColor: '#DDDDDD',
//     padding: 10
//   }
// })

const mapStateToProps = state => ({
  activities: state.activity.activities || [],
  filter: state.user.filter,
  radius: state.user.radius,
  lastLocation: state.user.lastLocation,
  user: state.user,
});

const mapDispatchToProps = {
  updateLocation: UserActions.updateLocation,
  getActivities: ActivityActions.getActivities,
  updateRadius: UserActions.updateRadius,

};

setMarkerRef = (ref) => {
  this.marker = ref
}

showCallout = () => {
  this.marker.showCallout()
}


let mapStyle =  [
    {
        "featureType": "all",
        "elementType": "all",
        "stylers": [
            {
                "hue": "#009983"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -70
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            },
            {
                "saturation": -60
            }
        ]
    }
];

class MapExtended extends Component {
  static propTypes = {
    activities: PropTypes.arrayOf(PropTypes.object),
}

static defaultProps = {
  activities: [],
}

  state = {
    loading: false,
    activities: [],
    mapRegion: null,
    // lastLat: 37.421998333333335,
    // lastLong: -122.08400000000002,
    lastLat: 56.6452 ,
    lastLong: 24.1039,
    circle: (
      <Circle
      center={{
        latitude: 0  ,
        longitude: 0,
      }}
      radius={60200}
      fillColor="rgba(255, 0, 255, 1)"
      strokeColor="rgba(0,0,0,0.3)"
      zIndex={2}
      strokeWidth={2}
    />
    )
  }

  getMarker(category){
    switch(category){
      case'Falling':{
        return(require('../../images/markers/lemon.png'))
      }
      case'Jumping':{
        return(require('../../images/markers/leaf.png'))
      }
      case'Running':{
        return(require('../../images/markers/truck.png'))
      }
      default:{
        return(require('../../images/markers/random.png'))
      }
    }
  }

  componentWillReceiveProps(props) {
    console.log('onRegionChange maps p receive ' + JSON.stringify(props.location))
    console.log('onRegionChange maps p receive ' + JSON.stringify(this.props.location))

    let templat
    let templong
    if (this.props.location != undefined){
      templat = props.location.latitude
      templong = props.location.longitude

      let region = {
        latitude:       templat,
        longitude:      templong,
        latitudeDelta:  0.00922*1.5,
        longitudeDelta: 0.00421*1.5
      }
      console.log('onRegionChange call Mapgot ' + JSON.stringify(region))

      this.onRegionChange(region, region.latitude, region.longitude);
    }


    if (props.activities) {
      this.setState({ activities: props.activities, radius: props.radius });

    }
  }

  componentWillMount(){
    if(JSON.stringify(this.props.user) != '{}' || JSON.stringify(this.props.user) != undefined){
      console.log('userloggedinonmap ' + JSON.stringify(this.props.user))
      if (this.props.user.location){
        let region = {
          // latitude:       position.coords.latitude,
          // longitude:      position.coords.longitude,
          latitude:       this.props.user.location.latitude,
          longitude:      this.props.user.location.longitude,
          latitudeDelta:  0.00922*1.5,
          longitudeDelta: 0.00421*1.5
        }
        console.log('onRegionChange call ' + JSON.stringify(region))
        
        this.onRegionChange(region, region.latitude, region.longitude);
      } else {
        console.log('nouseronmap')

        let region = {
          // latitude:       position.coords.latitude,
          // longitude:      position.coords.longitude,
          latitude:       56.9496,
          longitude:      24.1052,
          latitudeDelta:  0.00922*1.5,
          longitudeDelta: 0.00421*1.5
        }

        this.onRegionChange(region, region.latitude, region.longitude)
      }
    } 


    // if(JSON.stringify(this.props.user) != '{}' || JSON.stringify(this.props.user) != 'undefined'){
    //   this.props.updateRadius(this.props.user.radius)
    // } else {
    //   this.props.updateRadius(400)
    // }

    this.setState({loading: false})

  }


  componentDidMount() {
    let templat
    let templong

    this.watchID = navigator.geolocation.watchPosition((position) => {
      // Create the object to update this.state.mapRegion through the onRegionChange function
      if (this.props.location){
        templat = this.props.location.latitude
        templong = this.props.location.longitude
      } else {
        templat = position.coords.latitude
        templong = position.coords.longitude
      }
      let region = {
        // latitude:       position.coords.latitude,
        // longitude:      position.coords.longitude,
        latitude:       templat,
        longitude:      templong,
        latitudeDelta:  0.00922*1.5,
        longitudeDelta: 0.00421*1.5
      }
      console.log('onRegionChange callDidsis ' + JSON.stringify(region))
      this.onRegionChange(region, region.latitude, region.longitude);
    });

    if(!this.state.loading){
      console.log('updateafterload')
      this.state.loading = true

      Actions.app({type: 'reset'})
      Actions.refresh({title: 'map'})

      this.forceUpdate()
    }
  }

  onRegionChange(region, lastLat, lastLong) {
    console.log('onRegionChange Answer ' + lastLat + ' ' + lastLong)

    this.setState({
      mapRegion: region,
      // If there are no new values set use the the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });

    var mapRef = this.refs['mapView']
    console.log("MapViewRef " + JSON.stringify(mapRef))
    mapRef && mapRef.animateToRegion({
      ...position.coords,
      longitudeDelta: 0.005,
      latitudeDelta: 0.005,
    })

    console.log(lastLat + " lolly " + lastLong)
    //this.renderCircle(lastLat, lastLong, this.props.radius)
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }

  onMapPress(e) {
    if(typeof e.nativeEvent.coordinate == 'undefined'){
      return null;
    }

    let region = {
      latitude:       e.nativeEvent.coordinate.latitude,
      longitude:      e.nativeEvent.coordinate.longitude,
      latitudeDelta:  0.00922*1.5,
      longitudeDelta: 0.00421*1.5
    }
    //this.onRegionChange(region, region.latitude, region.longitude)

  }

  handleMarkerPress(event) {
    const markerID = event.nativeEvent.id
    renderedMarker = markerID
    console.log(markerID + " MarkerPress " + JSON.stringify(event.nativeEvent))
    console.log("WAS PRESSED ")
                  {/* Callout */}
                  <MapView.Callout tooltip style={styles.callout}>
                  <CalloutExtended
                    name={event.nativeEvent.name}
                    image={event.description}
                  />
                </MapView.Callout>

  }

  onPressCard(marker) {
    console.log(" MapExtendedCallout OnPressCard: " + JSON.stringify(this.props.activities))
    console.log(JSON.stringify(this.props.user) + " MapExtended marker is " + JSON.stringify(marker))
    Actions.activityView({
      title: marker.title,
      activity: marker,
      user: this.props.user
    });
  }

  watchLocation() {
    // eslint-disable-next-line no-undef
    this.watchID = navigator.geolocation.watchPosition((position) => {
      const myLastPosition = this.state.myPosition;
      const myPosition = position.coords;
      // if (!isEqual(myPosition, myLastPosition)) {
      //   this.setState({ myPosition });
      // }

      this.setState({
        // If there are no new values set use the the current ones
        lastLat: myPosition.latitude,
        lastLong: myPosition.longitude
      });

      if(JSON.stringify(this.props.user) != '{}') {
        this.props.updateLocation(myPosition.longitude, myPosition.latitude)
      }
      return(
          <View>
          {this.renderCircle(myPosition.latitude, myPosition.longitude, this.props.radius)}
          </View>
      )
      //this.renderCircle(myPosition.latitude, myPosition.longitude, this.props.radius)
    }, null, this.props.geolocationOptions);
}

  renderMapMarkers = () => {
    return(
      this.props.activities.map(marker => (
        <MapView.Marker
        // ref={}
        key={marker.id}
        zIndex={parseInt(marker.id)}
        // resizeMode="contain"
        // style={styles.customMap}
        style={{height:50, width:0.1}}
         coordinate={ {
           latitude: marker.location.latitude,
           longitude: marker.location.longitude
        } }
        image={this.getMarker(marker.category)}
        
        //identifier={marker.id}

      // onCalloutPress={e => this.onPressCard(marker)}
      // onPress={e => this.handleMarkerPress(e)}
        >


        {/* <Image source={this.getMarker(marker.category)} style={{ width: 40, height: 40 }} 
          onLoad={() => {console.log('ImageMarkeronLoad'), this.props.getActivities(), this.forceUpdate()}}
          onError={() => console.log('ImageMarkeronError')}
          onLayout={() => console.log('ImageMarkerOnLayout')}
          onLoadEnd={() => console.log('ImageMarkerOnLoadEnd')}
          onLoadStart={() => console.log('ImageMarkerOnLoadStart')}
          onProgress={() => console.log('ImageMarkerOnProgress')}
        /> */}
         <MapView.Callout tooltip={true}>
           <CalloutExtended
             name= {marker.title}
             subtitle = {marker.fullName}
             image= {marker.image}
             />
         </MapView.Callout>
        </MapView.Marker>
  ))
    )
  }

  renderCircle(a,b,r){
    console.log(b + " porops " + a + " radius " + r)
    console.log("Drawing a Circle")
    if(!r || parseInt(r)==0) {
      r = 600
      console.log(b + " porops " + a + " radius " + r)

      return(
            <View></View>
        )
    } else {
      return(
        <Circle
        center={{
          latitude: a,
          longitude: b,
        }}
        radius={r * 1000 }
        fillColor="rgba(0, 0, 0.5, 0.2)"
        strokeColor="rgba(0,0,0,0.5)"
        zIndex={2}
        strokeWidth={2}
      />
        )
    }

  }

  searchUpdated(search) {
    console.log("Searching for " + search)
    this.setState({
      search: search
    })

    console.log('I do not leak!');
    console.log("ListingView Search " + search + " " + this.state.search)
      console.log("ListingView Search Res " + search + " " + this.state.search)
        this.props.getActivities(search)

  }


  render() {
    return (
      <View style={{flex: 1}}>

      <View style={{alignItems:'stretch'}}>
            <SearchBar
              round
              showLoading
              icon={{ type: 'material', color: 'white', name: 'search' }}
              containerStyle={{
                backgroundColor: '#008d9a',
                borderBottomColor: 'transparent',
                borderTopColor: 'transparent',
                borderLeftColor: 'transparent',
                borderRightColor: 'transparent',
                borderLeftWidth:10,
                borderRightWidth:10,
                justifyContent: 'center',
              }}
              inputStyle={{
                backgroundColor:'#239ba7',
                borderColor:'rgba(0,0,0,0)',
                fontSize:18,
                textAlign: 'center',
                color: 'white'
              }}
              placeholderTextColor='white'
              onChangeText={(term) => { this.searchUpdated(term) }}
              // onClear={someMethod}
              // platform="android"
              placeholder='Search' />
      </View>

      <View style={{flex:1, alignItems:'stretch', justifyContent:'center'}}>

        <MapView
          calloutOffset={{ x: -8, y: 28 }}
          ref={ref => { this.map = ref; }}
          onRegionChangeComplete={this.showCallout}
          style={styles.map}
          region={this.state.mapRegion}
          showsUserLocation={true}
					showsMyLocationButton={true}
					showsCompass={true}
          followUserLocation={true}
          customMapStyle={mapStyle}
          minZoomLevel={8}
          // maxZoomLevel={0}
          provider={ Platform.OS === "ios" ? PROVIDER_GOOGLE : undefined}
          // onRegionChange={this.onRegionChange.bind(this)}
          // onPress={this.onMapPress.bind(this)}
          >

  {this.watchLocation()}
  {this.renderMapMarkers()}
  {this.renderCircle(this.state.lastLat, this.state.lastLong, this.state.radius)}
        </MapView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    //paddingHorizontal: 10
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  }
});

MapExtended.propTypes = {
  text: PropTypes.string,
};
MapExtended.defaultProps = {
  text: 'MapExtended',
  activities: []
 };
MapExtended.componentName = 'MapExtended';

class PinImage extends React.Component {
  render() {
      return (
          <View>
              <ImageBackground
                  onLoad={() => this.forceUpdate()}
                  resizeMode="contain"
                  source={require('../../images/markers/music.png')}
                  style={{
                      width: 50,
                      height: 50,
                  }}
              />
              {/* <Text
                  style={{
                      width: 0,
                      height: 0,
                  }}
              >
                  {Math.random()}
              </Text> */}
          </View>
      );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MapExtended);