# README #

This is a StepAhead app ReadMe. Check how the markdown works here [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### What is this repository for? ###

* Quick summary
* Version
* Notes and stuff

### Proposed DB structure ###

# Activities table structure #
 - profile {fname, lname, displayName, pictureUrl, facebook, twitter, aboutMe } //first version we are going to here, later in User
 - contacts { phone, phone2, website, address } //contact tied to this activity
 - description {short, long, price: { }, images: [], videoUrl, ageGroup }
 - mobile {enable, distance, price}
 - attributes {activityVerified, firstFree, suitableKids, equipmentRequired, partnerRequired, oneTime, seasonal}
 - timetable { available: { }, unavailable: {} } //need to think about this
 - userId

# User #
 - profile {fname, lname, display_name, pictureUrl, facebook, twitter, aboutMe, registrationNumber }
 - contacts { phone, phone2, website, address, email } 
 - settings {language, enableNotifications, filters: {}, radius }
 - lastLocation
 - lastLoggedin
 - accountType
